package com.studyway.miloficios.constantes;

import com.studyway.miloficios.entities.Host;

public class Constantes {


    public final static String IPPUBLICA = "";  //181.177.243.94:2029

    public final static int CODRESULTADOVACIO = 0;

    public final static int PUERTOHTTP = 0;
    public final static int PUERTOWS   = 0;
    public final static int RESULT_OK = 1;
    public final static int RESULT_ERROR_CONNECTION = 2;
    public final static int RESULT_ERROR_CERO = 0;
    public final static int RESULT_ERROR = -1;
    public final static int RESULT_WARNING  = 3;


    public final static int ACTIVO = 1;
    public final static int INACTIVO = 2;

    public final static String SEPBV = "|";
    public final static String SEPSN = "~";
    public final static String SEPAS = "*";
    public final static String SECOM = ",";

    public final static String DESA = "#";

    public final static String DESBV = "\\" + SEPBV;   // '\\|'
    public final static String DESAS = "\\" + SEPAS;
    public final static String DESSLASH = "\\/";

    public final static int BAJA    = 1;    // prioridades para los indicadores
    public final static int MEDIA   = 2;
    public final static int ALTA    = 3;


    public final static int CODHABILITADO = 1;
    public final static int CODDESHABILITADO = 0;

    // abstract communication
    public final static int IDRUTAERRONEA           = -1;


    public final static int IDMENSAJETOWEBSOCKET    = 10003;
    public final static int IDLISTENERWEBSOCKET     = 10004;
    public final static int IDGPS                   = 10005;


    //añadidos
    public final static int FLAG_LOCAL = 0;
    public final static int FLAG_ENVIADO = 1;
    public final static int COD_SQL = 0;


    public static Host getHostPublica(){
        Host host = new Host();
        host.setHost(Constantes.IPPUBLICA);
        host.setPuerto(Constantes.PUERTOHTTP);
        return host;
    }

    public static Host getHostSteven(){
        Host host = new Host();
//        host.setPuerto(2816); //TODO CAMBIAR EN LA EXPOSICIÓN GAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
        return host;
    }

    public static Host getHostJhon(){
        Host host = new Host();
//        host.setPuerto(2816); //TODO CAMBIAR EN LA EXPOSICIÓN GAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
        return host;
    }




}
