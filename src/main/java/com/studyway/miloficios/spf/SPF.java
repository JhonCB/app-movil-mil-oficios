package com.studyway.miloficios.spf;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

@SuppressLint("ApplySharedPref")
public class SPF {

    private static SPF instance = null;
    private Context context = null;
    private SharedPreferences.Editor editorSPF = null;
    private SharedPreferences spf = null;

    private static String SESIONGUARDADA = "SESIONGUARDADA";
    private static String KEYSPLASHSCREEN = "KEYSPLASHSCREEN";
    public static final String FECHACALENDAR = "";
    public static final String KEYCODIGO = "KEYCODIGO";

    private SPF(Context context)
    {
        this.context = context;
        spf = this.context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
    }

    public static SPF getInstance(Context context)
    {
        if (instance == null)
            instance = new SPF(context);
        return instance;
    }

    /**
     * esto es para guar el rango de kilometros que tendra el usuario
     * @param data
     */
    public void guardarSesion(boolean data){
        editorSPF = spf.edit();
        editorSPF.putBoolean(SESIONGUARDADA, data);
        editorSPF.commit();
    }

    public boolean getSesion(){
        return spf.getBoolean(SESIONGUARDADA, false);
    }

    /***
     * esto es para poder inciar el splash screen
     * @param
     */
    public void guardarKeySplashScreen(boolean data){
        editorSPF = spf.edit();
        editorSPF.putBoolean(KEYSPLASHSCREEN, data);
        editorSPF.commit();
    }

    public boolean getKeySplashScreen(){
        return spf.getBoolean(KEYSPLASHSCREEN, false);
    }

    /***
     * TESTIN SOLO EN PRUEBAS
     * @param
     */
    public void guardarKeyCodigoTemporal(int data){
        editorSPF = spf.edit();
        editorSPF.putInt(KEYCODIGO, data);
        editorSPF.commit();
    }

    public int getKeyCodigoTemporal(){
        return spf.getInt(KEYCODIGO, 0);
    }






}


