package com.studyway.miloficios.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.studyway.miloficios.R;
import com.studyway.miloficios.asyntask.ListaCategoriaTask;
import com.studyway.miloficios.asyntask.ListaLocalizacionTask;
import com.studyway.miloficios.asyntask.RegistrarServicioTask;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.dao.PerfilDAO;
import com.studyway.miloficios.entities.Categoria;
import com.studyway.miloficios.entities.Localizacion;
import com.studyway.miloficios.enums.IDUTILS;
import com.studyway.miloficios.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistrarServicioDialog extends Dialog {
    @BindView(R.id.tvFecha) TextView tvFecha;
    @BindView(R.id.spCategoria) Spinner spCategoria;
    @BindView(R.id.spUbicacion) Spinner spUbicacion;
    @BindView(R.id.btRegistrar) Button btRegistrar;

    @BindView(R.id.etDescripcion) EditText etDescripcion;

    private int codCategoria = 0;
    private int codLocalizacion = 0;

    private Context context;
    private IRegistro iRegistro;

    public interface IRegistro {
       void onCorrecto(String descripcion);
       void onError(String descripcion);
    }

    public RegistrarServicioDialog(Context context, IRegistro iRegistro) {
        super(context);
        this.setContentView(R.layout.dialog_registrar_servicio);
        this.context = context;
        this.iRegistro = iRegistro;
        this.setCancelable(true);
        Objects.requireNonNull(this.getWindow()).setBackgroundDrawable(new ColorDrawable(0));
        ButterKnife.bind(this);
        initViews();

    }

    @OnClick({R.id.btRegistrar, R.id.rlFecha})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.btRegistrar:
                registrarServicio();
                break;
            case R.id.rlFecha:
                abrirCalendario();
                break;
        }
    }

    private void abrirCalendario(){
        String SEPARADOR = "-";
        Calendar calendarMax = Calendar.getInstance();
        Calendar calendar = Calendar.getInstance();
        calendarMax.add(Calendar.YEAR,10);
        //Estos valores deben ir en ese orden, de lo contrario no mostrara la fecha actual
        DatePickerDialog recogerFecha = new DatePickerDialog(context, (view, year, month, dayOfMonth) -> {
            //Esta variable lo que realiza es aumentar en uno el mes ya que comienza desde 0 = enero
            final int mesActual = month + 1;
            //Formateo el día obtenido: antepone el 0 si son menores de 10
            String diaFormateado = Utils.twoDigits(dayOfMonth);
            //Formateo el mes obtenido: antepone el 0 si son menores de 10
            String mesFormateado = Utils.twoDigits(mesActual);
            //Muestro la fecha con el formato deseado
            tvFecha.setText(year+SEPARADOR+mesFormateado+SEPARADOR+diaFormateado);

        },calendar.get(Calendar.YEAR), Calendar.MONTH, Calendar.DAY_OF_MONTH);



//        calendar.add(Calendar.YEAR,-100);
//        recogerFecha.getDatePicker().setMinDate(calendar.getTimeInMillis());
        //El usuario no puedo nacer mañana
        recogerFecha.getDatePicker().setMaxDate(calendarMax.getTimeInMillis());
        recogerFecha.getDatePicker().setMinDate(calendar.getTimeInMillis());
        //Muestro el widget
        recogerFecha.show();

    }

    private void registrarServicio(){
        try {
            //bloquear el boton
            if(!etDescripcion.getText().toString().isEmpty()){
                if(!tvFecha.getText().toString().equals("Sin fecha")){
                    if(codCategoria > 0){
                        if(codLocalizacion > 0){
                            btRegistrar.setEnabled(false);
                            new RegistrarServicioTask(context, Integer.parseInt(
                                    new PerfilDAO(context).getUltimaSesion().getIdPerfil()),
                                    etDescripcion.getText().toString(),
                                    tvFecha.getText().toString()+" "+new Utils().getFechaHora(IDUTILS.HHMMSS),
                                    codCategoria,
                                    codLocalizacion, "-11.990377", "-77.061594", new RegistrarServicioTask.IRegistrarUsuario() {
                                @Override
                                public void onResult(int codResultado, String desResultado) {
                                    if(codResultado == Constantes.RESULT_OK){
                                        iRegistro.onCorrecto(desResultado);
                                        dismiss();
                                    }else{
                                        iRegistro.onError(desResultado);
                                        btRegistrar.setEnabled(true);
                                    }

                                }
                            }).execute();

                        }else{
                            new Utils().toast("Seleccione ubicación",context);
                        }
                    }else{
                        new Utils().toast("Seleccione categoria",context);
                    }
                }else{
                    new Utils().toast("Seleccione fecha",context);
                }
            }else{
                new Utils().toast("Ingrese descripción",context);
            }

        }catch (Exception e){
            btRegistrar.setEnabled(true);
        }

    }

    private void llenarSpinnerCategoria(){
        new ListaCategoriaTask(context, new ListaCategoriaTask.IListaCategoria() {
            @Override
            public void onResult(int codResultado, String desResultado, ArrayList<Categoria> alCategoria) {
                if(codResultado == Constantes.RESULT_OK){
                    Categoria categoria = new Categoria();
                    categoria.setCodCategoria(0);
                    categoria.setNomCategoria("[Seleccione]");
                    alCategoria.add(0,categoria);

                ArrayAdapter<Categoria> adapter = new ArrayAdapter<Categoria>(context, android.R.layout.simple_spinner_dropdown_item, alCategoria);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spCategoria.setAdapter(adapter);

                    spCategoria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            Categoria categoria = (Categoria) adapterView.getItemAtPosition(i);
                            codCategoria = categoria.getCodCategoria();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                }else{
                    new Utils().toast(desResultado,context);
                }
            }
        }).execute();
    }

    private void llenarSpinnerUbicacion(){
        new ListaLocalizacionTask(context, new ListaLocalizacionTask.IListaLocalizacion() {
            @Override
            public void onResult(int codResultado, String desResultado, ArrayList<Localizacion> alLocalizacion) {
                if(codResultado == Constantes.RESULT_OK){
                    Localizacion localizacion = new Localizacion();
                    localizacion.setCodLocalizacion(0);
                    localizacion.setNomLocalizacion("[Seleccione]");
                    alLocalizacion.add(0,localizacion);

                    ArrayAdapter<Localizacion> adapter = new ArrayAdapter<Localizacion>(context, android.R.layout.simple_spinner_dropdown_item, alLocalizacion);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spUbicacion.setAdapter(adapter);

                    spUbicacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            Localizacion localizacion = (Localizacion) adapterView.getItemAtPosition(i);
                            codLocalizacion = localizacion.getCodLocalizacion();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                }else{
                    new Utils().toast(desResultado,context);
                }
            }
        }).execute();
    }


    private void initViews(){
        llenarSpinnerCategoria();
        llenarSpinnerUbicacion();

    }


}
