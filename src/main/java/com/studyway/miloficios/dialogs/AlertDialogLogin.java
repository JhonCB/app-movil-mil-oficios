package com.studyway.miloficios.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.widget.Button;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.GraphResponse;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.SignInButton;
import com.studyway.miloficios.R;
import com.studyway.miloficios.autentification.Facebook;
import com.studyway.miloficios.autentification.Google;
import com.studyway.miloficios.utils.Utils;

import org.json.JSONObject;

public class AlertDialogLogin extends Dialog {
    //Esto es para google
    GoogleSignInClient googleSignInClient;
    Google loginGoogle;
    SignInButton btGoogle;

    private Activity activity;

    private OnAlertDialogCallback onAlertDialogCallback;
    private AccessToken mAccessToken;

    //esto es facebook
    private CallbackManager callbackManagerFacebook;


    public interface OnAlertDialogCallback {
        void onActivityResult(Google loginGoogle);
        void onResultAccountGoogle(GoogleSignInAccount account) throws Exception;
        void onResultAccontFacebook(JSONObject object, GraphResponse response) throws Exception;
    }

    public AlertDialogLogin(Activity activity,GoogleSignInClient googleSignInClient,CallbackManager callbackManagerFacebook, OnAlertDialogCallback onAlertDialogCallback) {
        super(activity);
        this.setContentView(R.layout.dialog_opciones_menu);
        this.activity = activity;
        this.onAlertDialogCallback = onAlertDialogCallback;
        //GOOGLE
        this.googleSignInClient = googleSignInClient;
        //FACEBOOK
        this.callbackManagerFacebook = callbackManagerFacebook;
        initViews();

    }

    private void initViews(){
        //casteando button de google ----------
        btGoogle =  findViewById(R.id.sign_in_button);
        loginGoogle = new Google(activity, googleSignInClient, new Google.OnLoginGoogleCallback() {
            @Override
            public void onResult(GoogleSignInAccount account) {
                try {
//                    loginGoogle.signOut();
                    onAlertDialogCallback.onResultAccountGoogle(account);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        loginGoogle.onStart();

        btGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginGoogle.signIn();
            }
        });

        onAlertDialogCallback.onActivityResult(loginGoogle);


        Button btRegistrarseFacebook = (Button) findViewById(R.id.btRegistrarseFacebook);
        final Facebook loginFacebook =  new Facebook(activity, callbackManagerFacebook, new Facebook.OnLoginFacebookCallback() {
            @Override
            public void onSuccess(JSONObject object, GraphResponse response) throws Exception {
                new Utils().toast("onSuccess", activity);
                onAlertDialogCallback.onResultAccontFacebook(object, response);
            }

            @Override
            public void onCancel(String cancel) {
                new Utils().toast("onCancel", activity);
            }

            @Override
            public void onError(String error) {
                new Utils().toast("Intentelo de nuevo !", activity);
            }
        });

        btRegistrarseFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginFacebook.logIn();
            }
        });
    }
}
