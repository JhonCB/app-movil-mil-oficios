package com.studyway.miloficios.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.studyway.miloficios.R;
import com.studyway.miloficios.adapters.AdapterRvCategoria;
import com.studyway.miloficios.asyntask.ListaCategoriaTask;
import com.studyway.miloficios.entities.Categoria;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoriaDialog extends Dialog {
    @BindView(R.id.rvListaCategoria) RecyclerView rvListaCategoria;

    private Context context;
    private ICategoriaDialog iCategoriaDialog;

    public interface ICategoriaDialog {
       void onClick(Categoria categoria);
    }

    public CategoriaDialog(Context context, ICategoriaDialog iCategoriaDialog) {
        super(context);
        this.setContentView(R.layout.dialog_categoria);
        this.context = context;
        this.iCategoriaDialog = iCategoriaDialog;
        this.setCancelable(true);
        Objects.requireNonNull(this.getWindow()).setBackgroundDrawable(new ColorDrawable(0));
        ButterKnife.bind(this);
        initViews();

    }

    private void initViews(){
        new ListaCategoriaTask(context, new ListaCategoriaTask.IListaCategoria() {
            @Override
            public void onResult(int codResultado, String desResultado, ArrayList<Categoria> alCategoria) {
                AdapterRvCategoria adapterRvServicio = new AdapterRvCategoria(context, alCategoria, new AdapterRvCategoria.IAdapterCategoria() {
                    @Override
                    public void onClick(Categoria categoria) {
                        iCategoriaDialog.onClick(categoria);
                        dismiss();
                    }
                });
                rvListaCategoria.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,false));
                rvListaCategoria.setAdapter(adapterRvServicio);
            }
        }).execute();
    }
}
