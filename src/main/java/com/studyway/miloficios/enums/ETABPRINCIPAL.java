package com.studyway.miloficios.enums;

/**
 * Created by innovaapps on 30/10/2015.
 */
public enum ETABPRINCIPAL
{
    AJUSTE          (0),
    OFERTAS         (1),
    HISTORIAL       (2),
    PUNTOS          (3);
    private int n;

    ETABPRINCIPAL(int n)
    {
        this.n = n;
    }
    public int getInt()
    {
        return n;
    }
}

