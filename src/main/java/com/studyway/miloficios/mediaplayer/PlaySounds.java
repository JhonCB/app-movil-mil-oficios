package com.studyway.miloficios.mediaplayer;

import android.content.Context;
import android.media.MediaPlayer;

import com.studyway.miloficios.R;

/**
 * Clase creada por Jhon Coronel 16/10/2019
 * Contacto: ghoozh@gmail.com
 */
public class PlaySounds {
    Context context;
    MediaPlayer soundSMS;

    public PlaySounds(Context context) {
        this.context = context;
    }

    public void reproducirSonidoSMS(){
        if(soundSMS ==null){
            soundSMS = MediaPlayer.create (context, R.raw.sound_sms);
        }
        soundSMS.start ();
    }

    public void stopSonidoSMS(){
        if(soundSMS != null && soundSMS.isPlaying()){
            soundSMS.stop();
        }
    }
}
