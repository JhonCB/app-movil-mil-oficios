package com.studyway.miloficios.datacontractenum;

public enum PerfilDataContract {

    TABLA		                ("Perfil"),
    ID			                ("_id"),
    NOMBRE                      ("Nombre"),
    APELLIDO                    ("Apellido"),
    NOMBRECOMPLETO              ("NombreCompleto"),
    TELEFONO                    ("Telefono"),
    TIPOUSUARIO                 ("TipoUsuario"),
    EMAIL                       ("Email"),
    CONTRASENIA                 ("Contrasenia"),
    CODLOCALIZACION             ("CodLocalizacion"),
    IDPERFIL                    ("IdPerfil"),
    TIPOPERFIL                  ("TipoPerfil"),
    URLPERFIL                   ("UrlFotoPerfil"),
    FLAGENVIADO                 ("FlagEnviado");


    String s;
    PerfilDataContract(String s){
        this.s = s;
    }

    public String getValue(){
        return s;
    }



}
