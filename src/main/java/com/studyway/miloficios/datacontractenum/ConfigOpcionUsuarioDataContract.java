package com.studyway.miloficios.datacontractenum;

public enum ConfigOpcionUsuarioDataContract {

    TABLA		                ("ConfigOpcionUsuario"),
    ID			                ("_id"),
    NOMBRE                      ("Nombre"),
    IDPERFIL                    ("IdPerfil"),
    CODVISUALIZACIONMAPA        ("CodVisualizacionMapa"),
    NUMRANGOMETROS               ("NumRangoMetros"),
    CODSONIDOHABILITADO         ("CodSonidoHabilitado"),
    FECHAHORA                   ("FechaHora"),
    LUGARGUARDADO               ("LugarGuardado"),
    FLAGENVIADO                 ("FlagEnviado");

    String s;

    ConfigOpcionUsuarioDataContract(String s){
        this.s = s;
    }

    public String getValue(){
        return s;
    }



}
