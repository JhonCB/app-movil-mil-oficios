package com.studyway.miloficios.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;
import com.moos.library.BottomBarLayout;
import com.moos.library.BottomTabView;
import com.studyway.miloficios.R;
import com.studyway.miloficios.adapters.AdapterPagerPrincipal;
import com.studyway.miloficios.dao.PerfilDAO;
import com.studyway.miloficios.entities.Perfil;
import com.studyway.miloficios.fragments.FragmentHome;
import com.studyway.miloficios.utils.AnimUtils;
import com.studyway.miloficios.utils.Utils;
import com.studyway.miloficios.view.ViewPagerCustom;

import androidx.drawerlayout.widget.DrawerLayout;

import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Creado por Jhon Coronel 15/10/2019
 * Contacto: ghoozh@gmail.com
 */

public class ActivityPrincipal extends BaseActivityNormal implements NavigationView.OnNavigationItemSelectedListener,BottomBarLayout.OnTabSelectedListener {

    @BindView(R.id.vpNavigationView) ViewPagerCustom vpNavigationView;
    @BindView(R.id.btNavigation) BottomBarLayout btNavigation;

    @BindView(R.id.ivTop) ImageView ivTop;
    @BindView(R.id.ivBottom) ImageView ivBottom;

    private TextView tvNombre,tvCorreo;
    private ImageView ivFoto;
    private BottomTabView tabAjustes, tabHome, tabHistorial, tabPuntos;
    private DrawerLayout drawer;
    private boolean isDrawerOpen = false;
    private Perfil perfil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        fullScreen();
        ButterKnife.bind(this);
        drawer = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        ivFoto = (ImageView)navigationView.getHeaderView(0).findViewById(R.id.ivFoto);
        tvNombre = (TextView)navigationView.getHeaderView(0).findViewById(R.id.tvNombre);
        tvCorreo = (TextView)navigationView.getHeaderView(0).findViewById(R.id.tvCorreo);

        drawer.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                    isDrawerOpen = true;

            }


            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                    isDrawerOpen = false;
            }
        });
        initBottomBar();
        setViews();
        AnimUtils.animacionEntrada(ivTop, ivBottom, new AnimUtils.IAnimUtils() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                standarScreen();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void initBottomBar(){
        vpNavigationView.setOffscreenPageLimit(4);

        tabAjustes = new BottomTabView(this);
        tabAjustes.setTabIcon(getResources().getDrawable(R.drawable.ic_svg_tab_ajustes));
        tabAjustes.setTabTitle("Ajustes");

        tabHome = new BottomTabView(this);
        tabHome.setTabIcon(getResources().getDrawable(R.drawable.ic_svg_home));
        tabHome.setTabTitle("Ofertas");

        tabHistorial = new BottomTabView(this);
        tabHistorial.setTabIcon(getResources().getDrawable(R.drawable.ic_svg_history));
        tabHistorial.setTabTitle("Historial");

        tabPuntos = new BottomTabView(this);
        tabPuntos.setTabIcon(getResources().getDrawable(R.drawable.ic_svg_sol));
        tabPuntos.setTabTitle("Puntos");
        btNavigation
                .addTab(tabAjustes)
                .addTab(tabHome)
                .addTab(tabHistorial)
                .addTab(tabPuntos)
                .create(this);

        vpNavigationView.setAdapter(new AdapterPagerPrincipal(ActivityPrincipal.this,getSupportFragmentManager()));
        vpNavigationView.setCurrentItem(1,false);
//        btNavigation.bindViewPager(vpNavigationView);
        btNavigation.setCurrentTabIndex(tabHome.getTabPosition());

        //todo MODIFICAR LIBRERIA DE KENNY
        final View rlNavigation = findViewById(R.id.rlNavigation);
        rlNavigation.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            Rect r = new Rect();
            //r will be populated with the coordinates of your view that area still visible.
            rlNavigation.getWindowVisibleDisplayFrame(r);

            int heightDiff = rlNavigation.getRootView().getHeight() - (r.bottom - r.top);
            if (heightDiff > Resources.getSystem().getDisplayMetrics().heightPixels/3) { // if more than 100 pixels, its probably a keyboard...
                rlNavigation.setVisibility(View.GONE);
//                btNavigation.hide(vpNavigationView); //descomentar
            }else{
                rlNavigation.setVisibility(View.VISIBLE);
//                btNavigation.show(vpNavigationView); //descomentar
            }
        });
    }

    private void setViews(){
        perfil = new PerfilDAO(this).getUltimaSesion();
        tvNombre.setText(perfil.getNombreCompleto());
        tvCorreo.setText(perfil.getEmail());
        Utils.setFotoPerfil(this, perfil.getUrlFotoPerfil(), ivFoto);
    }

    @OnClick({R.id.rlCuenta,R.id.rlUbicacion,R.id.rlCupon})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.rlCuenta:
                    drawer.openDrawer(GravityCompat.START);
                break;
            case R.id.rlUbicacion:
                    new Utils().toast("Agregar función UBICACION",this);
                break;
            case R.id.rlCupon:
                    new Utils().toast("Agregar función CUPON",this);
                break;
        }
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }else if(FragmentHome.IS_CHATBOT_ACTIVO) {
            FragmentHome.newInstance().ChatBotInvisible();
        } else{
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_cuenta:
                    startActivity(new Intent(ActivityPrincipal.this,ActivityCuentaLogin.class));
                break;
            case R.id.nav_gallery:

                break;
            case R.id.nav_slideshow:

                break;
            case R.id.nav_tools:

                break;
            case R.id.nav_share:

                break;
            case R.id.nav_developers:

                break;
        }
//        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onTabSelected(BottomTabView bottomTabView) {
        switch (bottomTabView.getTabPosition()){
            case 0:
                vpNavigationView.setCurrentItem(0,false);
                break;
            case 1:
                vpNavigationView.setCurrentItem(1,false);
                break;
            case 2:
                vpNavigationView.setCurrentItem(2,false);
                break;
            case 3:
                vpNavigationView.setCurrentItem(3,false);
                break;
        }
    }

    @Override
    public void onTabUnselected(BottomTabView bottomTabView) {
//        switch (bottomTabView.getTabPosition()){

//        }
    }

    @Override
    public void onTabReselected(BottomTabView bottomTabView) {
//        switch (bottomTabView.getTabPosition()){
//
//        }
    }

    private void fullScreen(){
        setTheme(R.style.AppTheme_FullScreen);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            WindowManager.LayoutParams params = getWindow().getAttributes();
            params.layoutInDisplayCutoutMode =
                    WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
    }
    private void standarScreen(){
        setTheme(R.style.AppThemeNoActionBar);
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

//    }
//        Window window = getWindow();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
//            WindowManager.LayoutParams params = getWindow().getAttributes();
//            params.layoutInDisplayCutoutMode =
//                    WindowManager.LayoutParams.TYPE_STATUS_BAR;
//            params.layoutInDisplayCutoutMode =
//                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION;
//        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
//            window.setNavigationBarColor(getResources().getColor(R.color.colorPrimary));
//        }
//        window.setFlags(WindowManager.LayoutParams.
//                        FLAG_FORCE_NOT_FULLSCREEN,
//                WindowManager.LayoutParams.
//                        FLAG_FORCE_NOT_FULLSCREEN);
    }
}
