package com.studyway.miloficios.activities;

import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.studyway.miloficios.R;
import com.studyway.miloficios.asyntask.GenerarSolicitudServicioTask;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.dao.PerfilDAO;
import com.studyway.miloficios.entities.Perfil;
import com.studyway.miloficios.entities.Servicio;
import com.studyway.miloficios.googleapi.MyMapView;
import com.studyway.miloficios.spf.SPF;
import com.studyway.miloficios.utils.GenerateUtils;
import com.studyway.miloficios.utils.GlideUtils;
import com.studyway.miloficios.utils.Utils;
import com.studyway.miloficios.utils.ValidationUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Clase creada por Jhon Coronel 04/11/2019
 * Contacto: ghoozh@gmail.com
 */

public class ActivityDetalleServicio extends BaseActivityNormal {

//    @BindView(R.id.svContenedor) ScrollView svContenedor;
    @BindView(R.id.rlAtras) RelativeLayout rlAtras;
    @BindView(R.id.rlSiguiente) RelativeLayout rlSiguiente;

    @BindView(R.id.ivFoto) ImageView ivFoto;
    @BindView(R.id.tvNombre) TextView tvNombre;
    @BindView(R.id.tvDescripcion) TextView tvDescripcion;
    @BindView(R.id.tvFechaInicio) TextView tvFechaInicio;
    @BindView(R.id.tvFechaFin) TextView tvFechaFin;
    @BindView(R.id.tvUbicacion) TextView tvUbicacion;
    @BindView(R.id.tvCategoria) TextView tvCategoria;
    @BindView(R.id.tvCosto) TextView tvCosto;
    @BindView(R.id.btSolicitar) Button btSolicitar;

    private MyMapView myMapView;
    private Servicio servicio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_servicio);
        ButterKnife.bind(this);
        myMapView = (MyMapView) findViewById(R.id.myMapView);
        myMapView.setOnCreate(savedInstanceState);
        myMapView.init(ActivityDetalleServicio.this);

        Intent intent = getIntent();
        if (intent.getSerializableExtra("iServicio") != null) {
            servicio = (Servicio) intent.getSerializableExtra("iServicio");
            if(servicio != null){
                if(servicio.getLatitud() != null && !servicio.getLatitud().isEmpty() && servicio.getLongitud() != null && !servicio.getLongitud().isEmpty()){
                    if(ValidationUtils.validarDouble(servicio.getLatitud()) && ValidationUtils.validarDouble(servicio.getLongitud())){
                        myMapView.setViews(servicio.getLatitud(), servicio.getLongitud());
                    }
                }else{
                    new Utils().toast("No tiene ubicación", ActivityDetalleServicio.this);
                }
                tvNombre.setText(servicio.getNombreUsuPub());
                tvDescripcion.setText(servicio.getDescripcionServicio());
                tvFechaInicio.setText(servicio.getFechaInicio());
                tvFechaFin.setText(servicio.getFechaFin());
                tvUbicacion.setText(servicio.getUbicacion());
                tvCategoria.setText(servicio.getDescripcion());
                if(ivFoto != null){
                    GlideUtils.setFotoServicio(ActivityDetalleServicio.this, servicio.getUrlFoto(), ivFoto, new GlideUtils.OnCallback() {
                        @Override
                        public void onResult(boolean isExitoso) {

                        }
                    });
                }
                try {
                    tvCosto.setText(GenerateUtils.obtenerNumeroAletorio(3));
                }catch (Exception e){
                    e.printStackTrace();
                    tvCosto.setText("220");
                }
            }

//            svContenedor.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
//                @Override
//                public void onScrollChanged() {
//                    int scrollY = svContenedor.getScrollY(); // For ScrollView
//                    int scrollX = svContenedor.getScrollX(); // For HorizontalScrollView
//                    Toast.makeText(ActivityDetalleServicio.this,scrollY+","+scrollX,Toast.LENGTH_SHORT).show();
//
//                }
//            });
            if(new PerfilDAO(ActivityDetalleServicio.this).getUltimaSesion().getTipoUsuario() == Perfil.SOCIO){
                btSolicitar.setVisibility(View.GONE);
            }

        }
    }

    @OnClick({R.id.rlAtras,R.id.llIrAMiUbicacion,R.id.btSolicitar})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.rlAtras:
                    finish();
                break;
            case R.id.llIrAMiUbicacion:
                if(servicio != null)
                    myMapView.animateCameraOferta(servicio.getLatitud(),servicio.getLongitud(),14f);
                break;
            case R.id.btSolicitar:
                btSolicitar.setEnabled(false);
                solicitarServicio();
                break;
        }
    }

    private void solicitarServicio(){
        if(servicio != null && servicio.getCodServicio() > 0){
            new GenerarSolicitudServicioTask(ActivityDetalleServicio.this, servicio.getCodServicio(), Double.parseDouble(tvCosto.getText().toString()), Double.parseDouble(tvCosto.getText().toString()), SPF.getInstance(ActivityDetalleServicio.this).getKeyCodigoTemporal(), new GenerarSolicitudServicioTask.IGenerarSolicitudServicio() {
                @Override
                public void onResult(int codResultado, String desResultado) {
                    new Utils().toast(desResultado,ActivityDetalleServicio.this);
                    if(codResultado == Constantes.RESULT_OK){

                        finish();
                    }else{
                        btSolicitar.setEnabled(true);
                    }
                }
            }).execute();
        }else{
            new Utils().toast("No se encontró el servicio, intentelo nuevamente",ActivityDetalleServicio.this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        myMapView.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        myMapView.onStart();
    }

    @Override
    public void onResume() {
        myMapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        myMapView.onDestroy();

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        myMapView.onLowMemory();
    }

    @Override
    public void onStop() {
        super.onStop();
        myMapView.onStop();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        myMapView.setOnSavedInstance(outState);
    }
}
