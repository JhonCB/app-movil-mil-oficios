package com.studyway.miloficios.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.studyway.miloficios.R;
import com.studyway.miloficios.asyntask.LoginTask;
import com.studyway.miloficios.asyntask.RecuperarContraseniaTask;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityNuevaContrasenia extends AppCompatActivity {
    @BindView(R.id.etContrasenia) EditText etContrasenia;
    @BindView(R.id.etRepetirContrasenia) EditText etRepetirContrasenia;
    @BindView(R.id.ivShield1) ImageView ivShield1;
    @BindView(R.id.ivShield2) ImageView ivShield2;
    @BindView(R.id.tvSiguiente) TextView tvSiguiente;
    @BindView(R.id.tvReintentarLogin) TextView tvReintentarLogin;

    private int codUsuario         = 0;
    private int codigoAutogenerado = 0;
    private String correo          = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nueva_contrasenia);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            codUsuario = intent.getExtras().getInt("codUsuario");
            codigoAutogenerado = intent.getExtras().getInt("codigoAutogenerado");
            correo = intent.getExtras().getString("correo");
        }else{
            new Utils().toast("Error al 602",ActivityNuevaContrasenia.this);
        }
        etContrasenia.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length() > 0){
                    if(editable.toString().equals(etRepetirContrasenia.getText().toString())){
                        ivShield1.setVisibility(View.VISIBLE);
                        ivShield2.setVisibility(View.VISIBLE);
                    }else{
                        ivShield1.setVisibility(View.INVISIBLE);
                        ivShield2.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });
        etRepetirContrasenia.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length() > 0){
                    if(editable.toString().equals(etContrasenia.getText().toString())){
                        ivShield1.setVisibility(View.VISIBLE);
                        ivShield2.setVisibility(View.VISIBLE);
                    }else{
                        ivShield1.setVisibility(View.INVISIBLE);
                        ivShield2.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });
    }

    @OnClick({R.id.tvSiguiente,R.id.tvSalir,R.id.tvReintentarLogin})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.tvSiguiente:
                if(etContrasenia.getText().toString().trim().equals(etRepetirContrasenia.getText().toString().trim())){
                    tvSiguiente.setEnabled(false);
                    etContrasenia.setEnabled(false);
                    etRepetirContrasenia.setEnabled(false);
                    new RecuperarContraseniaTask(ActivityNuevaContrasenia.this, codUsuario, String.valueOf(codigoAutogenerado), etContrasenia.getText().toString().trim(), new RecuperarContraseniaTask.ICambiarContrasenia() {
                        @Override
                        public void onResult(int codResultado, String desResultado) {
                            if(codResultado == Constantes.RESULT_OK){
                                new LoginTask(ActivityNuevaContrasenia.this, correo, etContrasenia.getText().toString().trim(), new LoginTask.IConsultarCorreo() {
                                    @Override
                                    public void onResult(int codResultado, String desResultado) {
                                        if(codResultado == Constantes.RESULT_OK){
                                            startActivity(new Intent(ActivityNuevaContrasenia.this,ActivityPrincipal.class));
                                            new Utils().toast(desResultado,ActivityNuevaContrasenia.this);
                                            finish();
                                        }else{
                                            tvReintentarLogin.setVisibility(View.VISIBLE);
                                            new Utils().toast("Error al intentar iniciar sesión, llame al soporte técnico 949568228",ActivityNuevaContrasenia.this);
                                        }
                                    }
                                }).execute();
                            }else {
                                tvSiguiente.setEnabled(true);
                            }
                        }
                    }).execute();
                }else{
                    new Utils().toast("La contraseña no coincide",ActivityNuevaContrasenia.this);
                }
                break;
            case R.id.tvSalir:
                finish();
                break;
            case R.id.tvReintentarLogin:
                tvReintentarLogin.setEnabled(false);
                new LoginTask(ActivityNuevaContrasenia.this, correo, etContrasenia.getText().toString().trim(), new LoginTask.IConsultarCorreo() {
                    @Override
                    public void onResult(int codResultado, String desResultado) {
                        if(codResultado == Constantes.RESULT_OK){
                            startActivity(new Intent(ActivityNuevaContrasenia.this,ActivityPrincipal.class));
                            new Utils().toast(desResultado,ActivityNuevaContrasenia.this);
                            finish();
                        }else{
                            tvReintentarLogin.setEnabled(true);
                            new Utils().toast("Error al intentar iniciar sesión, llame al soporte técnico 949568228",ActivityNuevaContrasenia.this);
                        }
                    }
                }).execute();
                break;
        }
    }
}
