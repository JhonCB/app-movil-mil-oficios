package com.studyway.miloficios.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.studyway.miloficios.R;
import com.studyway.miloficios.dao.PerfilDAO;
import com.studyway.miloficios.entities.Perfil;
import com.studyway.miloficios.mediaplayer.PlaySounds;
import com.studyway.miloficios.utils.AnimUtils;
import com.studyway.miloficios.utils.GenerateUtils;
import com.studyway.miloficios.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
/**
 * Clase creada por Jhon Coronel 18/10/2019
 * Contacto: ghoozh@gmail.com
 */
public class ActivityVerificarNumero extends AppCompatActivity {
    public final static String PUT_CODIGO = "PUT_CODIGO";
    public final static String PUT_NUMERO = "PUT_NUMERO";

    @BindView(R.id.tvSalir) TextView tvSalir;
    @BindView(R.id.tvSiguiente) TextView tvSiguiente;
    @BindView(R.id.tvNumero) TextView tvNumero;
    @BindView(R.id.etCodigoConfirmacion) EditText etCodigoConfirmacion;
    @BindView(R.id.rlMensajeContainer) RelativeLayout rlMensajeContainer;

    private Perfil perfil;

    private boolean isMensajeVisible = false;
    private String codigo;
    private String numero;
    private PlaySounds playSounds;
    private View viewSMS;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verificar_numero);
        ButterKnife.bind(this);
        playSounds = new PlaySounds(ActivityVerificarNumero.this);

        Intent intent = getIntent();
        if(intent.hasExtra(PUT_CODIGO)){
                codigo = intent.getStringExtra(PUT_CODIGO);
                numero = intent.getStringExtra(PUT_NUMERO);
                tvNumero.setText("+51 "+numero);
                new Handler().postDelayed(() ->{ //Es inseguro, se puede caer el aplicativo
                    simularMensaje(new PerfilDAO(ActivityVerificarNumero.this).getUltimaSesion().getNombreCompleto(),codigo);
                },2000);
        }else{
        new Utils().toast("Error, no se pudo solicitar código de verificaciòn",ActivityVerificarNumero.this);
        }
        cargarEntidad();
    }
    private void cargarEntidad(){
        try {
            perfil = new PerfilDAO(ActivityVerificarNumero.this).getUltimaSesion();
        }catch (Exception e){
            new Utils().toast("Error al obtener datos guardados, vuelva a intentarlo",ActivityVerificarNumero.this);
        }
    }


    @OnClick({R.id.tvSalir,R.id.tvSiguiente,R.id.tvNumeroErrado,R.id.rlMensajeContainer,R.id.llReenviarCodigoCorreo})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.tvSalir:
                finish(); //mejorar
                break;
            case R.id.tvSiguiente:
                if(etCodigoConfirmacion.getText().length() == 6){
                    if(etCodigoConfirmacion.getText().toString().equals(codigo)){
                        perfil.setTelefono(numero);
                        new PerfilDAO(ActivityVerificarNumero.this).updateEntidad(perfil);


                        startActivity(new Intent(ActivityVerificarNumero.this,ActivityTipoUsuario.class));
                        finishAffinity();
                    }else{
                        new Utils().toast("Código inválido",ActivityVerificarNumero.this);
                    }
                }else{
                    new Utils().toast("Ingrese 6 dígitos",ActivityVerificarNumero.this);
                }
                break;
            case R.id.tvNumeroErrado:
                finish();
                break;
            case R.id.rlMensajeContainer:
                isMensajeVisible = AnimUtils.animarVistaVertical(rlMensajeContainer,isMensajeVisible,200);
                break;
            case R.id.llReenviarCodigoCorreo:
                new Utils().toast("Solicitando nuevo código..",ActivityVerificarNumero.this);
                nuevoCodigo();
                break;
        }
    }
    private void nuevoCodigo(){
        codigo = GenerateUtils.obtenerNumeroAletorio(6);
        if(isMensajeVisible){
            isMensajeVisible = AnimUtils.animarVistaVertical(rlMensajeContainer,true,200);
            simularMensaje(new PerfilDAO(ActivityVerificarNumero.this).getUltimaSesion().getNombreCompleto(),codigo);
        }
    }

    private void simularMensaje(String nombre,String codigo){
        playSounds.reproducirSonidoSMS();
        etCodigoConfirmacion.setEnabled(false);
//        tvSiguiente.setEnabled(false);
//        tvSalir.setEnabled(false);

        if(viewSMS == null){
            viewSMS= getLayoutInflater().inflate(R.layout.layout_msg_verificar, null);
            rlMensajeContainer.addView(viewSMS);
        }
        TextView tvNombreUsuario = (TextView) viewSMS.findViewById(R.id.tvNombreUsuario);
        TextView tvCodigoMensaje = (TextView) viewSMS.findViewById(R.id.tvCodigoMensaje);

        tvNombreUsuario.setText("Hola "+nombre+"!");
        tvCodigoMensaje.setText(codigo);
        isMensajeVisible = AnimUtils.animarVistaVertical(rlMensajeContainer,false,200);

        etCodigoConfirmacion.setText(codigo);
    }

    private void cronometro(){
        Chronometer chronometer = new Chronometer(ActivityVerificarNumero.this);
    }

}
