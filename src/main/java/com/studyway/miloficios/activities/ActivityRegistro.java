package com.studyway.miloficios.activities;

import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.studyway.miloficios.R;
import com.studyway.miloficios.asyntask.ConsultaCorreoTask;
import com.studyway.miloficios.asyntask.ListaLocalizacionTask;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.dao.PerfilDAO;
import com.studyway.miloficios.entities.Localizacion;
import com.studyway.miloficios.entities.Perfil;
import com.studyway.miloficios.utils.Utils;
import com.studyway.miloficios.utils.ValidarUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
/**
 * Clase creada por Jhon Coronel 18/10/2019
 * Contacto: ghoozh@gmail.com
 */
public class ActivityRegistro extends BaseActivityFullScreen {

    @BindView(R.id.rlayout) RelativeLayout rlayout;
    @BindView(R.id.ivAtras) ImageView ivAtras;
    @BindView(R.id.spLocalizacion) Spinner spLocalizacion;

    @BindView(R.id.etEmail) EditText etEmail;
    @BindView(R.id.etNombre) EditText etNombre;
    @BindView(R.id.etContrasenia) EditText etContrasenia;
    @BindView(R.id.etRepetirContrasenia) EditText etRepetirContrasenia;

    @BindView(R.id.ivCheckEmail) ImageView ivCheckEmail;
    @BindView(R.id.ivShield1) ImageView ivShield1;
    @BindView(R.id.ivShield2) ImageView ivShield2;
    private Animation animation;
    private int codLocalizacion = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.bgHeader);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        rlayout = findViewById(R.id.rlayout);
        animation = AnimationUtils.loadAnimation(this,R.anim.uptodowndiagonal);
        rlayout.setAnimation(animation);

        initDataSpinnerLocalizacion();

        etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length() > 0 && ValidarUtils.validarEmail(editable.toString())) {
                    consultarExisteCorreo(editable.toString());
                }else{
                    etEmail.setError("Ingrese un correo válido");
                    ivCheckEmail.setVisibility(View.INVISIBLE);
                }
            }
        });
        etContrasenia.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length() > 0){
                    if(editable.toString().equals(etRepetirContrasenia.getText().toString())){
                        ivShield1.setVisibility(View.VISIBLE);
                        ivShield2.setVisibility(View.VISIBLE);
                    }else{
                        ivShield1.setVisibility(View.INVISIBLE);
                        ivShield2.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });
        etRepetirContrasenia.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length() > 0){
                    if(editable.toString().equals(etContrasenia.getText().toString())){
                        ivShield1.setVisibility(View.VISIBLE);
                        ivShield2.setVisibility(View.VISIBLE);
                    }else{
                        ivShield1.setVisibility(View.INVISIBLE);
                        ivShield2.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });

        spLocalizacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Localizacion localizacion = (Localizacion) adapterView.getItemAtPosition(i);
                codLocalizacion = localizacion.getCodLocalizacion();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @OnClick({R.id.ivAtras,R.id.etContrasenia,R.id.btSiguiente})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.ivAtras:
                onBackPressed();
                break;
            case R.id.etContrasenia:
//                consultarExisteCorreo(etEmail.getText().toString());
                break;
            case R.id.btSiguiente:
                if(etEmail.getError() == null){
                    if(!etNombre.getText().toString().isEmpty()) {
                        if (etRepetirContrasenia.getText().toString().equals(etContrasenia.getText().toString())) {
                            if (spLocalizacion.getSelectedItemPosition() != Spinner.INVALID_POSITION) {
                                Perfil perfil = new Perfil();
                                perfil.setNombreCompleto(etNombre.getText().toString().trim());
                                perfil.setCodLocalizacion(codLocalizacion);
                                perfil.setEmail(etEmail.getText().toString().trim());
                                perfil.setContrasenia(etContrasenia.getText().toString().trim());
                                perfil.setTipoPerfil(Perfil.STUDYWAY);
                                perfil.setUrlFotoPerfil("none");
                                if(new PerfilDAO(ActivityRegistro.this).insertarPerfil(perfil) > 0){
                                    startActivity(new Intent(ActivityRegistro.this,ActivityIngresarNumero.class));
                                }else{
                                    new Utils().toast("Error al intentar guardar la cuenta", ActivityRegistro.this);
                                }
                            } else {
                                new Utils().toast("Seleccione una ubicación válida", ActivityRegistro.this);
                            }
                        } else {
                            new Utils().toast("La contraseña no coincide", ActivityRegistro.this);
                        }
                    }else{
                        new Utils().toast("Ingrese un nombre",ActivityRegistro.this);
                    }
                }else{
                    new Utils().toast("Ingrese un correo válido",ActivityRegistro.this);
                }
                break;
        }
    }

    private void consultarExisteCorreo(String email){
        new ConsultaCorreoTask(ActivityRegistro.this, email, new ConsultaCorreoTask.IConsultarCorreo() {
            @Override
            public void onResult(int codResultado, String desResultado, int codUsuario, int codTipoUsuario, String telefono) {
                if(codResultado == Constantes.RESULT_OK){
                    etEmail.setError(null);
                    ivCheckEmail.setVisibility(View.VISIBLE);
                }else{
                    etEmail.setError(desResultado);
                    ivCheckEmail.setVisibility(View.INVISIBLE);
                }
            }
        }).execute();
    }

    private void initDataSpinnerLocalizacion(){
        new ListaLocalizacionTask(ActivityRegistro.this, new ListaLocalizacionTask.IListaLocalizacion() {
            @Override
            public void onResult(int codResultado, String desResultado, ArrayList<Localizacion> alLocalizacion) {
                if(codResultado == Constantes.RESULT_OK){
                    ArrayAdapter<Localizacion> adapter = new ArrayAdapter<Localizacion>(ActivityRegistro.this, android.R.layout.simple_spinner_dropdown_item, alLocalizacion);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spLocalizacion.setAdapter(adapter);
                }else{
                    new Utils().toast(desResultado,ActivityRegistro.this);
                }
            }
        }).execute();
    }
}
