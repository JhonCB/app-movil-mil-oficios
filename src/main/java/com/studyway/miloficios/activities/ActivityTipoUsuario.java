package com.studyway.miloficios.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.studyway.miloficios.R;
import com.studyway.miloficios.asyntask.RegistrarUsuarioTask;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.dao.PerfilDAO;
import com.studyway.miloficios.entities.Perfil;
import com.studyway.miloficios.spf.SPF;
import com.studyway.miloficios.utils.AnimUtils;
import com.studyway.miloficios.utils.GenerateUtils;
import com.studyway.miloficios.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
/**
 * Clase creada por Jhon Coronel 18/10/2019
 * Contacto: ghoozh@gmail.com
 */
public class ActivityTipoUsuario extends AppCompatActivity {
    @BindView(R.id.cvTop) CardView cvTop;
    @BindView(R.id.cvBot) CardView cvBot;
    @BindView(R.id.tvTipoUsuario) TextView tvTipoUsuario;
    @BindView(R.id.tvNombreCompleto) TextView tvNombreCompleto;

    private Perfil perfil;
    private int codRol = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_usuario);
        ButterKnife.bind(this);
        tvNombreCompleto.setText(new PerfilDAO(ActivityTipoUsuario.this).getUltimaSesion().getNombreCompleto());
        cargarEntidad();
    }

    private void cargarEntidad(){
        try {
            perfil = new PerfilDAO(ActivityTipoUsuario.this).getUltimaSesion();
        }catch (Exception e){
            new Utils().toast("Error al obtener datos guardados, vuelva a intentarlo",ActivityTipoUsuario.this);
        }
    }

    @OnClick({R.id.tvSalir, R.id.tvSiguiente, R.id.cvTop, R.id.cvBot})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvSalir:
                finish(); //mejorar
                break;
            case R.id.tvSiguiente:
                if (codRol != -1) {
//                        new Utils().toast("(Agregar metodo de registrar usuario)", ActivityTipoUsuario.this);
                        perfil.setTipoUsuario(codRol);
                        if(perfil.getTipoPerfil() == Perfil.FACEBOOK || perfil.getTipoPerfil() == Perfil.GOOGLE){ //explicacion -- 8 es STUDYWAY y FACEBOOK es 9 -- validación por el momento ..
                            perfil.setContrasenia(GenerateUtils.obtenerNumeroAletorio(4));
                        }
                        new PerfilDAO(ActivityTipoUsuario.this).updateEntidad(perfil);
                        new RegistrarUsuarioTask(ActivityTipoUsuario.this, perfil, new RegistrarUsuarioTask.IRegistrarUsuario() {
                            @Override
                            public void onResult(int codResultado, String desResultado) {
                                new Utils().toast(desResultado,ActivityTipoUsuario.this);
                                if(codResultado == Constantes.RESULT_OK){
                                    SPF.getInstance(ActivityTipoUsuario.this).guardarSesion(true);
                                    if(codRol == Perfil.CLIENTE){
                                        startActivity(new Intent(ActivityTipoUsuario.this,ActivityPrincipal.class));
                                        finishAffinity();
                                    }else if(codRol == Perfil.SOCIO){ //todo cambiar la ventana!!!
                                        startActivity(new Intent(ActivityTipoUsuario.this,ActivityPrincipal.class));
                                        finishAffinity();
                                    }else{
                                        new Utils().toast("Seleccione un tipo de usuario", ActivityTipoUsuario.this);
                                    }
                                }
                            }
                        }).execute();
                } else {
                    new Utils().toast("Seleccione un tipo de usuario", ActivityTipoUsuario.this);
                }
                break;
            case R.id.cvTop:
                codRol = 2; //Socio
                tvTipoUsuario.setText("Socio");
                AnimUtils.ScaleAnim(ActivityTipoUsuario.this,tvTipoUsuario);
                break;
            case R.id.cvBot:
                codRol = 3; //Cliente
                tvTipoUsuario.setText("Cliente");
                AnimUtils.ScaleAnim(ActivityTipoUsuario.this,tvTipoUsuario);
                break;

        }
    }
}
