package com.studyway.miloficios.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.studyway.miloficios.R;
import com.studyway.miloficios.autentification.Facebook;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.dao.PerfilDAO;
import com.studyway.miloficios.entities.Perfil;
import com.studyway.miloficios.spf.SPF;
import com.studyway.miloficios.utils.Utils;

/**
 * Clase creada por Jhon Coronel 18/10/2019
 * Contacto: ghoozh@gmail.com
 */
public class ActivityCuentaLogin extends BaseActivityNormal {


    //esto pertenece al layout activity cuenta login
    @BindView(R.id.tvNombreCompleto) TextView tvNombreCompleto;
    @BindView(R.id.tvApodo) TextView tvApodo;
    @BindView(R.id.tvCorreo) TextView tvCorreo;
    @BindView(R.id.ivGoogleFacebook) ImageView ivGoogleFacebook;
    @BindView(R.id.tvGoogleFacebook)TextView tvGoogleFacebook;
    @BindView(R.id.tvTelefono)TextView tvTelefono;
    @BindView(R.id.tvTipoUsuario)TextView tvTipoUsuario;
    @BindView(R.id.tvGoogleFacebookUser)TextView tvGoogleFacebookUser;
    @BindView(R.id.ivCuentaPerfil) ImageView ivCuentaPerfil;
    @BindView(R.id.ivElegirFoto) ImageView ivElegirFoto;

    @BindView(R.id.llEditar) LinearLayout llEditar;
    @BindView(R.id.llAceptarCancelar) LinearLayout llAceptarCancelar;

    @BindView(R.id.ivEditar) ImageView ivEditar;
    @BindView(R.id.ivAceptarEditar) ImageView ivAceptarEditar;
    @BindView(R.id.ivCancelarEditar) ImageView ivCancelarEditar;

    @BindView(R.id.etNombreCompleto) EditText etNombreCompleto;
    @BindView(R.id.etApodo) EditText etApodo;
    @BindView(R.id.etCorreo) EditText etCorreo;
    @BindView(R.id.etTelefono) EditText etTelefono;

    @BindView(R.id.spTipoUsuario) Spinner spTipoUsuario;

    Perfil perfil;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta_login);
        ButterKnife.bind(this);
        inhabilitarCampos();
        setViews();

        if (perfil.getTipoPerfil() == Perfil.GOOGLE){
            tvGoogleFacebook.setText("GOOGLE");
            ivGoogleFacebook.setImageResource(R.drawable.ic_google_32);
        }
        else if (perfil.getTipoPerfil() == Perfil.FACEBOOK){
            tvGoogleFacebook.setText("FACEBOOK");
            ivGoogleFacebook.setImageResource(R.drawable.ic_facebook_azul_32);
        }else{
            tvGoogleFacebook.setText("STUDYWAY");
            ivGoogleFacebook.setImageResource(R.drawable.ic_logo_minus); //cambiar
        }



        //registrando datos en Configuracion Opciones Usuario

//        if (new ConfigOpcionUsuarioDAO(ActivityCuentaLogin.this).getConfigOpcionUsuarioByFlagEnviado().getIdPerfil().equals("")){
//            ConfigOpcionUsuario configOpcionUsuario = new ConfigOpcionUsuario();
//            configOpcionUsuario.setNombre(perfil.getNombreCompleto());
//            configOpcionUsuario.setIdPerfil(perfil.getIdPerfil());
//            configOpcionUsuario.setCodVisualizacionMapa(0); //0 = dia //1 = noche
//            configOpcionUsuario.setNumRangoMetros(Constantes.RANGOPREDETERMIADO);
//            configOpcionUsuario.setCodSonidoHabilitado(0); //0 desabilitado //1 habilitado
//            configOpcionUsuario.setFechaHora(new Utils().getFechaHora(IDUTILS.DDHHMMSS));
//            configOpcionUsuario.setFlagEnviado(0);
//            configOpcionUsuario.setLugarGuardado(0);
//            if (new ConfigOpcionUsuarioDAO(this).insertarConfigOpcionUsuario(configOpcionUsuario) > 0)
//                new Utils().toast("Configuracion Realizada", this);
//            else
//                new Utils().toast("Error al configurar", this);
//
//        }


    }

    private void setViews(){
        perfil = new PerfilDAO(this).getUltimaSesion();
        tvNombreCompleto.setText(perfil.getNombreCompleto());
        tvApodo.setText(perfil.getApellidos());
        tvCorreo.setText(perfil.getEmail());
        tvGoogleFacebookUser.setText(perfil.getNombreCompleto());
        tvTelefono.setText(perfil.getTelefono().equals("")?"Ingrese su número":perfil.getTelefono());
        if(perfil.getTipoUsuario() == Perfil.SOCIO){
                    tvTipoUsuario.setText("Proveedor de servicio");
        }else if(perfil.getTipoUsuario() == Perfil.CLIENTE){
                    tvTipoUsuario.setText("Consumidor");
        }else {
            tvTipoUsuario.setText("No seleccionó su rol");
        }

        Utils.setFotoPerfil(this, perfil.getUrlFotoPerfil(), ivCuentaPerfil);
    }

    private boolean isEditar = false;
    @OnClick({R.id.tvCerraSession,R.id.tvBorrarCuenta,R.id.ivEditar,R.id.ivAceptarEditar,R.id.ivCancelarEditar,R.id.ivElegirFoto})
    public void setOnClikEvent(View view){
        switch (view.getId()){
            case R.id.tvCerraSession:
                if (new PerfilDAO(ActivityCuentaLogin.this).getUltimaSesion().getTipoPerfil() == Perfil.GOOGLE){
                    Utils.snackBar(view, "Sesion de Google cerrada", Constantes.RESULT_OK);
//                    new Google(this).signOut();
//                    new Google(this).revokeAccess();
                }
                else{
                    new Facebook().logOut();
                    Utils.snackBar(view, "Sesion de Facebook cerrada", Constantes.RESULT_OK);
                }
//                new ConfigOpcionUsuarioDAO(ActivityCuentaLogin.this).truncateEntidad();
                new PerfilDAO(ActivityCuentaLogin.this).truncateEntidad();
                SPF.getInstance(ActivityCuentaLogin.this).guardarSesion(false);
                startActivity(new Intent(ActivityCuentaLogin.this,ActivityLogin.class));
                finishAffinity();
            break;
            case R.id.tvBorrarCuenta: //todo: este botòn tiene que cambiar el estado a inactivo o eliminado en la bd..
                    Perfil perfil = new Perfil();
                    perfil.setIdPerfil(this.perfil.getIdPerfil());
                    perfil.setNombre("Jhon");
                    perfil.setApellidos("Coronel Bautista");
                    perfil.setNombreCompleto("Jhon Coronel Bautista");
                    perfil.setTelefono("949568229");
                    perfil.setTipoUsuario(Perfil.CLIENTE);
                    perfil.setEmail("NuevoCorreo@gmail.com");
                    perfil.setUrlFotoPerfil("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAxlBMVEUANmD///8ALlsANF8AMV0AK1kALVoAH1MAO2Ruk6duhJqxxtC80Nk9c44AMFwAKllAYH7n7/Pw9vhUcow+dY8AJVYASG/V2d4AVnk+V3cAI1VggZiQo7NlfJOzwMt5jaEAGlGivMgAFU/U4ui7w80AQWkqV3gAT3Sjsr/K197f6Ox/lqghSW2LrLxwkKUxaIYYYYGFp7hNa4Zjdo5UeJGQna18nLAuUHKeq7lkj6W1ytRThZxWfJUlUnSiv8u5wcwAC0tRZ4KQKy+sAAAIO0lEQVR4nO2cf1uruBLHgQktPa2k0EPdpbWniNSCAtp669Gt7t33/6YuSYBCf9jqw7Ma73z+MUKg+ZJkJglDFAVBEARBEARBEARBEARBEARBEARBEARBEARBEARBEARBEARBEARBEARBEARB3oXZvpxS4Ek9S+riKJ1etjWeAmd66ZLPKl0DaINQte4VJpEkvhol7CAoK0v11y2WHMa2+jyTV6K2sNWM2ARFX7OUmmS1qN2zlD3IatF9ZknLgM8u6QcB7ZXLsv8kCo140qIKPFo82QXFTHlKXdHPLupHgZ5Q8I8JhpBlB0D+FAdDA7SRSHrOZ5f0o2gxFxA9guKGPOm7Ctz6QpYJ5FooHLU+u6Qfhcx4zXVM1iW5lodMi5bw2mT2xfF4bSqy9sPMWcD8/Cp1mAD9en4+eeHuwkmvwrnB3QVdh+dJS16BzN9RR3g+hWTJ3C2YWVKogpbjSGtmkO8AmCcjZU8kTpCenUhqOPJpJI9d9R28SucxoPUugao6l21cUwxXTsaSbYZhnr1TIZ9ryIT58/9EYfzjOBcTmRWu3dZR2mOZFZ4yL6Ko8GuCCiugwi8KKqyACr8oqLACKvyiFAod7Shyj0vvz34e5cyTWeH3nx+iQlT4hckVht0T8GVWONLhKI7U3mKkH88qucLvO6bRxIrw0yl1+FtKheSOK+ycovBvltPqS7aqDwFX6LnHLY2ISYmuZXv5RHg4lO8Yx1BmPJjIl6wKs47IA7rU595RRKTNxP3sEr8XYSFP5y/pooa0gf0uhYFs3TCzNefvEdiVMGyIPr1H4Q/pGikLlBXhiM9vRmSkIipTztA2fcULbz+0zYNrNK7Ioz6cMDL4eoAh/ICdHmyBtFP0wn+zYM2hzUT5rXV7bxskhUBrZv7bZWsI/UYosFfG7rAaWoGXm5kzKdsoR49zDb3UrceuQWs68vOT91RGMyMAUlST2l0EtJWPPYlOb9dhcWYsaScUABS1mHmE+eiaTjPoH6OJXx4eS1yDDGglldGbbVtRZNvVI2t5+2CBs9jU1w69/neIgdaUxNqvL3oCyZYuDgB0OI529fk3wXeoQAHRlXQSVWrSiuaposttYrYAzVXSX2Pvtdt99ca/UuLIGdv9JmDqVDMJMTWqS7coczqgSDlPQhAEQZAC0KjjUKoV/gz0jGKBxdQdTjnLLXMUFDmz6e92zlq+jHyLggrVSTIbOWSXa02PGkAfLldxfN9ZvogpK0DS6XQGouDaohPHnufFqx99pZgvwG2nwiJ/Mv0fK5aV5cwLCbNOjQRYVjOpHBoFZUF0Y5Dcx3Hn4qXZcW2Ljotpnj8GJguG7J/zNi+jspnUWmGaf6Sl1SJOnnPVm1mGNbnm01+9szXr4DsQtKtH7OL9KWmPwvy3orHe4BIdHVSnPxH7CquqkPxRK+ENPaiw+Bo/L/iASdS31/+Dgwq1l17lqN/c12D6oj6HfWq9qdAWY8+6wp6xq1D1h7CnDg8qJEFYy9jYZgxmXzQM2w99JjUWu3ZsK+wNr+/u+aNI6EZhOBEkZKPQXz2NRVnHWU7t4TyD/2+HLMmfD1do5RfHt/wY6dXK8dzYhhptcWNvEZCgH6sxtwS7Cq/axGz/xVLic0mh8NclN52uMIdC4e+p7mj8E8xXpttk5zX+ONosr1Iq7FFXGF4upfXAyxEOhmDMVnYYNNVIW2L7io6bOQowaf5zexS6xZeVVYU3tZaUK2Q11+cSihNg8Ntt3AJXGBrVi3X+SrIHmREFQmlzy5Cmx3+suGHxZ18dEict2l6psM09WrHvQKnQPEVhL5stM8S1Ke8Ch1/0fJR8q450a1lsj8KX2UzY3NTcKJz84lxoVYWu2WrzbWq8oqHtV+gn4mpeleI1iH/ZtECFPPLefbvVJnYV2lEUcZMUiZ5Us6X2tKJwkt79nPOsN8WC8H6FBXfsQeg8+MFrPmgjN3/boQSHvYWdv0s6rLAkLK3h2wp5o2jxNwT3zS9BikJFx+uwxIddf7hXodUvm/4JCnUeABc3/1l7vu1Rf2uItKswvEvThLu1q4rCcM49Wt4cawo9sunb+xXm/nDO+yH9D3980+YVGtwdetN6Je6xNFPNpBCpRRhezR/m/S1vECIueN6u/so+hYU/5Ee0C951X5p/EyCenToSHQCoeUghswGUxx4sNeUtf+hdDllh7YdjdVjzhxBwQ91ThP1tNTfuJtf82dlJm2YzMxg/OAfGNEyhs6PQzeMuoKKQuNxq9DaFPFCHRBcXc1Fi0yV1omXTVOqMHprrkU4i+k0U36xebTbshH0K//4vddwhe872wiwVzpcXAm6MC4VEzKLWZQUf8Iej/GI+i4Ag3zHMW93EWTtvcP8l2NrGg20st6sw+j0eX4lQSm54c1taTB3XWkVh7tx85TRvoXbFODCpl6O5TcIgbx8FE/qmt+BPYHv2JKIPS4Ug9qgbn+bx1Z7w825SiwcMm/P+oNxUbhwbe1tpibdvBrylUKFiU7NiIJEr3JievQqV1royU32+bnCtBhxjHLKbW+HvQFiawLIseyJ6Ow2LH7b9q7u8NGZqWxvsM1Z6eIyyZMzWCIwuO/pU1hq/3eatvmNVLy625QOdJj02NrT957N2s4tRQIP+cjnoB+Uco5+Rj3SAnWMWYdkf0tJAGv0q3PCDMiuugtvKDbLarf3H1qeqFw83JyjM/lkuFy9a834RCDPbm58iGbA5J+LzCFTsG6mycxVUb1C/3c7FNaNJTFMzv/ErSARBEARBEARBEARBEARBEARBEARBEARBEARBEARBEARBEARBEARBEOSL8z9DYLmL89POngAAAABJRU5ErkJggg==");
                    perfil.setFlagEnviado(Constantes.FLAG_LOCAL);
                    if(new PerfilDAO(ActivityCuentaLogin.this).updatePerfil(perfil) > 0){
                        new Utils().toast("Datos actualizados",ActivityCuentaLogin.this);
                        setViews();
                    }
                break;
            case R.id.ivEditar:
                    habilitarCampos();
                    isEditar = true;
                break;
            case R.id.ivAceptarEditar:
                if(isEditar) {
                    guardarDatosUsuario();
                }
                break;
            case R.id.ivCancelarEditar:
                if (isEditar) {
                    cancelarGuardadoDatosUsuario();
                }
                break;
            case R.id.ivElegirFoto:

                break;
            default:
                break;
        }

    }

    private void habilitarCampos(){
        llEditar.setVisibility(View.INVISIBLE);
        llAceptarCancelar.setVisibility(View.VISIBLE);

        tvNombreCompleto.setVisibility(View.INVISIBLE);
        tvApodo.setVisibility(View.INVISIBLE);
        tvCorreo.setVisibility(View.INVISIBLE);
        tvTelefono.setVisibility(View.INVISIBLE);
//        tvTipoUsuario.setVisibility(View.INVISIBLE);

        etNombreCompleto.setVisibility(View.VISIBLE);
        etNombreCompleto.setText(tvNombreCompleto.getText().toString());

        etApodo.setVisibility(View.VISIBLE);
        etApodo.setText(tvApodo.getText().toString());

        etCorreo.setVisibility(View.VISIBLE);
        etCorreo.setText(tvCorreo.getText().toString());

        etTelefono.setVisibility(View.VISIBLE);
        etTelefono.setHint("Ingrese su número");
        if(tvTelefono.getText().toString().equals("Ingrese su número")){
            etTelefono.setText("");
        }else{
            etTelefono.setText(tvTelefono.getText().toString());
        }


//        spTipoUsuario.setVisibility(View.VISIBLE);
//        llenarSpinner();

    }

    private void llenarSpinner(){
        String[] tipoUsuario = {"Seleccione","Proveedor","Consumidor"};
        spTipoUsuario.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item,tipoUsuario));
        spTipoUsuario.setSelection(0);
    }

    private void inhabilitarCampos(){
        isEditar = false;

        llEditar.setVisibility(View.VISIBLE);
        llAceptarCancelar.setVisibility(View.INVISIBLE);

        tvNombreCompleto.setVisibility(View.VISIBLE);
        tvApodo.setVisibility(View.VISIBLE);
        tvCorreo.setVisibility(View.VISIBLE);
        tvTelefono.setVisibility(View.VISIBLE);
//        tvTipoUsuario.setVisibility(View.VISIBLE);

        etNombreCompleto.setVisibility(View.INVISIBLE);
        etApodo.setVisibility(View.INVISIBLE);
        etCorreo.setVisibility(View.INVISIBLE);
        etTelefono.setVisibility(View.INVISIBLE);
//        spTipoUsuario.setVisibility(View.INVISIBLE);
    }

    private int tipoUsuarioInsert = Perfil.CLIENTE;
    private void guardarDatosUsuario(){

        if(etNombreCompleto.getText().toString().isEmpty() || etApodo.getText().toString().isEmpty() || etCorreo.getText().toString().isEmpty()){
            new Utils().toast("Llene los campos vacíos",ActivityCuentaLogin.this);
        }
//        else if(!validarSpinnerTipoUsuario(spTipoUsuario.getSelectedItem().toString())){
//            new Utils().toast("Seleccione tipo de usuario",ActivityCuentaLogin.this);
//        }
        else{
            Perfil perfil = new Perfil();
            perfil.setIdPerfil(this.perfil.getIdPerfil());
            perfil.setNombre(etNombreCompleto.getText().toString());
            perfil.setApellidos(etApodo.getText().toString());
            perfil.setNombreCompleto(etNombreCompleto.getText().toString());
            perfil.setTelefono(etTelefono.getText().toString());
//            perfil.setTipoUsuario(tipoUsuarioInsert);
            perfil.setEmail(etCorreo.getText().toString());
//            perfil.setUrlFotoPerfil("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAxlBMVEUANmD///8ALlsANF8AMV0AK1kALVoAH1MAO2Ruk6duhJqxxtC80Nk9c44AMFwAKllAYH7n7/Pw9vhUcow+dY8AJVYASG/V2d4AVnk+V3cAI1VggZiQo7NlfJOzwMt5jaEAGlGivMgAFU/U4ui7w80AQWkqV3gAT3Sjsr/K197f6Ox/lqghSW2LrLxwkKUxaIYYYYGFp7hNa4Zjdo5UeJGQna18nLAuUHKeq7lkj6W1ytRThZxWfJUlUnSiv8u5wcwAC0tRZ4KQKy+sAAAIO0lEQVR4nO2cf1uruBLHgQktPa2k0EPdpbWniNSCAtp669Gt7t33/6YuSYBCf9jqw7Ma73z+MUKg+ZJkJglDFAVBEARBEARBEARBEARBEARBEARBEARBEARBEARBEARBEARBEARBEARBEARB3oXZvpxS4Ek9S+riKJ1etjWeAmd66ZLPKl0DaINQte4VJpEkvhol7CAoK0v11y2WHMa2+jyTV6K2sNWM2ARFX7OUmmS1qN2zlD3IatF9ZknLgM8u6QcB7ZXLsv8kCo140qIKPFo82QXFTHlKXdHPLupHgZ5Q8I8JhpBlB0D+FAdDA7SRSHrOZ5f0o2gxFxA9guKGPOm7Ctz6QpYJ5FooHLU+u6Qfhcx4zXVM1iW5lodMi5bw2mT2xfF4bSqy9sPMWcD8/Cp1mAD9en4+eeHuwkmvwrnB3QVdh+dJS16BzN9RR3g+hWTJ3C2YWVKogpbjSGtmkO8AmCcjZU8kTpCenUhqOPJpJI9d9R28SucxoPUugao6l21cUwxXTsaSbYZhnr1TIZ9ryIT58/9EYfzjOBcTmRWu3dZR2mOZFZ4yL6Ko8GuCCiugwi8KKqyACr8oqLACKvyiFAod7Shyj0vvz34e5cyTWeH3nx+iQlT4hckVht0T8GVWONLhKI7U3mKkH88qucLvO6bRxIrw0yl1+FtKheSOK+ycovBvltPqS7aqDwFX6LnHLY2ISYmuZXv5RHg4lO8Yx1BmPJjIl6wKs47IA7rU595RRKTNxP3sEr8XYSFP5y/pooa0gf0uhYFs3TCzNefvEdiVMGyIPr1H4Q/pGikLlBXhiM9vRmSkIipTztA2fcULbz+0zYNrNK7Ioz6cMDL4eoAh/ICdHmyBtFP0wn+zYM2hzUT5rXV7bxskhUBrZv7bZWsI/UYosFfG7rAaWoGXm5kzKdsoR49zDb3UrceuQWs68vOT91RGMyMAUlST2l0EtJWPPYlOb9dhcWYsaScUABS1mHmE+eiaTjPoH6OJXx4eS1yDDGglldGbbVtRZNvVI2t5+2CBs9jU1w69/neIgdaUxNqvL3oCyZYuDgB0OI529fk3wXeoQAHRlXQSVWrSiuaposttYrYAzVXSX2Pvtdt99ca/UuLIGdv9JmDqVDMJMTWqS7coczqgSDlPQhAEQZAC0KjjUKoV/gz0jGKBxdQdTjnLLXMUFDmz6e92zlq+jHyLggrVSTIbOWSXa02PGkAfLldxfN9ZvogpK0DS6XQGouDaohPHnufFqx99pZgvwG2nwiJ/Mv0fK5aV5cwLCbNOjQRYVjOpHBoFZUF0Y5Dcx3Hn4qXZcW2Ljotpnj8GJguG7J/zNi+jspnUWmGaf6Sl1SJOnnPVm1mGNbnm01+9szXr4DsQtKtH7OL9KWmPwvy3orHe4BIdHVSnPxH7CquqkPxRK+ENPaiw+Bo/L/iASdS31/+Dgwq1l17lqN/c12D6oj6HfWq9qdAWY8+6wp6xq1D1h7CnDg8qJEFYy9jYZgxmXzQM2w99JjUWu3ZsK+wNr+/u+aNI6EZhOBEkZKPQXz2NRVnHWU7t4TyD/2+HLMmfD1do5RfHt/wY6dXK8dzYhhptcWNvEZCgH6sxtwS7Cq/axGz/xVLic0mh8NclN52uMIdC4e+p7mj8E8xXpttk5zX+ONosr1Iq7FFXGF4upfXAyxEOhmDMVnYYNNVIW2L7io6bOQowaf5zexS6xZeVVYU3tZaUK2Q11+cSihNg8Ntt3AJXGBrVi3X+SrIHmREFQmlzy5Cmx3+suGHxZ18dEict2l6psM09WrHvQKnQPEVhL5stM8S1Ke8Ch1/0fJR8q450a1lsj8KX2UzY3NTcKJz84lxoVYWu2WrzbWq8oqHtV+gn4mpeleI1iH/ZtECFPPLefbvVJnYV2lEUcZMUiZ5Us6X2tKJwkt79nPOsN8WC8H6FBXfsQeg8+MFrPmgjN3/boQSHvYWdv0s6rLAkLK3h2wp5o2jxNwT3zS9BikJFx+uwxIddf7hXodUvm/4JCnUeABc3/1l7vu1Rf2uItKswvEvThLu1q4rCcM49Wt4cawo9sunb+xXm/nDO+yH9D3980+YVGtwdetN6Je6xNFPNpBCpRRhezR/m/S1vECIueN6u/so+hYU/5Ee0C951X5p/EyCenToSHQCoeUghswGUxx4sNeUtf+hdDllh7YdjdVjzhxBwQ91ThP1tNTfuJtf82dlJm2YzMxg/OAfGNEyhs6PQzeMuoKKQuNxq9DaFPFCHRBcXc1Fi0yV1omXTVOqMHprrkU4i+k0U36xebTbshH0K//4vddwhe872wiwVzpcXAm6MC4VEzKLWZQUf8Iej/GI+i4Ag3zHMW93EWTtvcP8l2NrGg20st6sw+j0eX4lQSm54c1taTB3XWkVh7tx85TRvoXbFODCpl6O5TcIgbx8FE/qmt+BPYHv2JKIPS4Ug9qgbn+bx1Z7w825SiwcMm/P+oNxUbhwbe1tpibdvBrylUKFiU7NiIJEr3JievQqV1royU32+bnCtBhxjHLKbW+HvQFiawLIseyJ6Ow2LH7b9q7u8NGZqWxvsM1Z6eIyyZMzWCIwuO/pU1hq/3eatvmNVLy625QOdJj02NrT957N2s4tRQIP+cjnoB+Uco5+Rj3SAnWMWYdkf0tJAGv0q3PCDMiuugtvKDbLarf3H1qeqFw83JyjM/lkuFy9a834RCDPbm58iGbA5J+LzCFTsG6mycxVUb1C/3c7FNaNJTFMzv/ErSARBEARBEARBEARBEARBEARBEARBEARBEARBEARBEARBEARBEARBEOSL8z9DYLmL89POngAAAABJRU5ErkJggg==");
            perfil.setFlagEnviado(Constantes.FLAG_LOCAL);
            if (new PerfilDAO(ActivityCuentaLogin.this).updatePerfil(perfil) > 0) {
                new Utils().toast("Datos actualizados", ActivityCuentaLogin.this);
                inhabilitarCampos();
                setViews();
            }
        }
    }

    private boolean validarSpinnerTipoUsuario(String tipoUsuario){
        switch (tipoUsuario){
            case "Seleccione":
                return false;
            case "Proveedor":
                tipoUsuarioInsert = Perfil.SOCIO;
                return true;
            case "Consumidor":
                tipoUsuarioInsert = Perfil.CLIENTE;
                return true;
                default:
                    break;
        }
        return true;
    }

    private void cancelarGuardadoDatosUsuario(){
        inhabilitarCampos();
    }

    @OnClick({R.id.ivAtras})
    public void setOnClick(View view){
        super.finish();
    }


}
