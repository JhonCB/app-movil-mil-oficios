package com.studyway.miloficios.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.studyway.miloficios.R;
import com.studyway.miloficios.dao.PerfilDAO;
import com.studyway.miloficios.entities.Perfil;
import com.studyway.miloficios.utils.GenerateUtils;
import com.studyway.miloficios.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
/**
 * Clase creada por Jhon Coronel 18/10/2019
 * Contacto: ghoozh@gmail.com
 */
public class ActivityIngresarNumero extends AppCompatActivity {
    @BindView(R.id.tvSalir) TextView tvSalir;
    @BindView(R.id.tvSiguiente) TextView tvSiguiente;
    @BindView(R.id.etNumero) EditText etNumero;

    private Perfil perfil;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingresar_numero);
        ButterKnife.bind(this);
        cargarEntidad();
    }

    private void cargarEntidad(){
        try {
            perfil = new PerfilDAO(ActivityIngresarNumero.this).getUltimaSesion();
        }catch (Exception e){
            new Utils().toast("Error al obtener datos guardados, vuelva a intentarlo",ActivityIngresarNumero.this);
        }

    }

    @OnClick({R.id.tvSalir,R.id.tvSiguiente})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.tvSalir:
                    finish(); //mejorar
                break;
            case R.id.tvSiguiente:
                if(etNumero.getText().length() == 9 && Integer.parseInt(etNumero.getText().toString()) > 900000000){
                    String numeroAletorio = GenerateUtils.obtenerNumeroAletorio(6);
                    Intent intent = new Intent(ActivityIngresarNumero.this,ActivityVerificarNumero.class);
                    intent.putExtra(ActivityVerificarNumero.PUT_CODIGO,numeroAletorio);
                    intent.putExtra(ActivityVerificarNumero.PUT_NUMERO,etNumero.getText().toString());

                    startActivity(intent);
                }else{
                    new Utils().toast("Ingrese un número válido",ActivityIngresarNumero.this);
                }
                break;
        }
    }
}
