package com.studyway.miloficios.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.studyway.miloficios.R;
import com.studyway.miloficios.asyntask.GenerarRecuperarContraseniaTask;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityGenerarRecuperarContrasenia extends AppCompatActivity {

    @BindView(R.id.etCorreoRecuperar) EditText etCorreoRecuperar;
    @BindView(R.id.tvSiguiente) TextView tvSiguiente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generar_recuperar_contrasenia);
        ButterKnife.bind(ActivityGenerarRecuperarContrasenia.this);
    }

    @OnClick({R.id.tvSiguiente,R.id.tvSalir})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.tvSiguiente:
                if(!etCorreoRecuperar.getText().toString().trim().isEmpty()){
                    tvSiguiente.setEnabled(false);
                    etCorreoRecuperar.setEnabled(false);
                    new GenerarRecuperarContraseniaTask(ActivityGenerarRecuperarContrasenia.this,etCorreoRecuperar.getText().toString().trim(), new GenerarRecuperarContraseniaTask.IGenerarRecuperacion() {
                        @Override
                        public void onResult(int codResultado, String desResultado, int codUsuario, int codigoAutogenerado) {
                            tvSiguiente.setEnabled(true);
                            etCorreoRecuperar.setEnabled(true);
                            new Utils().toast(desResultado,ActivityGenerarRecuperarContrasenia.this);
                            if(codResultado == Constantes.RESULT_OK){
                                Intent intent = new Intent(ActivityGenerarRecuperarContrasenia.this,ActivityVerificarPinRecuperacion.class);
                                intent.putExtra("codUsuario",codUsuario);
                                intent.putExtra("codigoAutogenerado",codigoAutogenerado);
                                intent.putExtra("correo",etCorreoRecuperar.getText().toString().trim());
                                startActivity(intent);
                            }
                        }
                    }).execute();
                }else{
                    new Utils().toast("Ingrese correo",ActivityGenerarRecuperarContrasenia.this);
                }
                break;
            case R.id.tvSalir:
                finish();
                break;
        }
    }
}
