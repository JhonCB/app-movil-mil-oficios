package com.studyway.miloficios.activities;

import androidx.annotation.RequiresApi;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.GraphResponse;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.studyway.miloficios.R;
import com.studyway.miloficios.asyntask.ConsultaCorreoTask;
import com.studyway.miloficios.asyntask.LoginTask;
import com.studyway.miloficios.autentification.Facebook;
import com.studyway.miloficios.autentification.FacebookButton;
import com.studyway.miloficios.autentification.Google;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.dao.PerfilDAO;
import com.studyway.miloficios.dialogs.AlertDialogLogin;
import com.studyway.miloficios.entities.Perfil;
import com.studyway.miloficios.spf.SPF;
import com.studyway.miloficios.utils.AnimUtils;
import com.studyway.miloficios.utils.JSONUtils;
import com.studyway.miloficios.utils.Utils;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
/**
 * Clase creada por Jhon Coronel 18/10/2019
 * Contacto: ghoozh@gmail.com
 */
public class ActivityLogin extends BaseActivityFullScreen {

    @BindView(R.id.btRegister) ImageButton btRegister;
    @BindView(R.id.tvLogin) TextView tvLogin;
    @BindView(R.id.ivTop) ImageView ivTop;
    @BindView(R.id.ivBottom) ImageView ivBottom;
    @BindView(R.id.btEntrar) Button btEntrar;
    @BindView(R.id.etUsuario) EditText etUsuario;
    @BindView(R.id.etContrasenia) EditText etContrasenia;

    @BindView(R.id.btLoginFacebook) Button btLoginFacebook;
    @BindView(R.id.btLoginGoogle) Button btLoginGoogle;

    public final static int REQUESTFACEBOOK = 64206;
    public final static int REQUESTGOOGLE = 10;


    //esto pertenece al layout activityCuenta
    Google loginGoogle;
    Facebook loginFacebook;
    FacebookButton loginFacebookButton;
    AlertDialogLogin alertDialogLogin;
    GoogleSignInClient googleSignInClient;

    //FACEBOOK
    // LoginButton btFacebook;
    private CallbackManager callbackManagerFacebook;
    LoginButton loginButton;

    Window window;


    //esto pertenece al layout activityCuenta
//    LoginGoogle loginGoogleActivity;
//    AlertDialogLogin alertDialogLogin;
//    GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppThemeNoActionBar);
        super.onCreate(savedInstanceState);
        this.window = getWindow();

        if(SPF.getInstance(ActivityLogin.this).getSesion()){
            startActivity(new Intent(ActivityLogin.this,ActivityPrincipal.class));
            finish();
        }
//        Utils.keyHash(ActivityLogin.this);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        FacebookSdk.sdkInitialize(ActivityLogin.this);
//        AppEventsLogger.activateApp(ActivityLogin.this);

        AnimUtils.animacionEntrada(ivTop, ivBottom, new AnimUtils.IAnimUtils() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
//        Utils.keyHash(this);

        //GOOGLE
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleSignInClient = GoogleSignIn.getClient(this, gso);

        // FACEBOOK
        callbackManagerFacebook = CallbackManager.Factory.create();
        loginButton = (LoginButton) findViewById(R.id.login_button);
        initViews();

//        loginButton.setReadPermissions("email");
//
//        loginButton.registerCallback(callbackManagerFacebook, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                // App code
//                new Utils().toast("PRIMER SUCCES",ActivityLogin.this);
//            }
//
//            @Override
//            public void onCancel() {
//                // App code
//                new Utils().toast("CANCEL",ActivityLogin.this);
//            }
//
//            @Override
//            public void onError(FacebookException exception) {
//                // App code
//                new Utils().toast("ERROR",ActivityLogin.this);
//            }
//        });

//        LoginManager.getInstance().registerCallback(callbackManagerFacebook,
//                new FacebookCallback<LoginResult>() {
//                    @Override
//                    public void onSuccess(LoginResult loginResult) {
//                        // App code
//                        GraphRequest request = GraphRequest.newMeRequest(
//                                loginResult.getAccessToken(),
//                                new GraphRequest.GraphJSONObjectCallback() {
//                                    @Override
//                                    public void onCompleted(JSONObject object, GraphResponse response) {
//                                        Log.v("LoginActivity", response.toString());
//                                        try {
//                                            // Application code
//                                            registarSesionFacebook(object);
//                                            String email = response.getJSONObject().getString("email");
//                                            new Utils().toast("Login Success \n" + email,ActivityLogin.this);
//                                        }catch(Exception e){
//                                            e.printStackTrace();
//                                        }
//                                    }
//                                });
//                        Bundle parameters = new Bundle();
//                        parameters.putString("fields", "id,name,email,picture,gender,birthday");
//                        request.setParameters(parameters);
//                        request.executeAsync();
//                    }
//
//                    @Override
//                    public void onCancel() {
//                        // App code
//                        new Utils().toast("==============Login Cancelled=============",ActivityLogin.this);
//                    }
//
//                    @Override
//                    public void onError(FacebookException exception) {
//                        // App code
//                        new Utils().toast("==============Login Error=================",ActivityLogin.this);
//                        exception.printStackTrace();
//
//                    }
//                });

    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @OnClick({R.id.btRegister,R.id.btLoginFacebook,R.id.btLoginGoogle,R.id.btEntrar,R.id.tvRecuperarContrasenia})
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btRegister:
                Intent intent   = new Intent(ActivityLogin.this,ActivityRegistro.class);
                Pair[] pairs    = new Pair[1];
                pairs[0] = new Pair<View,String>(tvLogin,"tvLogin");
                ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation(ActivityLogin.this,pairs);
                startActivity(intent,activityOptions.toBundle());
                break;
            case R.id.btLoginFacebook:
//                  loginButton.performClick();
//                loginButton.callOnClick();
//                loginFacebook.logIn();
                loginFacebookButton.logIn();
                break;
            case R.id.btLoginGoogle:
                loginGoogle.signIn();
                break;
            case R.id.btEntrar:
                //TODO FALTA AGREGA EL REGISTRO DE USUARIO
                if(!etUsuario.getText().toString().isEmpty() && !etContrasenia.getText().toString().isEmpty()){
                    new LoginTask(ActivityLogin.this, etUsuario.getText().toString(), etContrasenia.getText().toString(), new LoginTask.IConsultarCorreo() {
                        @Override
                        public void onResult(int codResultado, String desResultado) {
                            new Utils().toast(desResultado,ActivityLogin.this);
                            if(codResultado == Constantes.RESULT_OK){
                                startActivity(new Intent(ActivityLogin.this,ActivityPrincipal.class));
                            }
                        }
                    }).execute();
                }else{
                    new Utils().toast("Ingrese los campos",ActivityLogin.this);
                }

                break;
            case R.id.tvRecuperarContrasenia:
                startActivity(new Intent(ActivityLogin.this, ActivityGenerarRecuperarContrasenia.class));
                break;
        }

    }

    private void initViews(){
        //casteando button de google ----------
        loginGoogle = new Google(ActivityLogin.this, googleSignInClient, account -> {
            try {
                if (account != null){
//                    if (new PerfilDAO(ActivityLogin.this).getUltimaSesion().getIdPerfil().equals("")){
                    if(account.getEmail() != null && !account.getEmail().isEmpty()){
                        ComprobarCuentaGoogle(account.getEmail(),account);
                    }else{
                        new Utils().toast("Vuelva a intentarlo",ActivityLogin.this);
                    }
//                    }else {
//                        new Utils().toast("Ya se encuentra registrado",this);
////                        loginGoogle.signOut();
////                        loginGoogle.revokeAccess();
//                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        loginGoogle.onStart();
        loginFacebookButton = new FacebookButton(ActivityLogin.this, loginButton, callbackManagerFacebook, new FacebookButton.OnLoginFacebookCallback() {
            @Override
            public void onSuccess(JSONObject object, GraphResponse response){
                  if(JSONUtils.verificarString(object, "email") != null && !JSONUtils.verificarString(object, "email").isEmpty()){
                      ComprobarCuentaFacebook(JSONUtils.verificarString(object, "email"),object);
                  }else{
                      new Utils().toast("Vuelva a intentarlo",ActivityLogin.this);
                  }
            }

            @Override
            public void onCancel(String cancel) {

            }

            @Override
            public void onError(String error) {

            }
        });
//        loginFacebook =  new Facebook(ActivityLogin.this, callbackManagerFacebook, new Facebook.OnLoginFacebookCallback() {
//            @Override
//            public void onSuccess(JSONObject object, GraphResponse response) throws Exception {
////                new Utils().toast("onSuccess", ActivityLogin.this);
////                    comprovarSiExisteCuentaRegistrada(JSONUtils.verificarString(object,"id"));
//                  registarSesionFacebook(object);
//            }
//
//            @Override
//            public void onCancel(String cancel) {
//                new Utils().toast("Cancelaste el login con facebook", ActivityLogin.this);
//            }
//
//            @Override
//            public void onError(String error) {
//                new Utils().toast("Intentelo de nuevo!", ActivityLogin.this);
//            }
//        });

    }

    private void ComprobarCuentaFacebook(String correo, JSONObject jsonObject) {
        new ConsultaCorreoTask(ActivityLogin.this, correo, new ConsultaCorreoTask.IConsultarCorreo() {
            @Override
            public void onResult(int codResultado, String desResultado, int codUsuario, int codTipoUsuario, String telefono) {
                if(codResultado == Constantes.RESULT_OK){//1 no existe la cuenta
                    //No existe
                    try {
                        registarSesionFacebook(jsonObject,codUsuario,codTipoUsuario,telefono,false);
                    } catch (Exception e) {
                        new Utils().toast("Error de sintaxis - 406",ActivityLogin.this);
                    }
                }else if(codResultado == Constantes.RESULT_ERROR_CERO){//0 existe la cuenta
                    //Existe
                    try {
                        registarSesionFacebook(jsonObject,codUsuario,codTipoUsuario,telefono,true);
                    } catch (Exception e) {
                        new Utils().toast("Error de sintaxis - 407",ActivityLogin.this);
                    }
                }else{

                }
            }
        }).execute();
    }

    private void ComprobarCuentaGoogle(String correo, GoogleSignInAccount account){
        new ConsultaCorreoTask(ActivityLogin.this, correo, new ConsultaCorreoTask.IConsultarCorreo() {
            @Override
            public void onResult(int codResultado, String desResultado, int codUsuario, int codTipoUsuario, String telefono) {//1 no existe la cuenta
                if(codResultado == Constantes.RESULT_OK){
                    //No Existe
                    registrarSesionGoogle(account,codUsuario,codTipoUsuario,telefono,false);
                }else if(codResultado == Constantes.RESULT_ERROR_CERO){ //0 existe la cuenta
                    //Existe
                    registrarSesionGoogle(account,codUsuario,codTipoUsuario,telefono,true);
                }else{

                }
            }
        }).execute();
    }

    private void registrarSesionGoogle(GoogleSignInAccount account,int codUsuario, int codTipoUsuario, String telefono, boolean isRegistrado){
        Perfil perfil = new Perfil();
        perfil.setNombre(account.getGivenName());
        perfil.setIdPerfil(String.valueOf(codUsuario));
        perfil.setApellidos(account.getFamilyName());
        perfil.setNombreCompleto(account.getDisplayName());
        perfil.setEmail(account.getEmail());
//        perfil.setIdPerfil(account.getId());
        perfil.setTelefono(telefono);
        perfil.setTipoPerfil(Perfil.GOOGLE);
        perfil.setTipoUsuario(codTipoUsuario);
        perfil.setUrlFotoPerfil(String.valueOf(account.getPhotoUrl()));
        if(new PerfilDAO(ActivityLogin.this).insertarPerfil(perfil) > 0){
            SPF.getInstance(ActivityLogin.this).guardarKeyCodigoTemporal(codUsuario);
//            new Utils().toast("Perfil de Google Guardado",this);
            loginGoogle.signOut(); //todo está de más creo
            loginGoogle.revokeAccess();
            if(isRegistrado){
                SPF.getInstance(ActivityLogin.this).guardarSesion(true);
                if(codTipoUsuario == Perfil.CLIENTE){
                    abrirVentanaPrincipalTipoCliente();
                    finish();
                }else if(codTipoUsuario == Perfil.SOCIO){
                    abrirVentanaPrincipalTipoProveedor();
                    finish();
                }else{
                    new Utils().toast("El administrador, solo puede entrar a la web",ActivityLogin.this);
                }
            }else{
                abrirVentanaIngresarNumero();
                finish();
            }

        }
        else
            new Utils().toast("Error",this);

    }

    private void registarSesionFacebook(JSONObject object,int codUsuario, int codTipoUsuario, String telefono, boolean isRegistrado) throws Exception{
        Perfil perfil = new Perfil();
        perfil.setIdPerfil(String.valueOf(codUsuario));
//        perfil.setIdPerfil(JSONUtils.verificarString(object,"id"));
        perfil.setNombre(JSONUtils.verificarString(object,"first_name"));
        perfil.setApellidos(JSONUtils.verificarString(object,"last_name"));
        perfil.setNombreCompleto(JSONUtils.verificarString(object,"name"));
        perfil.setEmail(JSONUtils.verificarString(object, "email"));
        perfil.setTipoPerfil(Perfil.FACEBOOK);
        perfil.setTipoUsuario(codTipoUsuario);
        perfil.setTelefono(telefono);
        object.getJSONObject("picture").getJSONObject("data").getString("width");
        perfil.setUrlFotoPerfil(object.getJSONObject("picture").getJSONObject("data").getString("url"));
        if(new PerfilDAO(ActivityLogin.this).insertarPerfil(perfil) > 0){
            SPF.getInstance(ActivityLogin.this).guardarKeyCodigoTemporal(codUsuario);
            if(isRegistrado){
                SPF.getInstance(ActivityLogin.this).guardarSesion(true);
                if(codTipoUsuario == Perfil.CLIENTE){
                    abrirVentanaPrincipalTipoCliente();
                    finish();
                }else if(codTipoUsuario == Perfil.SOCIO){
                    abrirVentanaPrincipalTipoProveedor();
                    finish();
                }else{
                    new Utils().toast("El administrador, solo puede entrar a la web",ActivityLogin.this);
                }
            }else{
                abrirVentanaIngresarNumero();
                finish();
            }
        }
        else
            new Utils().toast("Error",this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUESTGOOGLE)
        {
            loginGoogle.onRequestActivity(requestCode, resultCode, data);
        }
        else if (requestCode == REQUESTFACEBOOK) {
            callbackManagerFacebook.onActivityResult(requestCode, resultCode, data);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void abrirVentanaIngresarNumero(){
        startActivity(new Intent(ActivityLogin.this,ActivityIngresarNumero.class));
    }

    private void abrirVentanaPrincipalTipoCliente(){
        startActivity(new Intent(ActivityLogin.this,ActivityPrincipal.class));
    }

    private void abrirVentanaPrincipalTipoProveedor(){
        startActivity(new Intent(ActivityLogin.this,ActivityPrincipal.class));
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(alertDialogLogin != null)
        {
            alertDialogLogin.dismiss();
//            alertDialogLogin = null;
        }
    }

//    @OnClick({R.id.ivAtras})
//    public void setOnClick(View view){
//        super.finish();
//    }

}
