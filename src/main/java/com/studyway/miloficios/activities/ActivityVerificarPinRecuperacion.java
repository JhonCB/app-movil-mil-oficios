package com.studyway.miloficios.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.studyway.miloficios.R;
import com.studyway.miloficios.asyntask.GenerarRecuperarContraseniaTask;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityVerificarPinRecuperacion extends AppCompatActivity {
    @BindView(R.id.tvCorreo) TextView tvCorreo;
    @BindView(R.id.etPinRecuperacion) EditText etPinRecuperacion;


    private int codUsuario         = 0;
    private int codigoAutogenerado = 0;
    private String correo          = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verificar_pin_recuperacion);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            codUsuario = intent.getExtras().getInt("codUsuario");
            codigoAutogenerado = intent.getExtras().getInt("codigoAutogenerado");
            correo = intent.getExtras().getString("correo");
            if(correo != null){
                tvCorreo.setText(correo);
            }
        }else{
            new Utils().toast("Error al 601",ActivityVerificarPinRecuperacion.this);
        }
    }

    @OnClick({R.id.tvSiguiente,R.id.tvSalir})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.tvSiguiente:
                if(codigoAutogenerado == Integer.parseInt(etPinRecuperacion.getText().toString().trim())){
                    Intent intent = new Intent(ActivityVerificarPinRecuperacion.this,ActivityNuevaContrasenia.class);
                    intent.putExtra("codUsuario",codUsuario);
                    intent.putExtra("codigoAutogenerado",codigoAutogenerado);
                    intent.putExtra("correo",correo);
                    startActivity(intent);
                    finish();
                }else{
                    new Utils().toast("código de recuperación inválido",ActivityVerificarPinRecuperacion.this);
                }
                break;
            case R.id.tvSalir:
                finish();
                break;
        }
    }
}
