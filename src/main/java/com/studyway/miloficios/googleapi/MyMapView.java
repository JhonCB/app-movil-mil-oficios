package com.studyway.miloficios.googleapi;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.studyway.miloficios.R;
import com.studyway.miloficios.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;


public class MyMapView extends MapView implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener
{

    public ArrayList<Marker> alMarkersGoogleMap = new ArrayList<>();
    GoogleMap googleMap = null;
    Context context = null;
    Marker mPositionMarker = null;
    private String[] arrayNombre;

    private String latitud,longitud;

    public MyMapView(Context context) {
        super(context);
    }

    public MyMapView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public MyMapView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public MyMapView(Context context, GoogleMapOptions googleMapOptions) {
        super(context, googleMapOptions);
    }

    /**
     * Inicializa el mapa, y las vistas que interceptaràn la pulsaciòn sobre el infowindow..
     */


    public void init(Context context)
    {
        this.getMapAsync(this);
        this.context = context;
    }



    @SuppressLint("MissingPermission")
    private void onInitListener() {
//        googleMap.setMyLocationEnabled(true);
//        googleMap.setOnMyLocationClickListeAner(this);
        googleMap.setOnMarkerClickListener(this);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);

    }

    /**
     * Eventos de Google
     *
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.getUiSettings().setMapToolbarEnabled(false); //desactiva las opciones de llevarte a googlemaps cuando presionas el marker.
        this.googleMap.getUiSettings().setCompassEnabled (false); //desactiva la brújula.
//        this.googleMap.getUiSettings().setAllGesturesEnabled(true);
//        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(new LatLng(-12.046490, -77.032130), 11);
//        crearMarcadorOfertante("-12.046490","-77.032130");
//        googleMap.animateCamera(location);
        onInitListener();
//        Comun.setGoogleMap(googleMap);  //lo ponemos como variable global solo para poder cambiar el color del mapa
        getMapStyle();

        crearMarcadorOfertante(latitud,longitud);
        crearMarcadorUsuario();
//        if (!InternetUtils.isGPSProvider(context))
//            context.startActivity(new Intent(context, ActivityAccesoUbicacion.class));

//        LatLng nuevoLatLngUsuario = SphericalUtil.computeOffset(Comun.getLatLngUsuario(), 1000, 270);
//        LatLng nuevoLatLngBus = SphericalUtil.computeOffset(new LatLng(Double.parseDouble(latitud), Double.parseDouble(longitud)), 200, 90);
//        adaptarZoomDeMarcadores(context,nuevoLatLngUsuario,nuevoLatLngBus, new OnCallbackMapConin() {
//            @Override
//            public void onFinishAnimacion() {
//
//            }
//
//            @Override
//            public void onCancelAnimacion() {
//
//            }
//        });
    }

    public void setViews(String latitud, String longitud){
        this.latitud = latitud;
        this.longitud = longitud;
    }

    private void crearMarcadorOfertante(String latitud, String longitud){
        if(googleMap != null) {
            if(latitud != null || longitud != null) {
                LatLng position = new LatLng(Double.parseDouble(latitud), Double.parseDouble(longitud));

                Marker markerBus = googleMap.addMarker(new MarkerOptions()
                        .position(position)
//                .draggable(true)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_usuario))
                        .zIndex(3.0f));
                markerBus.setSnippet(ETIPOSIW.MARKER_OFERTANTE.getString());
                CameraUpdate location = CameraUpdateFactory.newLatLngZoom(position, 14);
                googleMap.animateCamera(location);
            }
        }
    }

    public void crearMarcadorUsuario() {
        if (googleMap != null) {
//            if (Comun.getLatLngUsuario() != null) {
//                String nombreUsuario = "Usuario";
//                if (arrayNombre != null)
//                    nombreUsuario = arrayNombre[0];
//                if (mPositionMarker == null) {
//                    mPositionMarker = googleMap.addMarker(new MarkerOptions()
//                            .flat(false)
//                            .title("Hola " + nombreUsuario)
//                            .snippet("Estás aquí :)")
//                            .position(Comun.getLatLngUsuario()));
//                    if (SPF.getInstance(getContext()).getPreferenceString(ESPF.ESTADOMAPA.getString()).equals(Constantes.ESTILO_ABEXA)) {
//                        mPositionMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_posicion_blanco_32));
//                    } else {
//                        mPositionMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_posicion_32));
//                    }
//                    mPositionMarker.showInfoWindow();
//                    mPositionMarker.setSnippet(ETIPOSIW.MARKER_USUARIO.getString());
//                }
//                mPositionMarker.setPosition(Comun.getLatLngUsuario());
//            }
        }
    }

    public void animateCameraOferta(String latitud, String longitud, float zoom, boolean isInformacionVisible){
        if(googleMap != null) {
            if(latitud != null && longitud != null && !latitud.isEmpty() && !longitud.isEmpty()){
                LatLng latLng = new LatLng(Double.parseDouble(latitud),Double.parseDouble(longitud));
                if(isInformacionVisible){
//                    LatLng nuevoLatLng = SphericalUtil.computeOffset(latLng, 200, 270);
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
                }else{
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
                }
            }
        }
    }

    public void animateCameraOferta(String latitud, String longitud, float zoom){
        if(googleMap != null) {
            if(latitud != null && longitud != null && !latitud.isEmpty() && !longitud.isEmpty()){
                LatLng latLng = new LatLng(Double.parseDouble(latitud),Double.parseDouble(longitud));
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
            }else{
                new Utils().toast("Ubicación no encontrada",context);
            }
        }
    }

    public void animateCameraUsuario(LatLng latLng, float zoom, boolean isInformacionVisible){
        if(googleMap != null) {
            if(latitud != null || longitud != null){
//                if(isInformacionVisible){
//                    LatLng nuevoLatLng = SphericalUtil.computeOffset(latLng, 200, 270);
//                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(nuevoLatLng, zoom));
//                }else{
//                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
//                }
            }
        }
    }



    // TODO ----------------------------------  ESTO SE ESTA AGREGANDO

    /**
     * metodo para poder animar la camara del mapa
     * @param latLngPosicionInical
     * @param zoom
     */
    public void animateCamera(LatLng latLngPosicionInical, float zoom)
    {
        if (latLngPosicionInical == null || latLngPosicionInical.latitude == 0)
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-12.042948, -77.063263), 11.0f));
        else
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLngPosicionInical, zoom));
    }

    // TODO ----------------------------------  HASTA AQUI
    public void agregarMarkerTarjetas(LatLng latLngPosicionInical)
    {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        if(marker.getSnippet().equals(ETIPOSIW.MARKER_USUARIO.getString())){

        }else if(marker.getSnippet().equals(ETIPOSIW.MARKER_OFERTANTE.getString())){

        }

        return true;
    }

    public void clearMap()
    {
        googleMap.clear();
    }

    /**
     * ESto me permite cargar el stylo del mapa segun el usuario haya seleccionado
     */
    private void getMapStyle()
    {
        if(googleMap != null){
            googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.style_white_map));
        }
    }

    public void setOnCreate(Bundle savedInstanceState){
        this.onCreate(savedInstanceState);
    }

    /**
     * guarda el estado ùltimo del saveInstance
     * @param outState
     */
    public void setOnSavedInstance(Bundle outState){
        this.onSaveInstanceState(outState);
    }

    /**
     * remove markers all && por snippet
     * caso : cuando se cancela la ruta cargada ..
     */
    public void clearAllMarkersAndPolylines(){
        if(googleMap != null)
            googleMap.clear();
    }

//
//    public void clearAllTiendasContactless(){
//        this.clearAllMarkersBySnippet(ETIPOSIW.INFO_UBICACION_DISPOSITIVO);
//    }


    /**
     * elimina los marcadores comprobando si coincide con el snippet
     * recibido..
     * @param etiposiw ETIPOSIW
     */
    private void clearAllMarkersBySnippet(ETIPOSIW etiposiw){
        ArrayList<Integer> alIndexPorRemover = new ArrayList<Integer>();
        for (int i = 0; i < alMarkersGoogleMap.size(); i++) {
            Marker marker = alMarkersGoogleMap.get(i);
            if(marker.getSnippet().equals(etiposiw.getString())){
                //alMarkersGoogleMap.remove(marker);  // todo EN PRUEBA USO DE FOREACH  .. cuando se utiliza un for (i) puede remover y realizarìa un salto de iteraciòn.. CUIDADO
                marker.remove();
                alIndexPorRemover.add(i);
            }
        }
//
//        for (Marker marker : alMarkersGoogleMap) {
//            if(marker.getSnippet().equals(etiposiw.getString())){
//                alMarkersGoogleMap.remove(marker);  // todo EN PRUEBA USO DE FOREACH  .. cuando se utiliza un for (i) puede remover y realizarìa un salto de iteraciòn.. CUIDADO
//                marker.remove();
//
//            }
//        }

        Collections.sort(alIndexPorRemover, Collections.reverseOrder());    // reordenamos para que la eliminación de items no deletee items inexistentes..
        for (int idPorRemover : alIndexPorRemover) {
            try{
                alMarkersGoogleMap.remove(idPorRemover); // removemos recién la lista..
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public OnCallbackMapConin onCallbackMapConin;
    public interface OnCallbackMapConin {
        void onFinishAnimacion();
        void onCancelAnimacion();
    }
    public void adaptarZoomDeMarcadores(Context context,LatLng latLngUsuario, LatLng latLngBus, OnCallbackMapConin onCallbackMapConin)
    {
        if(googleMap != null) {
            if (latitud != null || longitud != null) {
                this.onCallbackMapConin = onCallbackMapConin;
                new Utils().adaptarZoomMarcadores(context, latLngUsuario, latLngBus, googleMap, new Utils.OnCallbackMap() {
                    @Override
                    public void onFinishAnimacion() {
                        onCallbackMapConin.onFinishAnimacion();
                    }

                    @Override
                    public void onCancelAnimacion() {
                        onCallbackMapConin.onCancelAnimacion();
                    }
                });
            }
        }
    }


    /**
     * enum para controlar los tipos de marcadores
     */
    public enum ETIPOSIW{
        MARKER_USUARIO   ("marker_usuario"),
        MARKER_OFERTANTE("marker_ofertante");

        String infoParadero = "";
        ETIPOSIW(String s){
            this.infoParadero = s;
        }

        public String getString(){
            return infoParadero;
        }
    }



}
