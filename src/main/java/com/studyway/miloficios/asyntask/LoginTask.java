package com.studyway.miloficios.asyntask;

import android.content.Context;
import android.os.AsyncTask;

import com.studyway.miloficios.R;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.dao.PerfilDAO;
import com.studyway.miloficios.entities.Perfil;
import com.studyway.miloficios.retrofit.ConnectionRetrofite;
import com.studyway.miloficios.retrofit.IRoutesRetrofit;
import com.studyway.miloficios.spf.SPF;
import com.studyway.miloficios.utils.JSONUtils;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Clase creada por Jhon Coronel 18/10/2019
 * Contacto: ghoozh@gmail.com
 */
public class LoginTask extends AsyncTask<String, Void, String>
{
    private final static String nomArrayJSON = "login";

    private Context context;
    private Retrofit retrofit;
    private String usuario;
    private String contrasenia;

    private IConsultarCorreo IConsultarCorreo;
    public interface IConsultarCorreo {
        void onResult(int codResultado, String desResultado);
    }

    public LoginTask(Context context, String usuario,String contrasenia, IConsultarCorreo IConsultarCorreo) {
        this.context = context;
        this.usuario = usuario;
        this.contrasenia = contrasenia;
        this.retrofit = ConnectionRetrofite.APIJHONHTTP(Constantes.getHostJhon());
        this.IConsultarCorreo = IConsultarCorreo;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            IRoutesRetrofit iRoutesRetrofit = retrofit.create(IRoutesRetrofit.class);
            (iRoutesRetrofit.Login(usuario,contrasenia)).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    try {
                        if (response.isSuccessful() && response.body() != null){

                            JSONObject jsonObject = JSONUtils.verificarJSONObject(response.body());
                            JSONObject jsonObject2 = jsonObject.getJSONObject(nomArrayJSON);

                            int codResultado = JSONUtils.verificarInteger(jsonObject, COLNAMES.CODRESULTADO.getString());
                            String desResultado = JSONUtils.verificarString(jsonObject, COLNAMES.DESRESULTADO.getString());
                            int codigoUsuario = JSONUtils.verificarInteger(jsonObject2, COLNAMES.CODUSUARIO.getString());

                            Perfil perfil = new Perfil();
                            perfil.setNombre(JSONUtils.verificarString(jsonObject2, COLNAMES.NOMBRES.getString()));
                            perfil.setIdPerfil(String.valueOf(codigoUsuario));
                            perfil.setApellidos("");
                            perfil.setNombreCompleto(JSONUtils.verificarString(jsonObject2, COLNAMES.NOMBRES.getString()));
                            perfil.setEmail(JSONUtils.verificarString(jsonObject2, COLNAMES.CORREO.getString()));
                            perfil.setTelefono(JSONUtils.verificarString(jsonObject2, COLNAMES.TELEFONO.getString()));
                            perfil.setTipoPerfil(Perfil.STUDYWAY);
                            perfil.setTipoUsuario(JSONUtils.verificarInteger(jsonObject2, COLNAMES.CODROL.getString()));
                            perfil.setUrlFotoPerfil(String.valueOf(JSONUtils.verificarString(jsonObject2, COLNAMES.URLFOTO.getString())));

                            new PerfilDAO(context).insertarPerfil(perfil);
                            SPF.getInstance(context).guardarKeyCodigoTemporal(codigoUsuario);
                            SPF.getInstance(context).guardarSesion(true);

                            IConsultarCorreo.onResult(codResultado,desResultado);
                        }else{
                            IConsultarCorreo.onResult(Constantes.RESULT_WARNING,context.getString(R.string.body_nulo));
                        }

                    } catch (Exception e) {
                        IConsultarCorreo.onResult(Constantes.RESULT_ERROR_CERO,context.getString(R.string.error_sintaxis));
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    IConsultarCorreo.onResult(Constantes.RESULT_ERROR_CERO,context.getString(R.string.error_conexion));
                }
            });
        }catch (Exception e){
            IConsultarCorreo.onResult(Constantes.RESULT_ERROR_CERO,context.getString(R.string.error_sintaxis));
        }


        return null;
    }

    public enum COLNAMES
    {
        CODRESULTADO          ("codResultado"),
        DESRESULTADO          ("desResultado"),
        CODUSUARIO            ("codUsuario"),
        CODROL                ("idRol"),
        NOMROL                ("nomRol"),
        NOMBRES               ("nombres"),
        CORREO                ("correo"),
        TELEFONO              ("telefono"),
        CODLOCALIZACION       ("idLocalizacion"),
        ACTIVO                ("activo"),
        URLFOTO               ("urlFoto"),
        FECHANACIMIENTO       ("fechaNacimiento");
        String s;
        COLNAMES(String s)
        {
            this.s = s;
        }

        public String getString()
        {
            return s;
        }
    }

}
