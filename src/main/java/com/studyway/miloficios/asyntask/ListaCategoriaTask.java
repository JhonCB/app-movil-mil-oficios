package com.studyway.miloficios.asyntask;

import android.content.Context;
import android.os.AsyncTask;

import com.studyway.miloficios.R;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.entities.Categoria;
import com.studyway.miloficios.entities.Servicio;
import com.studyway.miloficios.retrofit.ConnectionRetrofite;
import com.studyway.miloficios.retrofit.IRoutesRetrofit;
import com.studyway.miloficios.utils.JSONUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Clase creada por Jhon Coronel 18/10/2019
 * Contacto: ghoozh@gmail.com
 */
public class ListaCategoriaTask extends AsyncTask<String, Void, String>
{
    private final static String nomArrayJSON = "listaCategoria";
    private Context context;
    private Retrofit retrofit;
    private IListaCategoria IListaCategoria;

    public interface IListaCategoria {
        void onResult(int codResultado, String desResultado, ArrayList<Categoria> alCategoria);
    }

    public ListaCategoriaTask(Context context, IListaCategoria IListaCategoria) {
        this.context = context;
        this.retrofit = ConnectionRetrofite.APIJHONHTTP(Constantes.getHostJhon());
        this.IListaCategoria = IListaCategoria;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            IRoutesRetrofit iRoutesRetrofit = retrofit.create(IRoutesRetrofit.class);
            (iRoutesRetrofit.ListarCategoria()).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    try {
                        if (response.isSuccessful() && response.body() != null){

                            JSONObject jsonObject = JSONUtils.verificarJSONObject(response.body());

                            int codResultado = JSONUtils.verificarInteger(jsonObject, COLNAMES.CODRESULTADO.getString());
                            String desResultado = JSONUtils.verificarString(jsonObject, COLNAMES.DESRESULTADO.getString());

                            JSONArray jsonArray = jsonObject.getJSONArray(nomArrayJSON);
                            ArrayList<Categoria> alCategoria = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonLocalizacion = jsonArray.getJSONObject(i);
                                Categoria categoria = new Categoria();
                                categoria.setCodCategoria(JSONUtils.verificarInteger(jsonLocalizacion, COLNAMES.CODIGO.getString()));
                                categoria.setNomCategoria(JSONUtils.verificarString(jsonLocalizacion, COLNAMES.NOMBRECCATEGORIA.getString()));
                                alCategoria.add(categoria);
                            }

                            IListaCategoria.onResult(codResultado,desResultado,alCategoria);
                        }else{
                            IListaCategoria.onResult(Constantes.RESULT_WARNING,context.getString(R.string.body_nulo),new ArrayList<>());
                        }

                    } catch (Exception e) {
                        IListaCategoria.onResult(Constantes.RESULT_ERROR_CERO,context.getString(R.string.error_sintaxis), new ArrayList<>());
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    IListaCategoria.onResult(Constantes.RESULT_ERROR_CERO,context.getString(R.string.error_conexion), new ArrayList<>());
                }
            });
        }catch (Exception e){
            IListaCategoria.onResult(Constantes.RESULT_ERROR_CERO,context.getString(R.string.error_sintaxis),new ArrayList<>());
        }


        return null;
    }

    public enum COLNAMES
    {
        CODRESULTADO          ("codResultado"),
        DESRESULTADO          ("desResultado"),
        CODIGO                ("codCategoria"),
        NOMBRECCATEGORIA      ("nomCategoria");

        String s;
        COLNAMES(String s)
        {
            this.s = s;
        }

        public String getString()
        {
            return s;
        }
    }

}
