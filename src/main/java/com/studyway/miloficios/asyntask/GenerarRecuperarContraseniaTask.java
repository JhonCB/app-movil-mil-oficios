package com.studyway.miloficios.asyntask;

import android.content.Context;
import android.os.AsyncTask;

import com.studyway.miloficios.R;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.retrofit.ConnectionRetrofite;
import com.studyway.miloficios.retrofit.IRoutesRetrofit;
import com.studyway.miloficios.utils.JSONUtils;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Clase creada por Jhon Coronel 18/10/2019
 * Contacto: ghoozh@gmail.com
 */
public class GenerarRecuperarContraseniaTask extends AsyncTask<String, Void, String>
{
    private final static String nomJson = "recuperarContrasenia";

    private Context context;
    private Retrofit retrofit;
    private String email;

    private IGenerarRecuperacion IGenerarRecuperacion;
    public interface IGenerarRecuperacion {
        void onResult(int codResultado, String desResultado, int codUsuario, int codigoAutogenerado);
    }

    public GenerarRecuperarContraseniaTask(Context context, String email, IGenerarRecuperacion IGenerarRecuperacion) {
        this.context = context;
        this.email = email;
        this.retrofit = ConnectionRetrofite.APIJHONHTTP(Constantes.getHostJhon());
        this.IGenerarRecuperacion = IGenerarRecuperacion;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            IRoutesRetrofit iRoutesRetrofit = retrofit.create(IRoutesRetrofit.class);
            (iRoutesRetrofit.GenerarRecuperacionContrasenia(email)).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    try {
                        if (response.isSuccessful() && response.body() != null){

                            JSONObject jsonObject = JSONUtils.verificarJSONObject(response.body());
                            JSONObject jsonObjectRe = jsonObject.getJSONObject(nomJson);

                            int codResultado = JSONUtils.verificarInteger(jsonObject, COLNAMES.CODRESULTADO.getString());
                            String desResultado = JSONUtils.verificarString(jsonObject, COLNAMES.DESRESULTADO.getString());
                            int codUsuario = JSONUtils.verificarInteger(jsonObjectRe, COLNAMES.CODUSUARIO.getString());
                            int codigoAutogenerado = JSONUtils.verificarInteger(jsonObjectRe, COLNAMES.CODRECUPERACION.getString());

                            IGenerarRecuperacion.onResult(codResultado,desResultado,codUsuario,codigoAutogenerado);
                        }else{
                            IGenerarRecuperacion.onResult(Constantes.RESULT_WARNING,context.getString(R.string.body_nulo),0,0);
                        }

                    } catch (Exception e) {
                        IGenerarRecuperacion.onResult(Constantes.RESULT_ERROR,context.getString(R.string.error_sintaxis),0,0);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    IGenerarRecuperacion.onResult(Constantes.RESULT_ERROR_CONNECTION,context.getString(R.string.error_conexion),0,0);
                }
            });
        }catch (Exception e){
            IGenerarRecuperacion.onResult(Constantes.RESULT_ERROR,context.getString(R.string.error_sintaxis),0,0);
        }


        return null;
    }

    public enum COLNAMES
    {
        CODRESULTADO          ("codResultado"),
        DESRESULTADO          ("desResultado"),
        CODUSUARIO            ("codUsuario"),
        CODRECUPERACION       ("codigoAutogenerado");
        String s;
        COLNAMES(String s)
        {
            this.s = s;
        }

        public String getString()
        {
            return s;
        }
    }

}
