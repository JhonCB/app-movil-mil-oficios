package com.studyway.miloficios.asyntask;

import android.content.Context;
import android.os.AsyncTask;

import com.studyway.miloficios.R;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.entities.Servicio;
import com.studyway.miloficios.retrofit.ConnectionRetrofite;
import com.studyway.miloficios.retrofit.IRoutesRetrofit;
import com.studyway.miloficios.utils.JSONUtils;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Clase creada por Jhon Coronel 18/10/2019
 * Contacto: ghoozh@gmail.com
 */
public class GenerarSolicitudServicioTask extends AsyncTask<String, Void, String>
{
//    private final static String nomArrayJSON = "UsuarioLista";

    private Context context;
    private Retrofit retrofit;
    private int  codServicio;
    private double  costo;
    private double  cotizacion;
    private int  codUsuario;

    private IGenerarSolicitudServicio IGenerarSolicitudServicio;
    public interface IGenerarSolicitudServicio {
        void onResult(int codResultado, String desResultado);
    }

    public GenerarSolicitudServicioTask(Context context, int codServicio, double  costo,double  cotizacion, int  codUsuario, IGenerarSolicitudServicio IGenerarSolicitudServicio) {
        this.context = context;
        this.codServicio = codServicio;
        this.costo = costo;
        this.cotizacion = cotizacion;
        this.codUsuario = codUsuario;
        this.retrofit = ConnectionRetrofite.APIJHONHTTP(Constantes.getHostJhon());
        this.IGenerarSolicitudServicio = IGenerarSolicitudServicio;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            IRoutesRetrofit iRoutesRetrofit = retrofit.create(IRoutesRetrofit.class);
            (iRoutesRetrofit.GenerarSolicitudServicio(codServicio,costo,cotizacion,codUsuario)).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    try {
                        if (response.isSuccessful() && response.body() != null){

                            JSONObject jsonObject = JSONUtils.verificarJSONObject(response.body());

                            int codResultado = JSONUtils.verificarInteger(jsonObject, COLNAMES.CODRESULTADO.getString());
                            String desResultado = JSONUtils.verificarString(jsonObject, COLNAMES.DESRESULTADO.getString());

                            IGenerarSolicitudServicio.onResult(codResultado,desResultado);
                        }else{
                            IGenerarSolicitudServicio.onResult(Constantes.RESULT_WARNING,context.getString(R.string.body_nulo));
                        }

                    } catch (Exception e) {
                        IGenerarSolicitudServicio.onResult(Constantes.RESULT_ERROR_CERO,context.getString(R.string.error_sintaxis));
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    IGenerarSolicitudServicio.onResult(Constantes.RESULT_ERROR_CERO,context.getString(R.string.error_conexion));
                }
            });
        }catch (Exception e){
            IGenerarSolicitudServicio.onResult(Constantes.RESULT_ERROR_CERO,context.getString(R.string.error_sintaxis));
        }


        return null;
    }

    public enum COLNAMES
    {
        CODRESULTADO          ("codResultado"),
        DESRESULTADO          ("desResultado");
        String s;
        COLNAMES(String s)
        {
            this.s = s;
        }

        public String getString()
        {
            return s;
        }
    }

}
