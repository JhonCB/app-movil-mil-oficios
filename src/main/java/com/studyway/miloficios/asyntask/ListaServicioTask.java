package com.studyway.miloficios.asyntask;

import android.content.Context;
import android.os.AsyncTask;

import com.studyway.miloficios.R;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.entities.Servicio;
import com.studyway.miloficios.retrofit.ConnectionRetrofite;
import com.studyway.miloficios.retrofit.IRoutesRetrofit;
import com.studyway.miloficios.utils.JSONUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Clase creada por Jhon Coronel 18/10/2019
 * Contacto: ghoozh@gmail.com
 */
public class ListaServicioTask extends AsyncTask<String, Void, String>
{
    private final static String nomArrayJSON = "listaServicio";
    private Context context;
    private Retrofit retrofit;
    private IListaServicio IListaServicio;

    public interface IListaServicio {
        void onResult(int codResultado, String desResultado, ArrayList<Servicio> alServicio);
    }

    public ListaServicioTask(Context context, IListaServicio IListaServicio) {
        this.context = context;
        this.retrofit = ConnectionRetrofite.APIJHONHTTP(Constantes.getHostJhon());
        this.IListaServicio = IListaServicio;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            IRoutesRetrofit iRoutesRetrofit = retrofit.create(IRoutesRetrofit.class);
            (iRoutesRetrofit.ListarServicio()).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    try {
                        if (response.isSuccessful() && response.body() != null){

                            JSONObject jsonObject = JSONUtils.verificarJSONObject(response.body());

                            int codResultado = JSONUtils.verificarInteger(jsonObject, COLNAMES.CODRESULTADO.getString());
                            String desResultado = JSONUtils.verificarString(jsonObject, COLNAMES.DESRESULTADO.getString());

                            JSONArray jsonArray = jsonObject.getJSONArray(nomArrayJSON);
                            ArrayList<Servicio> alServicio = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonLocalizacion = jsonArray.getJSONObject(i);
                                Servicio servicio = new Servicio();
                                servicio.setCodServicio(JSONUtils.verificarInteger(jsonLocalizacion, COLNAMES.CODIGO.getString()));
                                servicio.setNombreUsuPub(JSONUtils.verificarString(jsonLocalizacion, COLNAMES.NOMBREUSU.getString()));
                                servicio.setCalificacion(JSONUtils.verificarInteger(jsonLocalizacion, COLNAMES.CALIFICACION.getString()));
                                servicio.setFechaInicio(JSONUtils.verificarString(jsonLocalizacion, COLNAMES.FECHAINICIO.getString()));
                                servicio.setFechaFin(JSONUtils.verificarString(jsonLocalizacion, COLNAMES.FECHAFIN.getString()));
                                servicio.setEstado(JSONUtils.verificarString(jsonLocalizacion, COLNAMES.ESTADO.getString()));
                                servicio.setDescripcionServicio(JSONUtils.verificarString(jsonLocalizacion, COLNAMES.DESCRIPCIONSERVICIO.getString()));
                                servicio.setDescripcion(JSONUtils.verificarString(jsonLocalizacion, COLNAMES.DESCRIPCION.getString()));
                                servicio.setUbicacion(JSONUtils.verificarString(jsonLocalizacion, COLNAMES.UBICACION.getString()));
                                servicio.setUrlFoto(JSONUtils.verificarString(jsonLocalizacion, COLNAMES.URLFOTO.getString()));
                                servicio.setLatitud(JSONUtils.verificarString(jsonLocalizacion, COLNAMES.LATITUD.getString()));
                                servicio.setLongitud(JSONUtils.verificarString(jsonLocalizacion, COLNAMES.LONGITUD.getString()));
                                alServicio.add(servicio);
                            }

                            IListaServicio.onResult(codResultado,desResultado,alServicio);
                        }else{
                            IListaServicio.onResult(Constantes.RESULT_WARNING,context.getString(R.string.body_nulo),new ArrayList<>());
                        }

                    } catch (Exception e) {
                        IListaServicio.onResult(Constantes.RESULT_ERROR_CERO,context.getString(R.string.error_sintaxis), new ArrayList<>());
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    IListaServicio.onResult(Constantes.RESULT_ERROR_CERO,context.getString(R.string.error_conexion), new ArrayList<>());
                }
            });
        }catch (Exception e){
            IListaServicio.onResult(Constantes.RESULT_ERROR_CERO,context.getString(R.string.error_sintaxis),new ArrayList<>());
        }


        return null;
    }

    public enum COLNAMES
    {
        CODRESULTADO          ("codResultado"),
        DESRESULTADO          ("desResultado"),
        CODIGO                ("codServicio"),
        NOMBREUSU             ("nombre"),
        URLFOTO               ("urlFoto"),
        CALIFICACION          ("calificacion"),
        FECHAINICIO           ("fechaInicio"),
        FECHAFIN              ("fechaFin"),
        ESTADO                ("estado"),
        DESCRIPCIONSERVICIO   ("descripcionServicio"),
        DESCRIPCION           ("descripcion"),
        UBICACION             ("ubicacion"),
        LATITUD               ("latitud"),
        LONGITUD              ("longitud");
        String s;
        COLNAMES(String s)
        {
            this.s = s;
        }

        public String getString()
        {
            return s;
        }
    }

}
