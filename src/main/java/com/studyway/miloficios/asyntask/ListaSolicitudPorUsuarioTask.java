package com.studyway.miloficios.asyntask;

import android.content.Context;
import android.os.AsyncTask;

import com.studyway.miloficios.R;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.entities.Categoria;
import com.studyway.miloficios.entities.SolicitudServicio;
import com.studyway.miloficios.retrofit.ConnectionRetrofite;
import com.studyway.miloficios.retrofit.IRoutesRetrofit;
import com.studyway.miloficios.utils.JSONUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Clase creada por Jhon Coronel 18/10/2019
 * Contacto: ghoozh@gmail.com
 */
public class ListaSolicitudPorUsuarioTask extends AsyncTask<String, Void, String>
{
    private final static String nomArrayJSON = "listaSolicitud";
    private Context context;
    private Retrofit retrofit;
    private IListaSolicitudPorUsuario IListaSolicitudPorUsuario;
    private int codUsuario;

    public interface IListaSolicitudPorUsuario {
        void onResult(int codResultado, String desResultado, ArrayList<SolicitudServicio> alSolicitudServicio);
    }

    public ListaSolicitudPorUsuarioTask(Context context, int codUsuario, IListaSolicitudPorUsuario IListaSolicitudPorUsuario) {
        this.context = context;
        this.codUsuario = codUsuario;
        this.retrofit = ConnectionRetrofite.APIJHONHTTP(Constantes.getHostJhon());
        this.IListaSolicitudPorUsuario = IListaSolicitudPorUsuario;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            IRoutesRetrofit iRoutesRetrofit = retrofit.create(IRoutesRetrofit.class);
            (iRoutesRetrofit.ListarSolicitudServicioPorUsuario(codUsuario)).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    try {
                        if (response.isSuccessful() && response.body() != null){

                            JSONObject jsonObject = JSONUtils.verificarJSONObject(response.body());

                            int codResultado = JSONUtils.verificarInteger(jsonObject, COLNAMES.CODRESULTADO.getString());
                            String desResultado = JSONUtils.verificarString(jsonObject, COLNAMES.DESRESULTADO.getString());

                            JSONArray jsonArray = jsonObject.getJSONArray(nomArrayJSON);
                            ArrayList<SolicitudServicio> alSolicitud = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                SolicitudServicio solicitud = new SolicitudServicio();
                                solicitud.setCodServicio(JSONUtils.verificarInteger(jsonObject1, COLNAMES.IDSERVICIO.getString()));
                                solicitud.setCosto(JSONUtils.verificarString(jsonObject1, COLNAMES.COSTO.getString()));
                                solicitud.setCotizacion(JSONUtils.verificarString(jsonObject1, COLNAMES.COTIZACION.getString()));
                                solicitud.setEstado(JSONUtils.verificarInteger(jsonObject1, COLNAMES.ESTADO.getString()));
                                solicitud.setFecha(JSONUtils.verificarString(jsonObject1, COLNAMES.FECHAHORA.getString()));
                                solicitud.setCategoria(JSONUtils.verificarString(jsonObject1, COLNAMES.CATEGORIA.getString()));
                                solicitud.setUrlFoto(JSONUtils.verificarString(jsonObject1, COLNAMES.URLFOTO.getString()));
                                solicitud.setNomOfertante(JSONUtils.verificarString(jsonObject1, COLNAMES.NOMOFERTANTE.getString()));
                                solicitud.setDesServicio(JSONUtils.verificarString(jsonObject1, COLNAMES.DESSERVICIO.getString()));
                                solicitud.setUbicacion(JSONUtils.verificarString(jsonObject1, COLNAMES.UBICACION.getString()));
                                alSolicitud.add(solicitud);
                            }

                            IListaSolicitudPorUsuario.onResult(codResultado,desResultado,alSolicitud);
                        }else{
                            IListaSolicitudPorUsuario.onResult(Constantes.RESULT_WARNING,context.getString(R.string.body_nulo),new ArrayList<>());
                        }

                    } catch (Exception e) {
                        IListaSolicitudPorUsuario.onResult(Constantes.RESULT_ERROR_CERO,context.getString(R.string.error_sintaxis), new ArrayList<>());
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    IListaSolicitudPorUsuario.onResult(Constantes.RESULT_ERROR_CERO,context.getString(R.string.error_conexion), new ArrayList<>());
                }
            });
        }catch (Exception e){
            IListaSolicitudPorUsuario.onResult(Constantes.RESULT_ERROR_CERO,context.getString(R.string.error_sintaxis),new ArrayList<>());
        }


        return null;
    }

    public enum COLNAMES
    {
        CODRESULTADO          ("codResultado"),
        DESRESULTADO          ("desResultado"),
        IDSERVICIO            ("idServicio"),
        COSTO                 ("costo"),
        COTIZACION            ("cotizacion"),
        ESTADO                ("estado"),
        FECHAHORA             ("fecha"),
        URLFOTO               ("urlFoto"),
        CATEGORIA             ("categoria"),
        NOMOFERTANTE          ("nomOfertante"),
        DESSERVICIO           ("desServicio"),
        UBICACION             ("ubicacion");

        String s;
        COLNAMES(String s)
        {
            this.s = s;
        }

        public String getString()
        {
            return s;
        }
    }

}
