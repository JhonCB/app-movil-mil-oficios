package com.studyway.miloficios.asyntask;

import android.content.Context;
import android.os.AsyncTask;

import com.studyway.miloficios.R;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.dao.PerfilDAO;
import com.studyway.miloficios.entities.Perfil;
import com.studyway.miloficios.enums.IDUTILS;
import com.studyway.miloficios.retrofit.ConnectionRetrofite;
import com.studyway.miloficios.retrofit.IRoutesRetrofit;
import com.studyway.miloficios.spf.SPF;
import com.studyway.miloficios.utils.JSONUtils;
import com.studyway.miloficios.utils.Utils;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Clase creada por Jhon Coronel 18/10/2019
 * Contacto: ghoozh@gmail.com
 */
public class RegistrarUsuarioTask extends AsyncTask<String, Void, String>
{
//    private final static String nomArrayJSON = "UsuarioLista";

    private Context context;
    private Retrofit retrofit;
    private Perfil perfil;

    private IRegistrarUsuario IRegistrarUsuario;
    public interface IRegistrarUsuario {
        void onResult(int codResultado, String desResultado);
    }

    public RegistrarUsuarioTask(Context context, Perfil perfil, IRegistrarUsuario IRegistrarUsuario) {
        this.context = context;
        this.perfil = perfil;
        this.retrofit = ConnectionRetrofite.APIJHONHTTP(Constantes.getHostJhon());
        this.IRegistrarUsuario = IRegistrarUsuario;
    }

    @Override
    protected String doInBackground(String... strings) {
        /**
         *                                   @Query("Nombre") String Nombre,
         *                                   @Query("Email") String Email,
         *                                   @Query("Contrasenia") String Contrasenia,
         *                                   @Query("isActivo") boolean isActivo,
         *                                   @Query("isElimando") boolean isElimando,
         *                                   @Query("FechaCreacion") String FechaCreacion,
         *                                   @Query("Telefono") int Telefono,
         *                                   @Query("codRol") int codRol,
         *                                   @Query("codLocalizacion") int codLocalizacion,
         *                                   @Query("urlFoto") String urlFoto);
         */
        try {
            IRoutesRetrofit iRoutesRetrofit = retrofit.create(IRoutesRetrofit.class);
            (iRoutesRetrofit.RegistrarUsuario(perfil.getNombreCompleto(),
                    perfil.getEmail(),
                    perfil.getContrasenia(),
                    true,
                    false,
                    new Utils().getFechaHora(IDUTILS.AAAAMMDD),
                    Integer.parseInt(perfil.getTelefono()),
                    perfil.getTipoUsuario(),
                    perfil.getCodLocalizacion(),
                    perfil.getUrlFotoPerfil())).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    try {
                        if (response.isSuccessful() && response.body() != null){

                            JSONObject jsonObject = JSONUtils.verificarJSONObject(response.body());

                            int codResultado = JSONUtils.verificarInteger(jsonObject, COLNAMES.CODRESULTADO.getString());
                            String desResultado = JSONUtils.verificarString(jsonObject, COLNAMES.DESRESULTADO.getString());
                            int codUsuario = JSONUtils.verificarInteger(jsonObject, COLNAMES.CODUSUARIO.getString());
                            perfil.setIdPerfil(String.valueOf(codUsuario));

                            new PerfilDAO(context).updateEntidad(perfil);
                            SPF.getInstance(context).guardarKeyCodigoTemporal(codUsuario);

                            IRegistrarUsuario.onResult(codResultado,desResultado);
                        }else{
                            IRegistrarUsuario.onResult(Constantes.RESULT_WARNING,context.getString(R.string.body_nulo));
                        }

                    } catch (Exception e) {
                        IRegistrarUsuario.onResult(Constantes.RESULT_ERROR,context.getString(R.string.error_sintaxis));
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    IRegistrarUsuario.onResult(Constantes.RESULT_ERROR_CONNECTION,context.getString(R.string.error_conexion));
                }
            });
        }catch (Exception e){
            IRegistrarUsuario.onResult(Constantes.RESULT_ERROR,context.getString(R.string.error_sintaxis));
        }


        return null;
    }

    public enum COLNAMES
    {
        CODRESULTADO          ("codResultado"),
        DESRESULTADO          ("desResultado"),
        CODUSUARIO            ("codUsuario");

        String s;
        COLNAMES(String s)
        {
            this.s = s;
        }

        public String getString()
        {
            return s;
        }
    }

}
