package com.studyway.miloficios.asyntask;

import android.content.Context;
import android.os.AsyncTask;

import com.studyway.miloficios.R;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.entities.Localizacion;
import com.studyway.miloficios.retrofit.ConnectionRetrofite;
import com.studyway.miloficios.retrofit.IRoutesRetrofit;
import com.studyway.miloficios.utils.JSONUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Clase creada por Jhon Coronel 18/10/2019
 * Contacto: ghoozh@gmail.com
 */
public class ListaLocalizacionTask extends AsyncTask<String, Void, String>
{
    private final static String nomArrayJSON = "listaLocalizacion";
    private Context context;
    private Retrofit retrofit;
    private IListaLocalizacion IListaLocalizacion;

    public interface IListaLocalizacion {
        void onResult(int codResultado, String desResultado, ArrayList<Localizacion> alLocalizacion);
    }

    public ListaLocalizacionTask(Context context, IListaLocalizacion IListaLocalizacion) {
        this.context = context;
        this.retrofit = ConnectionRetrofite.APIJHONHTTP(Constantes.getHostJhon());
        this.IListaLocalizacion = IListaLocalizacion;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            IRoutesRetrofit iRoutesRetrofit = retrofit.create(IRoutesRetrofit.class);
            (iRoutesRetrofit.ListarLocalizacion()).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    try {
                        if (response.isSuccessful() && response.body() != null){

                            JSONObject jsonObject = JSONUtils.verificarJSONObject(response.body());

                            int codResultado = JSONUtils.verificarInteger(jsonObject, COLNAMES.CODRESULTADO.getString());
                            String desResultado = JSONUtils.verificarString(jsonObject, COLNAMES.DESRESULTADO.getString());

                            JSONArray jsonArray = jsonObject.getJSONArray(nomArrayJSON);
                            ArrayList<Localizacion> alLocalizacion = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonLocalizacion = jsonArray.getJSONObject(i);
                                Localizacion localizacion = new Localizacion();
                                localizacion.setCodLocalizacion(JSONUtils.verificarInteger(jsonLocalizacion,COLNAMES.CODIGO.getString()));
                                localizacion.setNomLocalizacion(JSONUtils.verificarString(jsonLocalizacion,COLNAMES.NOMBRE.getString()));
                                localizacion.setLatitud(JSONUtils.verificarString(jsonLocalizacion,COLNAMES.LATITUD.getString()));
                                localizacion.setLongitud(JSONUtils.verificarString(jsonLocalizacion,COLNAMES.LONGITUD.getString()));
                                alLocalizacion.add(localizacion);
                            }

                            IListaLocalizacion.onResult(codResultado,desResultado,alLocalizacion);
                        }else{
                            IListaLocalizacion.onResult(Constantes.RESULT_WARNING,context.getString(R.string.body_nulo),new ArrayList<>());
                        }

                    } catch (Exception e) {
                        IListaLocalizacion.onResult(Constantes.RESULT_ERROR_CERO,context.getString(R.string.error_sintaxis), new ArrayList<>());
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    IListaLocalizacion.onResult(Constantes.RESULT_ERROR_CERO,context.getString(R.string.error_conexion), new ArrayList<>());
                }
            });
        }catch (Exception e){
            IListaLocalizacion.onResult(Constantes.RESULT_ERROR_CERO,context.getString(R.string.error_sintaxis),new ArrayList<>());
        }


        return null;
    }

    public enum COLNAMES
    {
        CODRESULTADO          ("codResultado"),
        DESRESULTADO          ("desResultado"),
        CODIGO                ("codLocalizacion"),
        NOMBRE                ("nomLocalizacion"),
        LATITUD               ("latitud"),
        LONGITUD              ("longitud");
        String s;
        COLNAMES(String s)
        {
            this.s = s;
        }

        public String getString()
        {
            return s;
        }
    }

}
