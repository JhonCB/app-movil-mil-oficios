package com.studyway.miloficios.asyntask;

import android.content.Context;
import android.os.AsyncTask;

import com.studyway.miloficios.R;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.entities.Perfil;
import com.studyway.miloficios.retrofit.ConnectionRetrofite;
import com.studyway.miloficios.retrofit.IRoutesRetrofit;
import com.studyway.miloficios.utils.JSONUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
/**
 * Clase creada por Jhon Coronel 18/10/2019
 * Contacto: ghoozh@gmail.com
 */
public class ConsultaUsuarioTask extends AsyncTask<String, Void, String>
{
    private final static String nomArrayJSON = "UsuarioLista";
    private Context context;
    private RequestBody jsonObject;
    private Retrofit retrofit;
    private IConsultarUsuario IConsultarUsuario;

    public interface IConsultarUsuario {
        void onResult(int codResultado, String desResultado, Perfil perfil);
    }

    public ConsultaUsuarioTask(Context context, RequestBody jsonObject , IConsultarUsuario IConsultarUsuario) {
        this.context = context;
        this.jsonObject = jsonObject;
        this.retrofit = ConnectionRetrofite.APIJHONHTTP(Constantes.getHostJhon());
        this.IConsultarUsuario = IConsultarUsuario;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            IRoutesRetrofit iRoutesRetrofit = retrofit.create(IRoutesRetrofit.class);
            (iRoutesRetrofit.consultarUsuario(jsonObject)).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (response.isSuccessful() && response.body() != null){

                            JSONObject jsonObject = JSONUtils.verificarJSONObject(response.body().string());
                            JSONArray jsonArray = jsonObject.getJSONArray(nomArrayJSON);

                            JSONObject jsonDatos = jsonArray.getJSONObject(0);
                            Perfil perfil = new Perfil();
                            perfil.setApellidos(JSONUtils.verificarString(jsonDatos, COLNAMES.APELLIDOS.getString()));
                            perfil.setEmail(JSONUtils.verificarString(jsonDatos, COLNAMES.CORREO.getString()));
                            perfil.setDni(JSONUtils.verificarString(jsonDatos, COLNAMES.DNI.getString()));
                            perfil.setDireccion(JSONUtils.verificarString(jsonDatos, COLNAMES.DIRECCION.getString()));
                            perfil.setFechaNacimiento(JSONUtils.verificarString(jsonDatos, COLNAMES.FECHANACIMIENTO.getString()));
                            perfil.setNomUsuario(JSONUtils.verificarString(jsonDatos, COLNAMES.NOMUSUARIO.getString()));
                            perfil.setNombre(JSONUtils.verificarString(jsonDatos, COLNAMES.NOMBRES.getString()));
                            perfil.setTelefono(JSONUtils.verificarString(jsonDatos, COLNAMES.TELEFONO.getString()));
                            perfil.setTipoUsuario(JSONUtils.verificarInteger(jsonDatos, COLNAMES.TIPOUSUARIO.getString()));

                            IConsultarUsuario.onResult(Constantes.RESULT_OK,context.getString(R.string.usuario_encontrado), perfil);
                        }else{
                            IConsultarUsuario.onResult(Constantes.RESULT_WARNING,context.getString(R.string.body_nulo), new Perfil());
                        }

                    } catch (Exception e) {
                        IConsultarUsuario.onResult(Constantes.RESULT_ERROR_CERO,context.getString(R.string.error_sintaxis), new Perfil());
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    IConsultarUsuario.onResult(Constantes.RESULT_ERROR_CERO,context.getString(R.string.error_conexion), new Perfil());
                }
            });
        }catch (Exception e){
            IConsultarUsuario.onResult(Constantes.RESULT_ERROR_CERO,context.getString(R.string.error_sintaxis), new Perfil());
        }


        return null;
    }

    public enum COLNAMES
    {
        APELLIDOS                     ("Apellidos"),
        CONTRASENIA                   ("Contrasena"),
        CORREO                        ("Correo"),
        DNI                           ("DNI"),
        DIRECCION                     ("Direccion"),
        FECHANACIMIENTO               ("FechaNacimiento"),
        NOMUSUARIO                    ("NomUsuario"),
        NOMBRES                       ("Nombres"),
        TELEFONO                      ("Telefono"),
        TIPOUSUARIO                   ("Tipo_usuario");  //2 = CLIENTE / 3 = SOCIO EMPLEADO
        String s;
        COLNAMES(String s)
        {
            this.s = s;
        }

        public String getString()
        {
            return s;
        }
    }

}
