package com.studyway.miloficios.asyntask;

import android.content.Context;
import android.os.AsyncTask;

import com.studyway.miloficios.R;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.retrofit.ConnectionRetrofite;
import com.studyway.miloficios.retrofit.IRoutesRetrofit;
import com.studyway.miloficios.utils.JSONUtils;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Clase creada por Jhon Coronel 18/10/2019
 * Contacto: ghoozh@gmail.com
 */
public class RecuperarContraseniaTask extends AsyncTask<String, Void, String>
{
    private final static String nomJson = "recuperarContrasenia";

    private Context context;
    private Retrofit retrofit;
    private int codUsuario;
    private String codigoAutogenerado;
    private String contraseniaNueva;

    private ICambiarContrasenia ICambiarContrasenia;
    public interface ICambiarContrasenia {
        void onResult(int codResultado, String desResultado);
    }

    public RecuperarContraseniaTask(Context context, int codUsuario,String codigoAutogenerado,String contraseniaNueva, ICambiarContrasenia ICambiarContrasenia) {
        this.context = context;
        this.codUsuario = codUsuario;
        this.codigoAutogenerado = codigoAutogenerado;
        this.contraseniaNueva = contraseniaNueva;
        this.retrofit = ConnectionRetrofite.APIJHONHTTP(Constantes.getHostJhon());
        this.ICambiarContrasenia = ICambiarContrasenia;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            IRoutesRetrofit iRoutesRetrofit = retrofit.create(IRoutesRetrofit.class);
            (iRoutesRetrofit.RecuperarContrasenia(codUsuario,codigoAutogenerado,contraseniaNueva)).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    try {
                        if (response.isSuccessful() && response.body() != null){

                            JSONObject jsonObject = JSONUtils.verificarJSONObject(response.body());

                            int codResultado = JSONUtils.verificarInteger(jsonObject, COLNAMES.CODRESULTADO.getString());
                            String desResultado = JSONUtils.verificarString(jsonObject, COLNAMES.DESRESULTADO.getString());

                            ICambiarContrasenia.onResult(codResultado,desResultado);
                        }else{
                            ICambiarContrasenia.onResult(Constantes.RESULT_WARNING,context.getString(R.string.body_nulo));
                        }

                    } catch (Exception e) {
                        ICambiarContrasenia.onResult(Constantes.RESULT_ERROR,context.getString(R.string.error_sintaxis));
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    ICambiarContrasenia.onResult(Constantes.RESULT_ERROR_CONNECTION,context.getString(R.string.error_conexion));
                }
            });
        }catch (Exception e){
            ICambiarContrasenia.onResult(Constantes.RESULT_ERROR,context.getString(R.string.error_sintaxis));
        }


        return null;
    }

    public enum COLNAMES
    {
        CODRESULTADO          ("codResultado"),
        DESRESULTADO          ("desResultado"),
        CODUSUARIO            ("codUsuario"),
        CODRECUPERACION       ("codigoAutogenerado");
        String s;
        COLNAMES(String s)
        {
            this.s = s;
        }

        public String getString()
        {
            return s;
        }
    }

}
