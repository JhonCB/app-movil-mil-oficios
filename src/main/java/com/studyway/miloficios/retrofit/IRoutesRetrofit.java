package com.studyway.miloficios.retrofit;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Jhon Coronel
 * Correo: ghoozh@gmail.com
 */
public interface IRoutesRetrofit {


    @Headers("Content-Type: application/json")
    @POST("/Api/ConsultarUsuario")
    Call<ResponseBody> consultarUsuario(@Body RequestBody body);

    @GET("/Api/ListarLocalizacion")
    Call<String> ListarLocalizacion();

    @GET("/Api/ConsultarCorreo")
    Call<String> ConsultarCorreo(@Query("email") String email);

    @GET("/Api/GenerarRecuperacionContrasenia")
    Call<String> GenerarRecuperacionContrasenia(@Query("correo") String correo);

    @GET("/Api/RecuperarContrasenia")
    Call<String> RecuperarContrasenia(@Query("codUsuario") int codUsuario,
                                      @Query("codRecuperacion") String codRecuperacion,
                                      @Query("contraseniaNueva") String contraseniaNueva);

    @GET("/Api/ListarServicio")
    Call<String> ListarServicio();

    @GET("/API/ListarCategoria")
    Call<String> ListarCategoria();

    @GET("/Api/ListarServicioXCategoria")
    Call<String> ListarServicioXCategoria(@Query("codCategoria") int codCategoria);

    @GET("/Api/ListarServicioPorSocio")
    Call<String> ListarServicioPorSocio(@Query("codSocio") int codSocio);

    @GET("/Api/RegistrarUsuario")
    Call<String> RegistrarUsuario(@Query("Nombre") String Nombre,
                                  @Query("Email") String Email,
                                  @Query("Contrasenia") String Contrasenia,
                                  @Query("isActivo") boolean isActivo,
                                  @Query("isElimando") boolean isElimando,
                                  @Query("FechaCreacion") String FechaCreacion,
                                  @Query("Telefono") int Telefono,
                                  @Query("codRol") int codRol,
                                  @Query("codLocalizacion") int codLocalizacion,
                                  @Query("urlFoto") String urlFoto);

    @GET("/Api/RegistrarServicio")
    Call<String> RegistrarServicio(@Query("CodUsuario") int CodUsuario,
                                  @Query("Descripcion") String Descripcion,
                                  @Query("FechaFin") String FechaFin,
                                  @Query("CodCategoria") int CodCategoria,
                                  @Query("codLocalizacion") int codLocalizacion,
                                  @Query("latitud") String latitud,
                                  @Query("longitud") String longitud);

    @GET("/Api/GenerarSolicitudServicio")
    Call<String> GenerarSolicitudServicio(@Query("codServicio") int codServicio,
                                          @Query("costo") double costo,
                                          @Query("cotizacion") double cotizacion,
                                          @Query("codUsuario") int codUsuario);

    @GET("/Api/ListarSolicitudServicioPorUsuario")
    Call<String> ListarSolicitudServicioPorUsuario(@Query("codUsuario") int codUsuario);

    @GET("/Api/ListarSolicitudServicioPorSocio")
    Call<String> ListarSolicitudServicioPorSocio(@Query("codSocio") int codSocio);

    @GET("/Api/Login")
    Call<String> Login(@Query("usuario") String usuario,
                       @Query("contrasenia") String contrasenia);


}
