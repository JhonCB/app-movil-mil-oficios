package com.studyway.miloficios.retrofit;


import com.studyway.miloficios.entities.Host;
import com.studyway.miloficios.utils.Utils;

import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ConnectionRetrofite {

    public static Retrofit APIHOST(String baseURL){
        return new Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
    }


    public static Retrofit APIHOST(Host host){
        // String baseURL = "http://" + host.getHost() + ":" + host.getPuerto();
        String baseURL = "http://" + host.getHost() + ":" + host.getPuerto();
        Utils.LOG_D(baseURL);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .build();

        return new Retrofit.Builder()
                .baseUrl(baseURL)
                .client(okHttpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
    }

    public static Retrofit APISTEVENHTTP(Host host){
        // String baseURL = "http://" + host.getHost() + ":" + host.getPuerto();
        String baseURL = "http://192.168.1.60";
        Utils.LOG_D(baseURL);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .build();

        return new Retrofit.Builder()
                .baseUrl(baseURL)
                .client(okHttpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
    }

    public static Retrofit APIJHONHTTP(Host host){
        // String baseURL = "http://" + host.getHost() + ":" + host.getPuerto();
        String baseURL = "http://miloficios.somee.com";
        Utils.LOG_D(baseURL);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .build();

        return new Retrofit.Builder()
                .baseUrl(baseURL)
                .client(okHttpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
    }

    public static Retrofit APISTEVENHTTPS(Host host){
        // String baseURL = "http://" + host.getHost() + ":" + host.getPuerto();
        String baseURL = "http://localhost";
        Utils.LOG_D(baseURL);
        OkHttpClient okHttpClientWithOutSSL =  ConnectionRetrofite.getUnsafeOkHttpClient(); // llamado al mEtodo para ignorar certificados https...

//        OkHttpClient okHttpClient = new OkHttpClient.Builder()
//                .connectTimeout(1, TimeUnit.MINUTES)
//                .readTimeout(30, TimeUnit.SECONDS)
//                .build();
        return new Retrofit.Builder()
                .baseUrl(baseURL)
//                .client(okHttpClient) // se va
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(okHttpClientWithOutSSL) // added
                .build();
    }

    public static Retrofit APIHOSTHTTPS(Host host){
        // String baseURL = "http://" + host.getHost() + ":" + host.getPuerto();
        String baseURL = "https://" + host.getHost() + ":" + host.getPuerto();
        Utils.LOG_D(baseURL);
        OkHttpClient okHttpClientWithOutSSL =  ConnectionRetrofite.getUnsafeOkHttpClient(); // llamado al mEtodo para ignorar certificados https...

//        OkHttpClient okHttpClient = new OkHttpClient.Builder()
//                .connectTimeout(1, TimeUnit.MINUTES)
//                .readTimeout(30, TimeUnit.SECONDS)
//                .build();
        return new Retrofit.Builder()
                .baseUrl(baseURL)
//                .client(okHttpClient) // se va
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(okHttpClientWithOutSSL) // added
                .build();
    }

    public static Retrofit APIHOST2(Host host){
        String baseURL = "http://" + host.getHost() + ":";
        Utils.LOG_D(baseURL);
        return new Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
    }

    public static Retrofit APIHOSTWILIAM(Host host){
        String baseURL = "http://" + host.getHost() + ":";
        Utils.LOG_D(baseURL);
        return new Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
    }


    public static Retrofit APIHOSTGOOGLE(){
        String baseURL = "https://maps.googleapis.com";
        Utils.LOG_D(baseURL);
        return new Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
    }

    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager)trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            // agregado de permisos..
            builder.connectTimeout(1, TimeUnit.MINUTES);
            builder.readTimeout(30, TimeUnit.SECONDS);

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
