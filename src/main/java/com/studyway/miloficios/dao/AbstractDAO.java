package com.studyway.miloficios.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.entities.AbstractEntity;
import com.studyway.miloficios.utils.CursorUtils;
import com.studyway.miloficios.utils.Utils;


// FIXME: 23/11/2016 replace - yieldIfContendedSafely

public abstract class AbstractDAO<T extends AbstractEntity> {

	protected String TABLA;
	protected String IDENTIFICADOR;
	protected Context mContext;
	public final static int SIN_FLAG = 0;
	public final static int CON_FLAG = 1;

	private final static int TAMANIODATE = 10;  // default 10
	public final static String FORMATODATESQLITE = "yyyy-MM-dd HH:mm:ss";
	public final static String FORMATODATENOSQLITE = "dd/MM/yyyy HH:mm:ss";
	public final static String FORMATOFECHALARGA = "yyyy/MM/dd HH:mm:ss";

	public AbstractDAO(Context context) {
		this.mContext = context;
	}

	/**
	 * M�todo abstracto que devuelve una instacia de ContextValues con los
	 * valores de la instacia T
	 *
	 * @param entidad
	 *            - Entidad de la que se extraer�n los datos para llenar la
	 *            instancia de ContentValues
	 * @return - Instancia de ContentValues con los datos de la instancia T
	 */
	protected abstract ContentValues transformarEnContentValues(T entidad);

	protected abstract ContentValues transformarEnContentValues(Cursor cursor);

	protected abstract ContentValues transformarEnContentValuesWithNull(Cursor cursor);

	/**
	 * M�todo abstracto que devuelve una instacia de ContextValues con los
	 * valores de la instacia T
	 *
	 * @param entidad
	 *            - Entidad de la que se extraer�n los datos para llenar la
	 *            instancia de ContentValues
	 * @return - Instancia de ContentValues con los datos de la instancia T
	 */
	protected abstract ContentValues transformarEstadoEnContentValues(T entidad);

	/**
	 * M�todo abstracto que devuelve una instancia de T con los valores del
	 * cursor indicado
	 *
	 * @param cursor
	 *            - Cursor del cual se extraer�n los valores para crear la
	 *            instancia de T
	 * @return Instacia de T creada con los valores del cursor
	 */
	public abstract T transformEnEntidad(Cursor cursor);

	/**
	 * M�todo que elimina una entidad
	 *
	 * @param id
	 *            - Identificador de la entidad a eliminarse
	 */
	public void deleteEntidad(String id) {
		String[] whereArgs = { id };

		DBHelperSingleton.getDatabase(mContext).delete(TABLA, IDENTIFICADOR + "=?", whereArgs);
	}

	public void deleteEntidadWhere(String query) {
		DBHelperSingleton.getDatabase(mContext).delete(TABLA, query, null);
	}


	public void truncateEntidad() throws SQLException
	{
		DBHelperSingleton.getDatabase(mContext).delete(TABLA, null, null);
	}


	public void rawQuery(String sQuery, String[] args){
		DBHelperSingleton.getDatabase(mContext).rawQuery(sQuery, args);
	}

	public void execSQLQuery(String sQuery, String[] args){
		DBHelperSingleton.getDatabase(mContext).execSQL(sQuery, args);
	}

	/**
	 * M�todo que inserta una entidad
	 *
	 * @param insertValues
	 *            - Instancia de ContentValues con los datos de la entidad a
	 *            insertar
	 */
	private int insertEntidad(ContentValues insertValues) {
		return (int) DBHelperSingleton.getDatabase(mContext).insert(TABLA, null, insertValues);
	}

	/**
	 * M�todo que actualiza una entidad
	 *
	 * @param updateValues
	 *            - Instancia de ContentValues con los valores a actualizar
	 * @param id
	 *            - Identificador de la entidad a actualizar
	 */
	protected int updateEntidad(ContentValues updateValues, String id) {
		String[] whereArgs = { id };

		return DBHelperSingleton.getDatabase(mContext).update(TABLA, updateValues, IDENTIFICADOR + "=?", whereArgs);
	}

	/**
	 * M todo que devuelve un arraylist con todas las entidades que cumplan con el criterio de selección
	 *
	 * @param selection String criterio de selección
	 * @param context Context
	 * @return ArrayList<T>
	 * @throws Exception
	 */
	public ArrayList<T> getAlEntidad(String selection, Context context) throws Exception
	{
		ArrayList<T> alEntidad = new ArrayList<T>();
		String sql = String.format(Locale.getDefault(), "SELECT * FROM %s %s",
				TABLA,
				selection   // criterio 01
		);
 		Log.d("sql", sql);
		Cursor cursor = null;
		try
		{
			cursor = DBHelperSingleton.getDatabase(context).rawQuery(sql , null);
			if (cursor == null || cursor.getCount() < 1)
				return alEntidad;

			cursor.moveToFirst();
			do
			{
				alEntidad.add(transformEnEntidad(cursor));
			}while (cursor.moveToNext());
		}
		catch (Exception e)
		{
			Utils.LOG_E("ABSTRACTDAO -> " +e.getMessage());
		}
		finally
		{
			if(cursor != null)
				cursor.close();
		}
		return alEntidad;
	}


	/**
	 * M todo que devuelve un arraylist con todas las entidades que cumplan con el criterio de selección
	 *
	 * @param query String criterio de selección
	 * @param context Context
	 * @return ArrayList<T>
	 * @throws Exception
	 */
	public ArrayList<T> getAlEntidadByQuery(String query, Context context) throws Exception
	{
		ArrayList<T> alEntidad = new ArrayList<T>();
		Cursor cursor = null;
		try
		{
			cursor = DBHelperSingleton.getDatabase(context).rawQuery(query , null);
			if (cursor == null || cursor.getCount() < 1)
				return alEntidad;

			cursor.moveToFirst();
			do
			{
				alEntidad.add(transformEnEntidad(cursor));
			}while (cursor.moveToNext());
		}
		catch (Exception e)
		{
			Utils.LOG_E("ABSTRACTDAO -> " +e.getMessage());
		}
		finally
		{
			if(cursor != null)
				cursor.close();
		}
		return alEntidad;
	}


	/**
	 * M todo que devuelve un arraylist con todas las entidades que cumplan con el criterio de selección
	 *
	 * @param query String criterio de selección
	 * @param context Context
	 * @return ArrayList<T>
	 * @throws Exception
	 */
	public ArrayList<T> getAlEntidadFromQuery(String query, Context context) throws Exception
	{
		ArrayList<T> alEntidad = new ArrayList<T>();
		Log.d("sql", query);
		Cursor cursor = null;
		try
		{
			cursor = DBHelperSingleton.getDatabase(context).rawQuery(query , null);
			if (cursor == null || cursor.getCount() < 1)
				return alEntidad;

			cursor.moveToFirst();
			do
			{
				alEntidad.add(transformEnEntidad(cursor));
			}while (cursor.moveToNext());
		}
		catch (Exception e)
		{
			Utils.LOG_E("ABSTRACTDAO -> " +e.getMessage());
		}
		finally
		{
			if(cursor != null)
				cursor.close();
		}
		return alEntidad;
	}

	/**
	 * ejecuta una lista de querys spliteando una cadena simple..
	 * @param sql {@link String}
	 * @param context {@link Context}
	 * @throws Exception
	 */
	public void execSQL(String sql, Context context)throws Exception
	{
		if(sql == null || sql.length() == 0)
			return;

		String[] listQuerys = sql.split(";");	// simple split
		if(listQuerys.length == 0)
			return;

		for (String query : listQuerys)
		{
			if(query == null || query.length() == 0)
				continue;
			DBHelperSingleton.getDatabase(context).execSQL(query);
		}
	}


	/**
	 * code : 1 unable to open database file (code 14)
	 * TODO : QUITAR EL CLOSE ------------------------------------------------------------------------------------
	 * TODO : QUITAR EL CLOSE ------------------------------------------------------------------------------------
	 * try catch ??
	 * M�todo que obtiene una isntancia de T
	 * analizar los errores
	 * @param query
	 *            - consulta de la instancia a obtener
	 * @return - Instancia de T correspondiente al registro obtenido por la consulta
	 */
	public T getEntidad(String query, Context context){

		T entidad = null;
		String sql = String.format(Locale.getDefault(), "SELECT * FROM %s %s", TABLA, query);
		Utils.LOG_I(sql);
		// Cursor cursor = DBHelperSingleton.getDatabase(context).rawQuery(sql , null); // cerrar cursor
//        if (cursor != null && cursor.moveToFirst()) {
//            entidad = transformEnEntidad(cursor);
//            cursor.close();
//        }
		Cursor cursor = null;
		try
		{
			cursor = DBHelperSingleton.getDatabase(context).rawQuery(sql , null); // cerrar cursor

			if (cursor == null || cursor.getCount() < 1) {
				return null;
			}
			cursor.moveToFirst();
			entidad = transformEnEntidad(cursor);
		}
		catch (Exception e)
		{
			Utils.LOG_E("ABSTRAC getEntidad"+  e.getMessage());
		}
		finally
		{
			if(cursor != null)
				cursor.close();
		}
		return entidad;
	}

	/**
	 * M�todo que insertar una entidad
	 *
	 * @param entidad
	 *            - Instancia de T a insertar
	 */
	public int insertEntidad(T entidad) {
		return insertEntidad(transformarEnContentValues(entidad));
	}

	/**
	 * M�todo que actualizar una entidad
	 *
	 * @param entidad
	 *            - Instancia de T a actualizar
	 */
	public int updateEntidad(T entidad) {
		return updateEntidad(transformarEnContentValues(entidad), String.valueOf(entidad.getID()));
	}

	/**
	 * M�todo que actualizar una entidad
	 *
	 * @param entidad
	 *            - Instancia de T a actualizar
	 */
	public int updateEstadoEntidad(T entidad) {
		return updateEntidad(transformarEstadoEnContentValues(entidad), String.valueOf(entidad.getID()));
	}


	/**
	 * M�todo que obtiene una entidad
	 *
	 * @param cursor
	 *            - Cursor del cual se obtendr�n los datos para crear una nueva
	 *            instancia de T
	 * @return Instancia de T creado con los valores del cursor
	 */
	public T getEntidadFromCursor(Cursor cursor) {
		return transformEnEntidad(cursor);
	}

	/**
	 * M�todo que reemplaza una entidad
	 *
	 * @param entidad
	 *            - Instancia de T a reemplazarse
	 */
	public void replaceEntidad(T entidad) {

		if (getEntidad(String.valueOf(entidad.getID())) != null) {
			updateEntidad(entidad);
		} else {
			insertEntidad(entidad);
		}
	}

	/**
	 * M�todo que obtiene una isntancia de T
	 *
	 * @param id
	 *            - Identificador de la instancia a obtener
	 * @return - Instancia de T correspondiente al id indicado
	 */
	public T getEntidad(String id) {

		T entidad = null;

		String[] whereArgs = { id };

		Cursor cursor = DBHelperSingleton.getDatabase(mContext).query(TABLA, null, IDENTIFICADOR + "=?", whereArgs, null, null, null);

		if (cursor != null && cursor.moveToFirst()) {

			entidad = transformEnEntidad(cursor);
			cursor.close();
		}

		return entidad;
	}


	/**
	 * M�todo que devuelve un cursor con todas las entidades de una tabla
	 *
	 * @return - Cursor con todas las entidades de una tabla
	 */
	public Cursor listEntidadCursor() {
		Cursor cursor = DBHelperSingleton.getDatabase(mContext).query(TABLA, new String[] { IDENTIFICADOR + " as '_id'", "*" }, null, null, null,
				null, IDENTIFICADOR + " ASC");
		return cursor;
	}

	/**
	 * M�todo que realiza un query a una tabla de la base de datos
	 *
	 * @param projection
	 *            - Columnas a seleccionar de una tabla (No se debe incluir la
	 *            palabra SELECT en el query)
	 * @param selection
	 *            - Columnas que se incluir�n en el WHERE del query (No se debe
	 *            incluir la palabra WHERE en el query)
	 * @param selectionArgs
	 *            - Valores de las columnas indicadas en el selection
	 * @param sortOrder
	 *            - Orden en el que se obtendr�n los datos (No se debe incluir
	 *            la palabra ORDER BY en el query)
	 * @return Devuelve un Cursor con la informaci�n solicitada
	 */
	public Cursor query(String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		String[] finalProjection = null;
		if (projection == null) {
			finalProjection = new String[] { IDENTIFICADOR + " _id", "*" };
		} else {
			finalProjection = new String[1 + projection.length];

			System.arraycopy(new String[] { IDENTIFICADOR + " _id" }, 0, finalProjection, 0, 1);
			System.arraycopy(projection, 0, finalProjection, 1, projection.length);
		}

		Cursor cursor = DBHelperSingleton.getDatabase(mContext).query(TABLA, finalProjection, selection, selectionArgs, null, null, sortOrder);
		return cursor;
	}


	public int getSiguienteID() {
		int id = 1;

		Cursor cursor = query(new String[] { IDENTIFICADOR }, null, null, IDENTIFICADOR + " DESC");
		if (cursor.moveToFirst()) {
			id = CursorUtils.verificarInteger(cursor, IDENTIFICADOR) + 1;
		}
		return id;
	}



	protected int toResult(long row)
	{
		return (row < 1) ? Constantes.RESULT_ERROR : Constantes.RESULT_OK;
	}

	public String convertFechaLarga(String fecha){
		if (fecha.length() < TAMANIODATE)
			return fecha;
		try
		{
			fecha = cambiarFormato(FORMATODATESQLITE, FORMATOFECHALARGA, fecha);
		}catch (Exception e)
		{
//			new IncidenciaDAO().insertEntidad(Constantes.getIncidencia(e.getMessage()));
		}
		return fecha;

	}

	/**
	 * M�todo parseo de fechas en formato SQLite y NOSQlite.
	 *
	 * @param fecha
	 *            - Fecha de entrada y salida (SQLite-Servidor)
	 * @return Devuelve un String con la fecha parseada.
	 */
	public String convertFechaNoSQLite(String fecha)
	{
		if (fecha.length() < TAMANIODATE)
			return fecha;
		try
		{
			fecha = cambiarFormato(FORMATODATESQLITE, FORMATODATENOSQLITE, fecha);
		}catch (Exception e)
		{
//			new IncidenciaDAO().insertEntidad(Constantes.getIncidencia(e.getMessage()));
		}
		return fecha;
	}
	public String convertFechaSQLite(String fecha)
	{
		if (fecha.length() < TAMANIODATE)
			return fecha;
		try
		{
			fecha = cambiarFormato(FORMATODATENOSQLITE, FORMATODATESQLITE, fecha);
		}catch (Exception e)
		{
//			new IncidenciaDAO().insertEntidad(Constantes.getIncidencia(e.getMessage()));
		}
		return fecha;
	}

	/**
	 * @param formato01 String
	 * @param formato02 String
	 * @param fecha String
	 * @return
	 * @throws Exception
	 */
	private String cambiarFormato(final String formato01, final String formato02, String fecha)throws Exception
	{
//		return new SimpleDateFormat(formatoFecha, new Locale("es", "pe"));
		SimpleDateFormat sdf = new SimpleDateFormat(formato01, new Locale("es", "pe"));
		Date date = sdf.parse(fecha);
		sdf.applyPattern(formato02);
		return sdf.format(date);
	}



}