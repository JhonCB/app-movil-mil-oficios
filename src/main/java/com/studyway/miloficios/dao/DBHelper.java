package com.studyway.miloficios.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;

import com.studyway.miloficios.datacontractenum.PerfilDataContract;
import com.studyway.miloficios.utils.Utils;


public class DBHelper extends SQLiteOpenHelper
{

    private static String database = "BD_MO";

    public DBHelper(Context context) {
        super(context, database , null, 3);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String queryCreateTbPerfil= "CREATE TABLE "+ PerfilDataContract.TABLA.getValue()+ " (" +
                PerfilDataContract.ID.getValue()                                    + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
                PerfilDataContract.NOMBRE.getValue()                                + " TEXT NULL DEFAULT '' ," +
                PerfilDataContract.APELLIDO.getValue()                              + " TEXT NULL DEFAULT '' ," +
                PerfilDataContract.NOMBRECOMPLETO.getValue()                        + " TEXT NULL DEFAULT ''," +
                PerfilDataContract.TELEFONO.getValue()                              + " TEXT NULL DEFAULT ''," +
                PerfilDataContract.TIPOUSUARIO.getValue()                           + " INTEGER DEFAULT 0," +
                PerfilDataContract.EMAIL.getValue()                                 + " TEXT NULL DEFAULT ''," +
                PerfilDataContract.CONTRASENIA.getValue()                           + " TEXT NULL DEFAULT ''," +
                PerfilDataContract.CODLOCALIZACION.getValue()                       + " INTEGER NULL DEFAULT 0," +
                PerfilDataContract.IDPERFIL.getValue()                              + " TEXT NULL DEFAULT ''," +
                PerfilDataContract.TIPOPERFIL.getValue()                            + " INTEGER DEFAULT 0 ," +
                PerfilDataContract.URLPERFIL.getValue()                             + " TEXT DEFAULT '' , " +
                PerfilDataContract.FLAGENVIADO.getValue()                           + " INTEGER DEFAULT 0 " +
                ")";


        db.execSQL(queryCreateTbPerfil);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

        db.execSQL("DROP TABLE IF EXISTS " + PerfilDataContract.TABLA.getValue());
        onCreate(db);
    }

    private final static String DIRBD = "BDMYRUTA";

    /**
     * @param context Context
     * @throws Exception exceptión : context is null or IOException
     */
    public static void exportdb(Context context)throws Exception
    {
        File file = new File(Environment.getExternalStorageDirectory(), DIRBD);
        if(!file.exists())
            file.mkdirs();

        String inFileName = "/data/data/" + context.getPackageName() + "/databases/" + database;
        file = new File(inFileName);
        if(file.exists())
            Utils.LOG_I(file.getName());
        FileInputStream fis = new FileInputStream(file);
        String outFileName = Environment.getExternalStorageDirectory() + "/"+ DIRBD + "/" + database + ".db";
        OutputStream output = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = fis.read(buffer)) > 0)
            output.write(buffer, 0, length);

        output.flush();
        output.close();
        fis.close();
    }


}
