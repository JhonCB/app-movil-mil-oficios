package com.studyway.miloficios.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.Locale;

import com.studyway.miloficios.datacontractenum.ConfigOpcionUsuarioDataContract;
import com.studyway.miloficios.datacontractenum.PerfilDataContract;
import com.studyway.miloficios.entities.Perfil;
import com.studyway.miloficios.utils.CursorUtils;

public class PerfilDAO extends AbstractDAO<Perfil> {

    private Context context;

    public PerfilDAO(Context context) {
        super(context);
        this.context = context;
        TABLA = PerfilDataContract.TABLA.getValue();
        IDENTIFICADOR = PerfilDataContract.ID.getValue();
    }

    @Override
    protected ContentValues transformarEnContentValues(Perfil entidad) {
        ContentValues cv = new ContentValues();
        cv.put(PerfilDataContract.NOMBRE.getValue(), entidad.getNombre());
        cv.put(PerfilDataContract.APELLIDO.getValue(), entidad.getApellidos());
        cv.put(PerfilDataContract.NOMBRECOMPLETO.getValue(), entidad.getNombreCompleto());
        cv.put(PerfilDataContract.TELEFONO.getValue(), entidad.getTelefono());
        cv.put(PerfilDataContract.TIPOUSUARIO.getValue(), entidad.getTipoUsuario());
        cv.put(PerfilDataContract.EMAIL.getValue(), entidad.getEmail());
        cv.put(PerfilDataContract.CONTRASENIA.getValue(), entidad.getContrasenia());
        cv.put(PerfilDataContract.CODLOCALIZACION.getValue(), entidad.getCodLocalizacion());
        cv.put(PerfilDataContract.IDPERFIL.getValue(), entidad.getIdPerfil());
        cv.put(PerfilDataContract.TIPOPERFIL.getValue(), entidad.getTipoPerfil());
        cv.put(PerfilDataContract.URLPERFIL.getValue(), entidad.getUrlFotoPerfil());
        cv.put(PerfilDataContract.FLAGENVIADO.getValue(), entidad.getFlagEnviado());
        return cv;
    }

    @Override
    protected ContentValues transformarEnContentValues(Cursor cursor) {
        return null;
    }

    @Override
    protected ContentValues transformarEnContentValuesWithNull(Cursor cursor) {
    return null;
    }

    @Override
    protected ContentValues transformarEstadoEnContentValues(Perfil entidad) {
        ContentValues cv = new ContentValues();
        return cv;
    }

    @Override
    public Perfil transformEnEntidad(Cursor cursor) {

        Perfil perfil = new Perfil();
        perfil.setID(CursorUtils.verificarInteger(cursor, PerfilDataContract.ID.getValue()));
        perfil.setNombre(CursorUtils.verificarString(cursor, PerfilDataContract.NOMBRE.getValue()));
        perfil.setApellidos(CursorUtils.verificarString(cursor, PerfilDataContract.APELLIDO.getValue()));
        perfil.setNombreCompleto(CursorUtils.verificarString(cursor, PerfilDataContract.NOMBRECOMPLETO.getValue()));
        perfil.setTelefono(CursorUtils.verificarString(cursor, PerfilDataContract.TELEFONO.getValue()));
        perfil.setTipoUsuario(CursorUtils.verificarInteger(cursor, PerfilDataContract.TIPOUSUARIO.getValue()));
        perfil.setEmail(CursorUtils.verificarString(cursor, PerfilDataContract.EMAIL.getValue()));
        perfil.setContrasenia(CursorUtils.verificarString(cursor, PerfilDataContract.CONTRASENIA.getValue()));
        perfil.setCodLocalizacion(CursorUtils.verificarInteger(cursor, PerfilDataContract.CODLOCALIZACION.getValue()));
        perfil.setIdPerfil(CursorUtils.verificarString(cursor, PerfilDataContract.IDPERFIL.getValue()));
        perfil.setTipoPerfil(CursorUtils.verificarInteger(cursor, PerfilDataContract.TIPOPERFIL.getValue()));
        perfil.setUrlFotoPerfil(CursorUtils.verificarString(cursor, PerfilDataContract.URLPERFIL.getValue()));
        perfil.setFlagEnviado(CursorUtils.verificarInteger(cursor, PerfilDataContract.FLAGENVIADO.getValue()));
        return perfil;
    }

    public int insertarPerfil(Perfil entidad) {
        return super.insertEntidad(entidad);
    }


    public Perfil getUltimaSesion()
    {
        String query = String.format(Locale.getDefault(), "ORDER BY %s DESC LIMIT 1", IDENTIFICADOR);
        Perfil perfil = this.getEntidad(query, context);
        return (perfil == null)? new Perfil() : perfil;
    }

    public int updatePerfil(Perfil perfil){
        ContentValues cv = new ContentValues();
        cv.put(PerfilDataContract.NOMBRE.getValue(), perfil.getNombre());
        cv.put(PerfilDataContract.APELLIDO.getValue(), perfil.getApellidos());
        cv.put(PerfilDataContract.NOMBRECOMPLETO.getValue(), perfil.getNombreCompleto());
        cv.put(PerfilDataContract.TELEFONO.getValue(), perfil.getTelefono());
        cv.put(PerfilDataContract.TIPOUSUARIO.getValue(), perfil.getTipoUsuario());
//        cv.put(PerfilDataContract.TIPOPERFIL.getValue(), perfil.getIdPerfil());
        cv.put(PerfilDataContract.EMAIL.getValue(), perfil.getEmail());
        cv.put(PerfilDataContract.CONTRASENIA.getValue(), perfil.getContrasenia());
        cv.put(PerfilDataContract.CODLOCALIZACION.getValue(), perfil.getCodLocalizacion());
        cv.put(PerfilDataContract.URLPERFIL.getValue(), perfil.getUrlFotoPerfil());
        cv.put(PerfilDataContract.FLAGENVIADO.getValue(), perfil.getFlagEnviado());
        return DBHelperSingleton.getDatabase(context).update(TABLA, cv,
                PerfilDataContract.IDPERFIL.getValue() + " = '" + perfil.getIdPerfil()+"'",
                null);
    }

//    public int updatePerfil2(Perfil perfil){
//        String query = String.format(Locale.getDefault(), "UPDATE %s " +
//                "SET %s = %s, %s = %s, %s = %s, %s = %s, %s = %s, %s = %d" +
//                "WHERE %s = %s", TABLA,
//                PerfilDataContract.NOMBRE.getValue(),perfil.getNombre(),
//                PerfilDataContract.APELLIDO.getValue(),perfil.getApellidos(),
//                PerfilDataContract.NOMBRECOMPLETO.getValue(),perfil.getNombreCompleto(),
//                PerfilDataContract.EMAIL.getValue(),perfil.getEmail(),
//                PerfilDataContract.URLPERFIL.getValue(),perfil.getUrlFotoPerfil(),
//                PerfilDataContract.FLAGENVIADO.getValue(),perfil.getFlagEnviado(),
//                PerfilDataContract.IDPERFIL,perfil.getIdPerfil());
//        return this.updateEntidad(perfil);
//    }

    public int updatePerfilFlag(Perfil perfil){
        ContentValues cv = new ContentValues();
        cv.put(ConfigOpcionUsuarioDataContract.FLAGENVIADO.getValue(), perfil.getFlagEnviado());
        return DBHelperSingleton.getDatabase(context).update(TABLA, cv,
                PerfilDataContract.FLAGENVIADO.getValue() + " = 0",
                null);
    }




}
