package com.studyway.miloficios.application;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;

import androidx.multidex.MultiDexApplication;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.studyway.miloficios.utils.Utils;

import java.util.Locale;

public class Comun extends MultiDexApplication {
    @SuppressLint("StaticFieldLeak")
    private static Context context = null;  // esto puede ocasionar fugas de memoria/ this can cause memory leaks..
    private static GoogleMap googleMap = null;
    private static Activity baseActivity = null;
    private static boolean isAplicativoPrimerPlano = false;
    private static LatLng latLng = new LatLng(0d,0d);

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        changedLocale();
    }

    public static boolean getIsAplicativoPrimerPlano() {
        return isAplicativoPrimerPlano;
    }

    public static void setIsAplicativoPrimerPlano(boolean isAplicativoPrimerPlano) {
        Comun.isAplicativoPrimerPlano = isAplicativoPrimerPlano;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Utils.LOG_D("onTerminate");
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Utils.LOG_D("onLowMemory");
    }

    public static GoogleMap getGoogleMap() {
        return googleMap;
    }

    public static void setGoogleMap(GoogleMap googleMap) {
        Comun.googleMap = googleMap;
    }

    public static Context getContext() {
        return context;
    }

    public static Activity getBaseActivity() {
        return baseActivity;
    }

    public static void setBaseActivity(Activity baseActivity) {
        Comun.baseActivity = baseActivity;
    }

    /**
     * Esto latLngUsuario publico serviara para poder obtener coordenas desde cualquier lugar
     */
    public static LatLng getLatLngUsuario() {
        return latLng;
    }

    public static void setLatLngUsuario(LatLng latLngUsuario) {
        Comun.latLng = latLngUsuario;
    }

    private void changedLocale(){
        Locale locale = new Locale("es_US");
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        this.getResources().updateConfiguration(config, null);
    }
}
