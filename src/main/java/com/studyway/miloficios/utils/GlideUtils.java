package com.studyway.miloficios.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.studyway.miloficios.R;

public class GlideUtils {


    public interface OnCallback{
        void onResult(boolean isExitoso);   // boolean -> is
    }

    /**
     * ELIMINAR CACHÉ DEL GLIDE
     */
    public static void deleteCacheGlideThread(Context context){
        new Thread(() -> Glide.get(context).clearDiskCache()).start();
    }

    /**
     * Esto es para poner visualizar la foto del evento en caso contrario que no sea posible mostrar la foto se pondra una por defautl.
     * @param context
     * @param urlImage
     * @param imageView
     * @param onCallback
     */
    public static void setFotoServicio(Context context, String urlImage, final ImageView imageView, final OnCallback onCallback){
        try {
//            Utils.LOG_I("descargando " + urlImage);
            Glide.with(context)
                    .load(urlImage)
                    .placeholder(R.drawable.ic_no_photo)
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            onCallback.onResult(false);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            onCallback.onResult(true);
                            return false;
                        }
                    })
                    .into(imageView);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

}
