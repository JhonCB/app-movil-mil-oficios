package com.studyway.miloficios.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.drawable.Drawable;
import android.os.BatteryManager;
import android.telephony.TelephonyManager;
import android.text.format.Time;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.studyway.miloficios.R;
import com.studyway.miloficios.enums.IDUTILS;

import java.security.MessageDigest;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by innovaapps on 18/10/2016.
 */
public class Utils {

    private static final String TAG = "APPTEGUIO";
    private static final String INTREGEX    = "^([0-9]{1})+$";

    public static final boolean DEBUG = true;

    /**
     * convertir string DD/MM/YYYY a DATE
     * @param fecha
     * @return
     */
    public static Date stringToDate(String fecha){
        Date date = new Date();
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
            date = sdf.parse(fecha);
        }catch (Exception e){
            e.printStackTrace();
        }
        return date;
    }

    public static void keyHash(Context context){
        // Add code to print out the key hash
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    "com.studyway.miloficios",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * comprovamos si hay algun perfil registrado o si esta logueado
     * @param context
     * @return
     */
//    public static boolean isUsuarioLogueado(Context context){
//
//        UsuarioSesion usuarioSesion = new UsuarioSesionDAO(context).getUltimaSesion();
//        return usuarioSesion.getCodUsuario() != 0;
//    }


    /**
     * foto perfil , en caso no hayga un perfil se le asigna un por defecto, cuando se logue tomara la foto del usuario
     * @param context
     * @param urlImage
     * @param imageView
     */
    public static void setFotoPerfil(Context context, String urlImage, ImageView imageView){

        if (urlImage.equals("") || urlImage.equals("none"))
            imageView.setImageResource(R.drawable.ic_user_256x256);
        else {
            Glide.with(context)
                    .load(urlImage)
                    .centerCrop()
                    //.crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .into(imageView);
        }

    }


    /**
     *  foto unidad, cuando veamos el recorrido en el popup, se asiganara la imagen correspondiente
     * @param context
     * @param urlImage
     * @param imageView
     */
    public static void setFotoUnidad(Context context, String urlImage, ImageView imageView){
//        try {
//                Glide.with(context)
//                        .load(urlImage)
//                        .placeholder(R.drawable.ic_sin_foto_bus_128)
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .listener(new RequestListener<Drawable>() {
//                            @Override
//                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                                return false;
//                            }
//
//                            @Override
//                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                                return false;
//                            }
//                        })
//                        .into(imageView);
//
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }

    }

    /**
     *
     * @param texto
     * @param tipo (0 = advertencia, 1 =  okey, -1 = error)
     */
    public static void snackBar(View view, String texto, int tipo){

//        Snackbar mSnackBar = Snackbar.make(view, texto, Snackbar.LENGTH_LONG);
//        // styling for action text
//        // styling for rest of text
//        View snackbarView = mSnackBar.getView();
//        TextView tvSnackBar = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
//        tvSnackBar.setTextColor(Color.WHITE);
//        tvSnackBar.setTextSize(14);
//        tvSnackBar.setTypeface(Typeface.DEFAULT_BOLD);
//        // styling for background of snackbar
//        View sbView = snackbarView;
//        switch (tipo){
//            case Constantes.RESULT_OK:
//                sbView.setBackgroundColor(Color.parseColor("#0F9D58"));
//                break;
//            case Constantes.RESULT_ERROR:
//                sbView.setBackgroundColor(Color.parseColor("#FA5858"));
//                break;
//            case Constantes.RESULT_WARNING:
//                sbView.setBackgroundColor(Color.parseColor("#FFB22C"));
//                break;
//        }
//        mSnackBar.show();

    }

    public static void LOG_D(String texto)
    {
        if (DEBUG)
            Log.d(TAG, texto);
    }
    public static void LOG_E(String error)
    {
        if (DEBUG)
            Log.e(TAG, error);
    }
    public static void LOG_I(String info)
    {
        if (DEBUG)
            Log.i(TAG, info);
    }

    public void toast(String texto, Context context)
    {
        Toast.makeText(context, texto, Toast.LENGTH_SHORT).show();
    }

    public static void toHideSoftKeyboard(Activity activity) {
        if(activity.getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    /**
     * Shows the soft keyboard
     */
    public static void toShowSoftKeyboard(Activity activity, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
        view.requestFocus();
        inputMethodManager.showSoftInput(view, 0);
    }


    /**
     * Simple función para comprobar si la cadena recibida tiene
     * formato INT.
     * @param linea String que se aplicará matches.
     * @return TRUE si la cadena es INT, FALSE caso contrario..
     **/
    public boolean lineIsInt(String linea) {
        Pattern pattern = Pattern.compile(INTREGEX);
        Matcher matcher = pattern.matcher(linea);
        return matcher.matches();
//        return Pattern.matches(INTREGEX, linea);
    }

    /**
     * Simple función que retornará la versión del aplicativo..
     * @param context Contexto de la actividad actualizar..
     * @param indice int indice de petición
     * @return
     */


    public String getVersion(Context context, int indice)
    {
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            if (pInfo == null)
                return "";
            if(indice == 1) // retorna numero de versión
                return pInfo.versionCode + "";
            else	// Retorna nombre de version
                return pInfo.versionName + "";
        } catch (PackageManager.NameNotFoundException e) {}
        catch (Exception e){}
        return "";
    }
    /**
     * Simple funcion que retornará el IMEI del
     * dispotivo..
     * @return String Imei
     */
    @SuppressLint("MissingPermission")
    public String getImei(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    public int getBattery(Context context){
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);
        return batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
    }

    /**
     *  FUNCIÓN AUXILIAR QUE DEVOLVERÁ LA FECHA Y HORA EN ALGUNOS FORMATOS
     *  @param idutils IDUTILS indice para que evalue que Formato se requiere.
     *  @return String Fecha Formateada.
     **/
    public String getFechaHora(IDUTILS idutils)
    {
        String formatoFecha  = (idutils == IDUTILS.HHMMSS) ? "HH:mm:ss" :
                (idutils == IDUTILS.DDHHMMSS) ? "dd/MM/yyyy HH:mm:ss" :
                        (idutils == IDUTILS.DDHHMMSSSS) ?"dd/MM/yyyy HH:mm:ss.SSS" :
                                (idutils == IDUTILS.YYYYMMMDD) ?"yyyy-MM-dd" :
                                        (idutils == IDUTILS.DDMMAA) ?"dd/MM/yyyy" :
                                                (idutils == IDUTILS.DDMMA) ?"dd/MM/yy" :
                                                        (idutils == IDUTILS.AAAAMMDD) ?"yyyy/MM/dd HH:mm:ss" : "";
        return simpleDateFormat(formatoFecha).format(Calendar.getInstance().getTime());
    }


    /**
     * @param tPasado Time
     * @return long seconds
     */
    public long getDiferenciaInSeconds(Time tPasado)
    {
        Time tActual = new Time();
        tActual.setToNow();
        return TimeUnit.MILLISECONDS.toSeconds(tActual.toMillis(true) - tPasado.toMillis(true));
    }

    /**
     * Combinamos dos arreglos y retornamos un único arreglo
     * @param a Primer Objeto Arreglo   -
     * @param b Segundo Objeto Arreglo  -
     * @return String[]
     */
    public String[] unirArrayString(String[] a, String[] b)
    {
        int length          = a.length + b.length;
        String[] result     = new String[length];          // 3 FILAS / 3 COLUMNAS
        //        region SINTAXYS DEVELOPER ARRAYCOPY
        //         SINTAXYS DEVELOPER
        //         arraycopy (Object src, int srcPos, Object dst, int dstPos, int length)
        //         arraycopy ( matriz origén, indice a partir del contenido origén, matriz destino, índice a partir del contenido destino, número de elementos a copiar.
        //         endregion
        System.arraycopy(a, 0, result, 0, a.length);        // Copiamos la primera matriz
        System.arraycopy(b, 0, result, a.length, b.length); // Copiamos la segunda matriz
        return result;
    }

    /**
     * @param formatoFecha String  FormatoFecha
     * @return SimpleDateFormat con lenguaje ESPANIOL y País PERÚ
     **/
    private SimpleDateFormat simpleDateFormat(String formatoFecha)
    {
        return new SimpleDateFormat(formatoFecha, new Locale("es", "pe"));
    }

    public void pintarBackground(View view, Drawable drawable)throws Exception
    {
        int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN)
            view.setBackgroundDrawable(drawable);
        else
            view.setBackground(drawable);
    }
//    public static String convertirMetrosToKilometros(String metros){
//
//
//        if (!metros.equals("")){
//            return String.format(Locale.getDefault(), "%.2f", (Float.parseFloat(metros)/1000));
//        }
//        return "";
//    }

    public static String convertirMetrosToKilometros(int numMetros){
        float numKilometos;
        if (numMetros > 0){
            numKilometos = (Float.parseFloat(String.valueOf(numMetros))/1000);
            return String.format(Locale.getDefault(), "%.2f", (numKilometos));
        }
        return "";
    }

    public static String convertirMetrosToKilometros(String numMetros){
        if (!numMetros.equals("")){
            float numMetro = Float.parseFloat(numMetros);
            if (numMetro >= 1000){
                return String.format(Locale.getDefault(), "%.1fKM", (numMetro/1000f));
            }
            return String.format(Locale.getDefault(), "%.1fM", numMetro);
        }
        return "";
    }


    /**
     * Es para que los días o meses se muestren con 2 dígitos.
     * @param n
     * @return
     */
    public static String twoDigits(int n) {
        return (n<=9) ? ("0"+n) : String.valueOf(n);
    }

    /**
     * ESte metodo guarda los recientes si en caso me es retornada una ruta existente, ver si podemos reutilzar el codigo
     * para usarlo en recientes, favorito, casa, trabajo
     */
//    public void guardarRecientes(Context context, PlaceInfo placeInfo){
//
//        if (new LugarDestinoDAO(context).getLugarDestinoExisteEnRecientes(placeInfo).getLugarNombre().equals("")){
//            LugarDestino lugarDestino = new LugarDestino();
//            lugarDestino.setCodTipoLugarDestino(LugarDestino.RECIENTES);
//            lugarDestino.setLugarNombre(String.valueOf(placeInfo.getName()));
//            lugarDestino.setLugarDireccion(String.valueOf(placeInfo.getAddress()));
//
//            lugarDestino.setNumPhone(String.valueOf(placeInfo.getPhoneNumber()));
//            lugarDestino.setWebSiteUrl(String.valueOf(placeInfo.getWebsiteUri()));
//            lugarDestino.setRating(String.valueOf(placeInfo.getRating()));
//            lugarDestino.setAttributions(String.valueOf(placeInfo.getAttributions()));
//
//            lugarDestino.setLat(placeInfo.getLatString());
//            lugarDestino.setLng(placeInfo.getLngString());
//            lugarDestino.setFechaHora(new Utils().getFechaHora(IDUTILS.DDHHMMSS));
//            new LugarDestinoDAO(context).insertarLugarDestino(lugarDestino);
//        }
//    }

//    public void guardarRecientes(Context context, Direccion direccion){
//
//        if (new LugarDestinoDAO(context).getLugarDestinoExisteEnRecientes(direccion).getLugarNombre().equals("")){
//            LugarDestino lugarDestino = new LugarDestino();
//            lugarDestino.setCodTipoLugarDestino(LugarDestino.RECIENTES);
//            lugarDestino.setLugarNombre(String.valueOf(direccion.getStreetName()));
//            lugarDestino.setLugarDireccion(String.valueOf(direccion.getFormattedAddress()));
//            lugarDestino.setLat(direccion.getLat());
//            lugarDestino.setLng(direccion.getLng());
//            lugarDestino.setFechaHora(new Utils().getFechaHora(IDUTILS.DDHHMMSS));
//            new LugarDestinoDAO(context).insertarLugarDestino(lugarDestino);
//        }
//
//    }

//    public void guardarRecientes(Context context, DestinoMarker destinoMarker){
//
//        if (new LugarDestinoDAO(context).getLugarDestinoExisteEnRecientes(destinoMarker).getLugarNombre().equals("")){
//            LugarDestino lugarDestino = new LugarDestino();
//            lugarDestino.setCodTipoLugarDestino(LugarDestino.RECIENTES);
//            lugarDestino.setLugarNombre(String.valueOf(destinoMarker.getStreetName()));
//            lugarDestino.setLugarDireccion(String.valueOf(destinoMarker.getFormattedAddress()));
//            lugarDestino.setLat(String.valueOf(destinoMarker.getLatLng().latitude));
//            lugarDestino.setLng(String.valueOf(destinoMarker.getLatLng().longitude));
//            lugarDestino.setFechaHora(new Utils().getFechaHora(IDUTILS.DDHHMMSS));
//            new LugarDestinoDAO(context).insertarLugarDestino(lugarDestino);
//        }
//
//    }

    public static String diferenciaFechaDDHHMMSS(Date startDate){
        String salida = "Hace 1 Segundo";
        //milliseconds
        try {
            long different = obtenerDate().getTime() - startDate.getTime();
//            long different = startDate.getTime() - obtenerDate().getTime();

            Date dat1 = startDate;
            Date dat2 = obtenerDate();

            System.out.println("startDate : " + startDate);
            System.out.println("endDate : "+ obtenerDate());
            System.out.println("diferencia : " + different);

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = different / daysInMilli;
            different = different % daysInMilli;

            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;
            different = different % minutesInMilli;

            long elapsedSeconds = different / secondsInMilli;
            if((int)elapsedDays > 0){
                salida = MessageFormat.format("Hace {0} "+mostrarTexto((int)elapsedDays,"D"), elapsedDays); //fecha
            }else if((int)elapsedHours > 0){
                salida = MessageFormat.format("Hace {0} "+mostrarTexto((int)elapsedHours,"H"), elapsedHours);
            }else if((int)elapsedMinutes > 0){
                salida = MessageFormat.format("Hace {0} "+mostrarTexto((int)elapsedMinutes,"M"), elapsedMinutes);
            }else if((int)elapsedSeconds >= 0){
                salida = MessageFormat.format("Hace {0} "+mostrarTexto((int)elapsedSeconds,"S"), elapsedSeconds);
            }
        }catch (Exception e){
            salida = "Sin fecha";
        }
        return  salida;
    }

    private static String mostrarTexto(int entero,String tipo){
        String salida = "";
        switch (tipo){
            case "D":
                salida = "Dia"+verificarNumero(entero);
                break;
            case "H":
                salida = "Hora"+verificarNumero(entero);
                break;
            case "M":
                salida = "Minuto"+verificarNumero(entero);
                break;
            case "S":
                salida = "Segundo"+verificarNumero(entero);
                break;
        }
        return salida;
    }

    private static String verificarNumero(int entero){
        String salida ="";
        if(entero > 1){
            salida = "s";
        }
        return salida;
    }

    /**
     * Convierte una fecha en Date dd/MM/yyyy hh:mm:ss
     * @return Date
     */
    public static Date obtenerDate(){
        SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        Date dateSalida;
        try {
            dateSalida = simpleDateFormat.parse(getFechaHoraStatic());
        } catch (ParseException e) {
            dateSalida = new Date();
        }
        return dateSalida;
    }

    public static Date stringToDateStatic(String fecha){
        Date date = new Date();
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss", Locale.getDefault());
            date = sdf.parse(fecha);
        }catch (Exception e){
            e.printStackTrace();
        }
        return date;
    }

    private static String getFechaHoraStatic()
    {
        String formatoFecha  = "dd/MM/yyyy hh:mm:ss";
        return simpleDateFormatStatic(formatoFecha).format(Calendar.getInstance().getTime());
    }

    private static  SimpleDateFormat simpleDateFormatStatic(String formatoFecha)
    {
        return new SimpleDateFormat(formatoFecha, new Locale("es", "pe"));
    }

    public interface OnCallbackMap {
        void onFinishAnimacion();
        void onCancelAnimacion();
    }
    public void adaptarZoomMarcadores(Context context, LatLng latLngUsuario, LatLng latLngVendedor, GoogleMap mapGoogle, OnCallbackMap onCallbackMap){
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(latLngUsuario);
        builder.include(latLngVendedor);
        LatLngBounds bounds = builder.build();

        float width =  context.getResources().getDisplayMetrics().widthPixels;
        int padding = (int) (width * 0.25); // offset from edges of the map - 25% of screen

//        int padding =
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
//        mapGoogle.animateCamera(cu);
        mapGoogle.animateCamera(cu, 200, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                onCallbackMap.onFinishAnimacion();
            }

            @Override
            public void onCancel() {
                onCallbackMap.onCancelAnimacion();
            }
        });
    }

}
