package com.studyway.miloficios.utils;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;

import com.studyway.miloficios.R;

/**
 * Clase creada por Jhon Coronel
 * 15/10/2019
 * El código está cochino, lo se, pero que se puede hacer en 2 semanas :S full trabajo - estudio no hay tiempo u.u
 */
public class AnimUtils {

    public interface IAnimUtils{
        void onAnimationStart(Animation animation);
        void onAnimationEnd(Animation animation);
        void onAnimationRepeat(Animation animation);
    }
    public static void animacionEntrada(final View viewTop, final View viewBottom, IAnimUtils iAnimUtils){
        viewTop.setVisibility(View.VISIBLE);
        viewBottom.setVisibility(View.VISIBLE);
        Animation animationTop = new TranslateAnimation(0,0,0,-1900);
        animationTop.setStartTime(1500);
        animationTop.setDuration(1500);
        animationTop.setFillAfter(true);

        Animation animationBot = new TranslateAnimation(0,0,0,1900);
        animationBot.setStartTime(1500);
        animationBot.setDuration(1500);
        animationBot.setFillAfter(true);
        viewTop.animate().translationY(-1900).setDuration(1500).setStartDelay(1500);
//        viewBottom.animate().translationY(1900).setDuration(1500).setStartDelay(1500);
        viewTop.startAnimation(animationTop);
        viewBottom.startAnimation(animationBot);

        animationTop.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                iAnimUtils.onAnimationStart(animation);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                iAnimUtils.onAnimationEnd(animation);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                iAnimUtils.onAnimationRepeat(animation);
            }
        });

    }

    public static void animacionEntradaTest(final View viewTop, final View viewBottom){
        Animation animationTop = new TranslateAnimation(0,0,0,Math.abs(viewTop.getHeight()));
        animationTop.setDuration(1500);
        animationTop.setFillAfter(true);
        animationTop.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                viewTop.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                viewTop.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        viewTop.startAnimation(animationTop);

        Animation animationBottom = new TranslateAnimation(0,0,0,Math.abs(viewTop.getHeight()));
        animationBottom.setDuration(1500);
        animationBottom.setFillAfter(true);
        animationBottom.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                viewBottom.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                viewBottom.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        viewBottom.startAnimation(animationBottom);
    }

    public enum ETO
    {
        TOP     (1),
        BOTTOM  (2),
        LEFT    (3),
        RIGHT   (4);

        int n;
        ETO(int n){
            this.n = n;
        }
        int getInt(){
            return n;
        }
    }

    public static void animarVistaHorizontal(LinearLayout linearLayout, boolean isInformacionVisible, long duracion){
//        linearLayout.clearAnimation();
        float posXOpen = 0;  // +
        float posXClose = linearLayout.getWidth() * -1;    // -
        float posXFrom = (isInformacionVisible)? posXOpen : posXClose;
        float posXDetal = (isInformacionVisible)? posXClose : posXOpen;

        TranslateAnimation animation = new TranslateAnimation(posXFrom, posXDetal, 0, 0);
        animation.setDuration(duracion);
        animation.setFillAfter(true);
        linearLayout.startAnimation(animation);
    }

    public static boolean animarVistaVertical(View view, boolean isLlamada, long duracion){
//        linearLayout.clearAnimation();
        float posXOpen = 0;  // +
        float posXClose = view.getWidth() * -1;    // -
        float posXFrom = (isLlamada)? posXOpen : posXClose;
        float posXDetal = (isLlamada)? posXClose : posXOpen;

        TranslateAnimation animation = new TranslateAnimation(0, 0, posXFrom, posXDetal);
        animation.setDuration(duracion);
        animation.setFillAfter(true);
        view.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(isLlamada)
                    view.setVisibility(View.GONE);
                else
                    view.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        return !isLlamada;
    }

    private onVistaAnimation onVistaAnimation;
    public interface onVistaAnimation{
        void onFinishAnimation1();
        void onFinishAnimation2();
    }
    public static boolean ocultarMostrarVistas(View linearLayout, ETO toPosition, View linearLayout2, ETO toPosition2, boolean isComponenteVisible, long duracion, onVistaAnimation onVistaAnimation){
//        linearLayout.clearAnimation();
        float posXOpen = 0;  // +

        float poslinearLayoutClose = linearLayout.getWidth() * -1;
        float posXFrom1 = (isComponenteVisible)? posXOpen : poslinearLayoutClose;
        float posYToDelta1 = (isComponenteVisible)? poslinearLayoutClose : posXOpen;

        float poslinearLayout2Close = linearLayout2.getWidth() * -1;
        float posXFrom2 = (isComponenteVisible)? posXOpen : poslinearLayout2Close;
        float posYToDelta2 = (isComponenteVisible)? poslinearLayout2Close : posXOpen;

        TranslateAnimation animation = null;
        switch (toPosition){
            case TOP:
                animation = new TranslateAnimation(0, 0, posXFrom1, posYToDelta1);
                break;
            case RIGHT:
                animation = new TranslateAnimation(posXFrom1*-1, posYToDelta1*-1, 0, 0);
                break;
            case BOTTOM:
                animation = new TranslateAnimation(0, 0, posXFrom1*-1, posYToDelta1*-1);
                break;
            case LEFT:
                animation = new TranslateAnimation(posXFrom1, posYToDelta1, 0, 0);
                break;
        }
        animation.setDuration(duracion);
        animation.setFillAfter(true);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(isComponenteVisible){
                    linearLayout.setVisibility(View.INVISIBLE);
                }else{
                    linearLayout.setVisibility(View.VISIBLE);
                }
                linearLayout.clearAnimation();
                onVistaAnimation.onFinishAnimation1();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        linearLayout.startAnimation(animation);

        //Todo descomentar, cuando se acepte que el boton de ubicacion tambien se oculte con la animación
//        TranslateAnimation animation2 = null;
//        switch (toPosition2){
//            case TOP:
//                animation2 = new TranslateAnimation(0, 0, posXFrom2, posYToDelta1);
//                break;
//            case RIGHT:
//                animation2 = new TranslateAnimation(Math.abs(posXFrom2), Math.abs(posYToDelta2), 0, 0);
//                break;
//            case BOTTOM:
//                animation2 = new TranslateAnimation(0, 0, Math.abs(posXFrom2), Math.abs(posYToDelta2));
//                break;
//            case LEFT:
//                animation2 = new TranslateAnimation(posXFrom2, posYToDelta2, 0, 0);
//                break;
//        }
//        animation2.setDuration(duracion);
//        animation2.setFillAfter(true);
//        animation2.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                if(isComponenteVisible){
//                    linearLayout2.setVisibility(View.INVISIBLE);
//                }else{
//                    linearLayout2.setVisibility(View.VISIBLE);
//                }
//                linearLayout2.clearAnimation();
//                onVistaAnimation.onFinishAnimation2();
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//
//            }
//        });
//        linearLayout2.startAnimation(animation2);
        return !isComponenteVisible;
    }

    public static void ScaleAnim(Context context,View view)
    {
        Animation a = AnimationUtils.loadAnimation(context, R.anim.scale_v2);
        a.reset();
        view.clearAnimation();
        view.startAnimation(a);
    }
}
