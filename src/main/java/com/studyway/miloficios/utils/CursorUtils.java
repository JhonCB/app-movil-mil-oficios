package com.studyway.miloficios.utils;

import android.database.Cursor;


/**
 * Clase utilitaria que se encarga de obtener el valor de cursores de base de
 * datos
 * 
 * @author Diego Antonio Eduardo Rodríguez Valdivia
 * 
 */
public class CursorUtils {
	/**
	 * Método que obtiene el valor Long de un cursor de base de datos
	 * 
	 * @param cursor
	 *            - Cursor que apunta a una fila de una tabla de la base de
	 *            datos
	 * @param name
	 *            - Nombre de la columna de la cual se quiere obtener la
	 *            información
	 * @return Devuelve el valor Long de la columna indicada en el cursor
	 *         indicado. Por defecto se devolverá 0
	 */
	public static Long verificarLong(Cursor cursor, String name) {
		Long aux = 0l;
		try {
			aux = cursor.isNull(cursor.getColumnIndex(name)) ? 0l : Long.parseLong(cursor.getString(cursor.getColumnIndex(name)));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return aux;
	}

	public static Long verificarLongWithNull(Cursor cursor, String name) {
		Long aux = null;
		try {
			aux = cursor.isNull(cursor.getColumnIndex(name)) ? null : Long.parseLong(cursor.getString(cursor.getColumnIndex(name)));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return aux;
	}

	/**
	 * Método que obtiene el valor String de un cursor de base de datos
	 * 
	 * @param cursor
	 *            - Cursor que apunta a una fila de una tabla de la base de
	 *            datos
	 * @param name
	 *            - Nombre de la columna de la cual se quiere obtener la
	 *            información
	 * @return Devuelve el valor String de la columna indicada en el cursor
	 *         indicado. Por defecto se devolverá una cadena vacía
	 */
	public static String verificarString(Cursor cursor, String name) {
		String aux = "";
		try {
			aux = cursor.isNull(cursor.getColumnIndex(name)) ? "" : cursor.getString(cursor.getColumnIndex(name));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return aux;
	}

	public static String verificarStringWithNull(Cursor cursor, String name) {
		String aux = null;
		try {
			aux = cursor.isNull(cursor.getColumnIndex(name)) ? null : cursor.getString(cursor.getColumnIndex(name));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return aux;
	}

	/**
	 * Método que obtiene el valor Integer de un cursor de base de datos
	 * 
	 * @param cursor
	 *            - Cursor que apunta a una fila de una tabla de la base de
	 *            datos
	 * @param name
	 *            - Nombre de la columna de la cual se quiere obtener la
	 *            información
	 * @return Devuelve el valor Integer de la columna indicada en el cursor
	 *         indicado. Por defecto se devolverá 0
	 */
	public static Integer verificarInteger(Cursor cursor, String name) {
		int aux = 0;
		try {
			aux = cursor.isNull(cursor.getColumnIndex(name)) ? 0 : cursor.getInt(cursor.getColumnIndex(name));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return aux;
	}


	public static Integer verificarIntegerWithNull(Cursor cursor, String name) {
		Integer aux = null;
		try {
			aux = cursor.isNull(cursor.getColumnIndex(name)) ? null : cursor.getInt(cursor.getColumnIndex(name));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return aux;
	}

	/**
	 * Método que obtiene el valor Double de un cursor de base de datos
	 * 
	 * @param cursor
	 *            - Cursor que apunta a una fila de una tabla de la base de
	 *            datos
	 * @param name
	 *            - Nombre de la columna de la cual se quiere obtener la
	 *            información
	 * @return Devuelve el valor Double de la columna indicada en el cursor
	 *         indicado. Por defecto se devolverá 0
	 */
	public static Double verificarDouble(Cursor cursor, String name) {
		Double aux = 0d;
		try {
			aux = cursor.isNull(cursor.getColumnIndex(name)) ? 0d : cursor.getDouble(cursor.getColumnIndex(name));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return aux;
	}

	public static Double verificarDoubleWithNull(Cursor cursor, String name) {
		Double aux = null;
		try {
			aux = cursor.isNull(cursor.getColumnIndex(name)) ? null : cursor.getDouble(cursor.getColumnIndex(name));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return aux;
	}

	/**
	 * Método que obtiene el valor Boolean de un cursor de base de datos
	 * 
	 * @param cursor
	 *            - Cursor que apunta a una fila de una tabla de la base de
	 *            datos
	 * @param name
	 *            - Nombre de la columna de la cual se quiere obtener la
	 *            información
	 * @return Devuelve el valor Boolean de la columna indicada en el cursor
	 *         indicado. Por defecto devolverá false
	 */
	public static Boolean verificarBoolean(Cursor cursor, String name) {
		Boolean aux = false;
		Integer aux2 = 0;
		try {
			aux2 = verificarInteger(cursor, name);
			if (aux2 == 0) {
				aux = false;
			} else {
				aux = true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return aux;
	}

	public static Boolean verificarBooleanWithNull(Cursor cursor, String name) {
		Boolean aux = null;
		Integer aux2 = null;
		try {
			aux2 = verificarInteger(cursor, name);
			if (aux2 != null) {
				if (aux2 == 0)
					aux = false;
				else
					aux = true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return aux;
	}
}
