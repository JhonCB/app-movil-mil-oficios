package com.studyway.miloficios.utils;

import android.view.View;
import android.view.animation.TranslateAnimation;

/**
 * Creado por Jhon Coronel 20/11/2019
 * correo: ghoozh@gmail.com
 */
public class TransitionUtils {
    public static void animarVistaHorizontal(View view, boolean isVisible, long duracion){
//        linearLayout.clearAnimation();
        float posXOpen = 0;  // +
        float posXClose = view.getWidth();    // -
        float posXFrom = (isVisible)? posXOpen : posXClose;
        float posXDetal = (isVisible)? posXClose : posXOpen;

        TranslateAnimation animation = new TranslateAnimation(posXFrom, posXDetal, 0, 0);
        animation.setDuration(duracion);
        animation.setFillAfter(true);
        view.startAnimation(animation);
    }
}
