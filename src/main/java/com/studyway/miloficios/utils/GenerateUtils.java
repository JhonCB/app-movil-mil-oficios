package com.studyway.miloficios.utils;

import java.util.ArrayList;

/**
 * Clase creada por Jhon Coronel 16/10/2019
 * Contacto: ghoozh@gmail.com
 */
public class GenerateUtils {

    public static String obtenerNumeroAletorio(int cantNum){
        int numero = 0;
        StringBuilder salida = new StringBuilder();
        ArrayList<Integer> numeros = new ArrayList<>();

        // Genera n(cantNum) numeros entre 1 y 10
        for (int i = 1; i <= cantNum; i++) {
            numero = (int) (Math.random() * 9 + 1);
            if (numeros.contains(numero)) {
                i--;
            } else {
                numeros.add(numero);
            }
        }

        System.out.println("Se generó el código aletorio");
        for (Integer n : numeros) {
            salida.append(n);
        }
        return salida.toString();
    }
}
