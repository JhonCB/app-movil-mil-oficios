package com.studyway.miloficios.autentification;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.studyway.miloficios.utils.Utils;

import org.json.JSONObject;

import java.util.Arrays;

public class FacebookButton {
    private final static String FIELDS = "fields";
    private final static String ID = "id";
    private final static String FIRSTNAME = "first_name";
    private final static String LASTNAME = "last_name";
    private final static String NAME = "name";
    private final static String EMAIL = "email";
    private final static String PICTURES = "picture.width(200)";

    private AccessToken mAccessToken;
    private CallbackManager callbackManagerFacebook;
    private OnLoginFacebookCallback onLoginFacebookCallback;
    private LoginButton loginButton;
    Activity activity;

    public interface OnLoginFacebookCallback{
        void onSuccess(JSONObject object, GraphResponse response)throws Exception;
        void onCancel(String cancel);
        void onError(String error);
    }

    public FacebookButton(){

    }

    public FacebookButton(Activity activity, LoginButton loginButton, CallbackManager callbackManagerFacebook, OnLoginFacebookCallback onLoginFacebookCallback){
        this.activity = activity;
        this.loginButton = loginButton;
        this.callbackManagerFacebook = callbackManagerFacebook;
        this.onLoginFacebookCallback = onLoginFacebookCallback;
        init();
    }

    private void init(){
        loginButton.setReadPermissions("email");
        // Callback registration
        loginButton.registerCallback(callbackManagerFacebook, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });
        LoginManager.getInstance().registerCallback(callbackManagerFacebook, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                getUserProfile(loginResult);
            }

            @Override
            public void onCancel() {
                onLoginFacebookCallback.onCancel("Canceled");
            }

            @Override
            public void onError(FacebookException error) {
                onLoginFacebookCallback.onError(error.getMessage());
            }
        });


    }

    private void getUserProfile(LoginResult loginResult){
        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.v("FacebookButton", response.toString());
                                        try {
                                            // Application code
                                            String email = response.getJSONObject().getString("email");
                                            onLoginFacebookCallback.onSuccess(object , response);
                                        }catch(Exception e){
                                            e.printStackTrace();
                                            new Utils().toast("Error:" + e.getMessage(),activity);
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,picture,gender,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();

    }

    public void logIn(){
        LoginManager.getInstance().logInWithReadPermissions(activity , Arrays.asList("public_profile","email"));
    }

    public void logOut(){
        LoginManager.getInstance().logOut();
    }
}
