package com.studyway.miloficios.autentification;

import android.app.Activity;
import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

import java.util.Arrays;

public class Facebook {
    private final static String FIELDS = "fields";
    private final static String ID = "id";
    private final static String FIRSTNAME = "first_name";
    private final static String LASTNAME = "last_name";
    private final static String NAME = "name";
    private final static String EMAIL = "email";
    private final static String PICTURES = "picture.width(200)";

    private AccessToken mAccessToken;
    private CallbackManager callbackManagerFacebook;
    private OnLoginFacebookCallback onLoginFacebookCallback;
    Activity activity;

    public interface OnLoginFacebookCallback{
        void onSuccess(JSONObject object, GraphResponse response)throws Exception;
        void onCancel(String cancel);
        void onError(String error);
    }

    public Facebook(){

    }

    public Facebook(Activity activity, CallbackManager callbackManagerFacebook, OnLoginFacebookCallback onLoginFacebookCallback){
        this.activity = activity;
        this.callbackManagerFacebook = callbackManagerFacebook;
        this.onLoginFacebookCallback = onLoginFacebookCallback;
        init();
    }

    private void init(){
        LoginManager.getInstance().registerCallback(callbackManagerFacebook, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                mAccessToken = loginResult.getAccessToken();
                getUserProfile(mAccessToken);
                //todo prueba
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    // Application code
                                    String email = response.getJSONObject().getString("email");
                                }catch(Exception e){
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
                //todo end
            }

            @Override
            public void onCancel() {
                onLoginFacebookCallback.onCancel("Canceled");
            }

            @Override
            public void onError(FacebookException error) {
                onLoginFacebookCallback.onError(error.getMessage());
            }
        });


    }

    private void getUserProfile(AccessToken accessToken){
        GraphRequest request = GraphRequest.newMeRequest(accessToken, (object, response) -> {
            try {
                onLoginFacebookCallback.onSuccess(object , response);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString(FIELDS, String.format("%s, %s, %s, %s, %s, %s", ID, FIRSTNAME, LASTNAME, NAME, EMAIL, PICTURES));
        request.setParameters(parameters);
        request.executeAsync();

    }

    public void logIn(){
        LoginManager.getInstance().logInWithReadPermissions(activity , Arrays.asList("public_profile","email"));
    }

    public void logOut(){
        LoginManager.getInstance().logOut();
    }
}
