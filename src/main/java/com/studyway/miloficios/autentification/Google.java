package com.studyway.miloficios.autentification;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.Nullable;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

public class Google {
    private Context context;
    private GoogleSignInClient mGoogleSignInClient;
    private int RC_SIGN_IN = 10;
    private final static String TAG = "APPMOVIL";

    private OnLoginGoogleCallback onLoginGoogleCallback;

    public interface OnLoginGoogleCallback {
        void onResult(GoogleSignInAccount account);
    }

    public Google(Context context, GoogleSignInClient googleSignInClient, OnLoginGoogleCallback onLoginGoogleCallback){
        this.onLoginGoogleCallback = onLoginGoogleCallback;
        this.mGoogleSignInClient = googleSignInClient;
        this.context = context;
    }

    public Google(Context context){
        this.context = context;
    }

    public void onStart(){
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(context);
        updateUI(account);
    }

    private void updateUI(@Nullable GoogleSignInAccount account) {
        if (account != null) {
            onLoginGoogleCallback.onResult(account);
        } else {
            onLoginGoogleCallback.onResult(null);
        }
    }

    public void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        ((Activity)context).startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void onRequestActivity(int requestCode, int resultCode, Intent data){
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            updateUI(account);
        } catch (ApiException e) {
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }

    public void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener((Activity) context, task -> {
                    // [START_EXCLUDE]
                    updateUI(null);
                    // [END_EXCLUDE]
                });
    }

    public void revokeAccess() {
        mGoogleSignInClient.revokeAccess()
                .addOnCompleteListener((Activity) context, task -> {
                    // [START_EXCLUDE]
                    updateUI(null);
                    // [END_EXCLUDE]
                });
    }
}
