package com.studyway.miloficios.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.studyway.miloficios.R;
import com.studyway.miloficios.entities.Servicio;
import com.studyway.miloficios.utils.GlideUtils;
import com.studyway.miloficios.utils.Utils;

import java.util.ArrayList;
import java.util.Date;

public class AdapterRvServicio  extends RecyclerView.Adapter<AdapterRvServicio.ViewHolder> implements Filterable
{

    private Context context;
    private ArrayList<Servicio> alServicio;
    private ArrayList<Servicio> alServicioFilter;
    private int lastItemPosition = 0;

    private IAdapterServicio iAdapterServicio;
    public interface IAdapterServicio{
        void onClick(Servicio servicio);
        void onScrolledUp(int lastItemPosition);
        void onScrolledDown(int lastItemPosition);
        void onScrolledTwoItem(boolean isTwoItem);
    }

    public AdapterRvServicio(Context context, ArrayList<Servicio> alServicio, IAdapterServicio iAdapterServicio) {
        this.alServicio = alServicio;
        this.alServicioFilter = alServicio;
        this.context = context;
        this.iAdapterServicio = iAdapterServicio;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return alServicioFilter.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_servicio,null,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(position > 4){
            iAdapterServicio.onScrolledTwoItem(true);
        }
        if(position == 0){
            iAdapterServicio.onScrolledTwoItem(false);
        }
        if (position > lastItemPosition) {
            // Scrolled Down
            iAdapterServicio.onScrolledDown(position);
        }
        else {
            // Scrolled Up
            iAdapterServicio.onScrolledUp(position);
        }

        lastItemPosition = position;
        Servicio servicio = alServicioFilter.get(position);
        holder.setViews(servicio);

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView cvItemServicio;
        ImageView ivFoto;
        TextView tvTitulo;
        TextView tvDescripcionServicio;
        TextView tvDescripcion;
        TextView tvFechaHora;
        TextView tvUbicacion;
//        TextView tvCosto;


        private ViewHolder(@NonNull View view) {
            super(view);
            cvItemServicio = (CardView)view.findViewById(R.id.cvItemServicio);
            ivFoto = (ImageView)view.findViewById(R.id.ivFoto);
            tvTitulo = (TextView)view.findViewById(R.id.tvTitulo);
            tvDescripcionServicio = (TextView)view.findViewById(R.id.tvDescripcionServicio);
            tvDescripcion = (TextView)view.findViewById(R.id.tvDescripcion);
            tvFechaHora = (TextView)view.findViewById(R.id.tvFechaHora);
            tvUbicacion = (TextView)view.findViewById(R.id.tvUbicacion);
//            tvCosto = (TextView)view.findViewById(R.id.tvCosto);

        }
        public void setViews(Servicio servicio) {
//            ivModalidad.setImageDrawable(getConceptoImagen(servicio));
            tvTitulo.setText(servicio.getNombreUsuPub());
            tvDescripcionServicio.setText(String.valueOf(servicio.getDescripcionServicio()));
            tvDescripcion.setText(String.valueOf(servicio.getDescripcion()));
            tvFechaHora.setText(calcularHora(Utils.stringToDateStatic(servicio.getFechaInicio()))); //mostrarlo más agradable luego
            tvUbicacion.setText(String.valueOf(servicio.getUbicacion()));
//            tvCosto.setText(String.valueOf(servicio.getUbicacion()));
            GlideUtils.setFotoServicio(context, servicio.getUrlFoto(), ivFoto, new GlideUtils.OnCallback() {
                @Override
                public void onResult(boolean isExitoso) {

                }
            });
            cvItemServicio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    iAdapterServicio.onClick(servicio);
                }
            });
        }

        /**
         * Calcula la diferencia entre la fecha de la notificacion y la fecha de hoy
         * @param fechaNotificacion
         */
        private String calcularHora(Date fechaNotificacion){
            try {
                return Utils.diferenciaFechaDDHHMMSS(fechaNotificacion);
            }catch (Exception e){
                return "";
            }
        }

//        private Drawable getConceptoImagen(HistorialPunto historialPunto){
//            //Todo: Pedir que cambien el servicio y que retorne codigo de concepto para mejorar este flujo
//            if(historialPunto.getCodConcepto() == puntoConceptoDAO.getComentar().getCodPuntoConcepto()){
//                return itemView.getResources().getDrawable(R.drawable.ic_comentario_64_circle);
//            }else if (historialPunto.getCodConcepto() == puntoConceptoDAO.getCompartirRedSocial().getCodPuntoConcepto()){
//                return itemView.getResources().getDrawable(R.drawable.ic_invitar_64);
//            }else if(historialPunto.getCodConcepto() == puntoConceptoDAO.getRegistroInicial().getCodPuntoConcepto()){
//                return itemView.getResources().getDrawable(R.drawable.ic_registro_inicial_64_circle);
//            }else if(historialPunto.getCodConcepto() == puntoConceptoDAO.getCompartirCodigoReferido().getCodPuntoConcepto()){
//                return itemView.getResources().getDrawable(R.drawable.ic_referidos_64);
//            }else if(historialPunto.getCodConcepto() == puntoConceptoDAO.getComentarGooglePlay().getCodPuntoConcepto()){
//                return itemView.getResources().getDrawable(R.drawable.ic_playstore_64_circle);
//            }
//
////            if(historialPunto.getNomConcepto().equals("Reporte y calificacion")){
////                return itemView.getResources().getDrawable(R.drawable.ic_comentario_64_circle);
////            }else if (historialPunto.getNomConcepto().equals("Compartir en redes sociales")){
////                return itemView.getResources().getDrawable(R.drawable.ic_invitar_64);
////            }else if(historialPunto.getNomConcepto().equals("Registro Inicial")){
////                return itemView.getResources().getDrawable(R.drawable.ic_registro_inicial_64_circle);
////            }else if(historialPunto.getNomConcepto().equals("Codigo promoción invitación")){
////                return itemView.getResources().getDrawable(R.drawable.ic_referidos_64);
////            }else if(historialPunto.getNomConcepto().equals("Comentario en Google Play")){
////                return itemView.getResources().getDrawable(R.drawable.ic_playstore_64_circle);
////            }
//            return itemView.getResources().getDrawable(R.drawable.ic_sol_64);
//        }

    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                alServicioFilter = (ArrayList<Servicio>) results.values; // has the filtered values
//                alEmpresa = (ArrayList<Empresa>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<Servicio> FilteredArrList = new ArrayList<Servicio>();

                if (alServicio == null) {
                    alServicio = new ArrayList<Servicio>(alServicioFilter); // saves the original data in mOriginalValues
                }

                /********
                 *
                 *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                 *  else does the Filtering and returns FilteredArrList(Filtered)
                 *
                 ********/
                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = alServicio.size();
                    results.values = alServicio;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < alServicio.size(); i++) {
                        String data = alServicio.get(i).getDescripcion();
//                        String data2 = alServicio.get(i).getNombreUsuPub();
                        // || data.toLowerCase().contains(constraint.toString())
                        if (data.toLowerCase().contains(constraint.toString())) {//todo: opcional, buscar por startsWith y si no encuentra, buscar por contains
                            FilteredArrList.add(new Servicio(alServicio.get(i).codServicio,
                                    alServicio.get(i).nombreUsuPub,
                                    alServicio.get(i).calificacion,
                                    alServicio.get(i).fechaInicio,
                                    alServicio.get(i).fechaFin,
                                    alServicio.get(i).estado,
                                    alServicio.get(i).descripcion,
                                    alServicio.get(i).descripcionServicio,
                                    alServicio.get(i).ubicacion,
                                    alServicio.get(i).urlFoto,
                                    alServicio.get(i).latitud,
                                    alServicio.get(i).longitud));
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
    }
}