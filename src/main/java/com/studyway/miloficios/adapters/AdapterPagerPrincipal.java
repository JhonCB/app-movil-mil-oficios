package com.studyway.miloficios.adapters;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.studyway.miloficios.dao.PerfilDAO;
import com.studyway.miloficios.entities.Perfil;
import com.studyway.miloficios.fragments.FragmentAjuste;
import com.studyway.miloficios.fragments.FragmentHistorial;
import com.studyway.miloficios.fragments.FragmentHome;
import com.studyway.miloficios.fragments.FragmentHomeSocio;
import com.studyway.miloficios.fragments.FragmentPuntos;

public class AdapterPagerPrincipal extends FragmentPagerAdapter {

    private static int NUM_ITEMS = 4;
    private String[] mTabTitles = new String[]{"Ajustes", "Ofertas","Historial","Puntos"};
    private Context context;

    public AdapterPagerPrincipal(Context context,FragmentManager fragmentManager) {
        super(fragmentManager);
        this.context = context;
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return FragmentAjuste.newInstance();
            case 1:
                if(new PerfilDAO(context).getUltimaSesion().getTipoUsuario() == Perfil.SOCIO){
                    return FragmentHomeSocio.newInstance();
                }else{
                    return FragmentHome.newInstance();
                }
            case 2:
                return FragmentHistorial.newInstance();
            case 3:
                return FragmentPuntos.newInstance();
            default:
                return null;
        }
    }

    /**
     * destruye instancias
     */
    public static void destroyInstances() {
        FragmentAjuste.destroyInstance();
        FragmentHome.destroyInstance();
        FragmentHistorial.destroyInstance();
        FragmentPuntos.destroyInstance();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabTitles[position];
    }
}
