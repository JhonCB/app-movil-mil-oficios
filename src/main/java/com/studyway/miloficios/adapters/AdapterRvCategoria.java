package com.studyway.miloficios.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.studyway.miloficios.R;
import com.studyway.miloficios.entities.Categoria;

import java.util.ArrayList;

public class AdapterRvCategoria extends RecyclerView.Adapter<AdapterRvCategoria.ViewHolder>
{

    private Context context;
    private ArrayList<Categoria> alCategoria;

    private IAdapterCategoria iAdapterCategoria;
    public interface IAdapterCategoria {
        void onClick(Categoria categoria);
    }

    public AdapterRvCategoria(Context context, ArrayList<Categoria> alCategoria, IAdapterCategoria iAdapterCategoria) {
        this.alCategoria = alCategoria;
        this.context = context;
        this.iAdapterCategoria = iAdapterCategoria;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return alCategoria.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dialog_categoria,null,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Categoria categoria = alCategoria.get(position);
        holder.setViews(categoria);

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout llContenedorCategoria;
        TextView tvNombreCategoria;


        private ViewHolder(@NonNull View view) {
            super(view);
            llContenedorCategoria = (LinearLayout)view.findViewById(R.id.llContenedorCategoria);
            tvNombreCategoria = (TextView)view.findViewById(R.id.tvNombreCategoria);

        }
        public void setViews(Categoria categoria) {
            tvNombreCategoria.setText(String.valueOf(categoria.getNomCategoria()));
            llContenedorCategoria.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    iAdapterCategoria.onClick(categoria);
                }
            });
        }
    }
}