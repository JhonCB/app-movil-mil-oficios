package com.studyway.miloficios.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.studyway.miloficios.R;
import com.studyway.miloficios.entities.SolicitudServicio;
import com.studyway.miloficios.utils.GlideUtils;

import java.util.ArrayList;

public class AdapterRvSolicitudServicio extends RecyclerView.Adapter<AdapterRvSolicitudServicio.ViewHolder> implements Filterable
{

    private Context context;
    private ArrayList<SolicitudServicio> alSolicitud;
    private ArrayList<SolicitudServicio> alSolicitudFilter;

    private IAdapterServicio iAdapterServicio;
    public interface IAdapterServicio{
        void onClick(SolicitudServicio solicitudServicio);
    }

    public AdapterRvSolicitudServicio(Context context, ArrayList<SolicitudServicio> alSolicitud, IAdapterServicio iAdapterServicio) {
        this.alSolicitud = alSolicitud;
        this.alSolicitudFilter = alSolicitud;
        this.context = context;
        this.iAdapterServicio = iAdapterServicio;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return alSolicitudFilter.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_solicitud_servicio,null,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        SolicitudServicio solicitudServicio = alSolicitudFilter.get(position);
        holder.setViews(solicitudServicio);

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView cvItemSolicitud;
        ImageView ivFoto;
        TextView tvTitulo;
        TextView tvDescripcionServicio;
        TextView tvDescripcion;
        TextView tvFechaHora;
        TextView tvUbicacion;
        TextView tvCosto;


        private ViewHolder(@NonNull View view) {
            super(view);
            cvItemSolicitud = (CardView)view.findViewById(R.id.cvItemServicio);
            ivFoto = (ImageView)view.findViewById(R.id.ivFoto);
            tvTitulo = (TextView)view.findViewById(R.id.tvTitulo);
            tvDescripcionServicio = (TextView)view.findViewById(R.id.tvDescripcionServicio);
            tvDescripcion = (TextView)view.findViewById(R.id.tvDescripcion);
            tvFechaHora = (TextView)view.findViewById(R.id.tvFechaHora);
            tvUbicacion = (TextView)view.findViewById(R.id.tvUbicacion);
            tvCosto = (TextView)view.findViewById(R.id.tvCosto);

        }
        public void setViews(SolicitudServicio solicitud) {
//            ivModalidad.setImageDrawable(getConceptoImagen(servicio));
            tvTitulo.setText(solicitud.getNomOfertante());
            tvDescripcionServicio.setText(String.valueOf(solicitud.getDesServicio()));
            tvDescripcion.setText(solicitud.getCategoria());
            tvFechaHora.setText(solicitud.getFecha());
            tvUbicacion.setText(solicitud.getUbicacion());
            tvCosto.setText("S/. "+solicitud.getCosto());
            GlideUtils.setFotoServicio(context, solicitud.getUrlFoto(), ivFoto, new GlideUtils.OnCallback() {
                @Override
                public void onResult(boolean isExitoso) {

                }
            });
            cvItemSolicitud.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    iAdapterServicio.onClick(solicitud);
                }
            });
        }

        /**
         * Calcula la diferencia entre la fecha de la notificacion y la fecha de hoy
         * @param fechaNotificacion
         */
//        private String calcularHora(Date fechaNotificacion){
//            try {
//                return Utils.diferenciaFechaDDHHMMSS(fechaNotificacion);
//            }catch (Exception e){
//                return "";
//            }
//        }

//        private Drawable getConceptoImagen(HistorialPunto historialPunto){
//            //Todo: Pedir que cambien el servicio y que retorne codigo de concepto para mejorar este flujo
//            if(historialPunto.getCodConcepto() == puntoConceptoDAO.getComentar().getCodPuntoConcepto()){
//                return itemView.getResources().getDrawable(R.drawable.ic_comentario_64_circle);
//            }else if (historialPunto.getCodConcepto() == puntoConceptoDAO.getCompartirRedSocial().getCodPuntoConcepto()){
//                return itemView.getResources().getDrawable(R.drawable.ic_invitar_64);
//            }else if(historialPunto.getCodConcepto() == puntoConceptoDAO.getRegistroInicial().getCodPuntoConcepto()){
//                return itemView.getResources().getDrawable(R.drawable.ic_registro_inicial_64_circle);
//            }else if(historialPunto.getCodConcepto() == puntoConceptoDAO.getCompartirCodigoReferido().getCodPuntoConcepto()){
//                return itemView.getResources().getDrawable(R.drawable.ic_referidos_64);
//            }else if(historialPunto.getCodConcepto() == puntoConceptoDAO.getComentarGooglePlay().getCodPuntoConcepto()){
//                return itemView.getResources().getDrawable(R.drawable.ic_playstore_64_circle);
//            }
//
////            if(historialPunto.getNomConcepto().equals("Reporte y calificacion")){
////                return itemView.getResources().getDrawable(R.drawable.ic_comentario_64_circle);
////            }else if (historialPunto.getNomConcepto().equals("Compartir en redes sociales")){
////                return itemView.getResources().getDrawable(R.drawable.ic_invitar_64);
////            }else if(historialPunto.getNomConcepto().equals("Registro Inicial")){
////                return itemView.getResources().getDrawable(R.drawable.ic_registro_inicial_64_circle);
////            }else if(historialPunto.getNomConcepto().equals("Codigo promoción invitación")){
////                return itemView.getResources().getDrawable(R.drawable.ic_referidos_64);
////            }else if(historialPunto.getNomConcepto().equals("Comentario en Google Play")){
////                return itemView.getResources().getDrawable(R.drawable.ic_playstore_64_circle);
////            }
//            return itemView.getResources().getDrawable(R.drawable.ic_sol_64);
//        }

    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                alSolicitudFilter = (ArrayList<SolicitudServicio>) results.values; // has the filtered values
//                alEmpresa = (ArrayList<Empresa>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<SolicitudServicio> FilteredArrList = new ArrayList<SolicitudServicio>();

                if (alSolicitud == null) {
                    alSolicitud = new ArrayList<SolicitudServicio>(alSolicitudFilter); // saves the original data in mOriginalValues
                }

                /********
                 *
                 *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                 *  else does the Filtering and returns FilteredArrList(Filtered)
                 *
                 ********/
                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = alSolicitud.size();
                    results.values = alSolicitud;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < alSolicitud.size(); i++) {
                        String data = alSolicitud.get(i).getCategoria();
//                        String data2 = alSolicitud.get(i).getNombreUsuPub();
                        // || data.toLowerCase().contains(constraint.toString())
                        if (data.toLowerCase().contains(constraint.toString())) {//todo: opcional, buscar por startsWith y si no encuentra, buscar por contains
                            FilteredArrList.add(new SolicitudServicio(alSolicitud.get(i).codServicio,alSolicitud.get(i).costo,alSolicitud.get(i).cotizacion,alSolicitud.get(i).estado,alSolicitud.get(i).fecha,alSolicitud.get(i).categoria,alSolicitud.get(i).urlFoto,alSolicitud.get(i).nomOfertante,alSolicitud.get(i).desServicio,alSolicitud.get(i).ubicacion));
                            }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
    }
}