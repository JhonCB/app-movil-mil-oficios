package com.studyway.miloficios.adapters;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.studyway.miloficios.R;
import com.studyway.miloficios.entities.Categoria;
import com.studyway.miloficios.entities.Mensaje;
import com.studyway.miloficios.enums.IDUTILS;
import com.studyway.miloficios.utils.Utils;

import java.util.ArrayList;

public class AdapterRvChatBot extends RecyclerView.Adapter<AdapterRvChatBot.ViewHolder>
{

    private Context context;
    private ArrayList<Mensaje> alMensaje = new ArrayList<>();
    private static final int LIST_ITEM_TYPE_1 = 1;
    private static final int LIST_ITEM_TYPE_2 = 2;
    private static final int LIST_ITEM_TYPE_3 = 3;

    private IAdapterCategoria iAdapterCategoria;
    public interface IAdapterCategoria {
        void onClick(Categoria categoria);
    }

    public AdapterRvChatBot(Context context, ArrayList<Mensaje> alMensaje) {
        this.alMensaje = alMensaje;
        this.context = context;
        this.iAdapterCategoria = iAdapterCategoria;
    }

    public void addMensaje(Mensaje mensaje){
        alMensaje.add(0,mensaje);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
//        return super.getItemViewType(position);
        if (alMensaje.get(position).getTipoMensaje() == Mensaje.HUMANO){
            return LIST_ITEM_TYPE_1;
        }
        else if(alMensaje.get(position).getTipoMensaje() == Mensaje.BOT){
            return LIST_ITEM_TYPE_2;
        }else{
            return LIST_ITEM_TYPE_3;
        }
    }

    @Override
    public int getItemCount() {
        return alMensaje.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = null;
        ViewHolder holder = null;

        switch (viewType)
        {
            case LIST_ITEM_TYPE_1:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_user,null,false);
                holder = new ViewHolder(view);
                holder.setViewHolderHumano(view);
                return  holder;
            case LIST_ITEM_TYPE_2:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_bot,null,false);
                holder = new ViewHolder(view);
                holder.setViewHolderBot(view);
                return  holder;
            case LIST_ITEM_TYPE_3:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_bot_imagen,null,false);
                holder = new ViewHolder(view);
                holder.setViewHolderBotImagen(view);
                return  holder;
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Mensaje mensaje = alMensaje.get(position);
        if (mensaje.getTipoMensaje() == Mensaje.HUMANO){
            holder.viewHolderHumano.setViews(mensaje);
        }
        else if(mensaje.getTipoMensaje() == Mensaje.BOT){
            holder.viewHolderBot.setViews(mensaje);
        }else{
            holder.viewHolderBotImagen.setViews(mensaje);
        }


    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ViewHolderHumano viewHolderHumano;
        ViewHolderBot viewHolderBot;
        ViewHolderBotImagen viewHolderBotImagen;

        private ViewHolder(@NonNull View view) {
            super(view);
        }

        private void setViewHolderHumano(View view){
            this.viewHolderHumano = new ViewHolderHumano(view);
        }

        private void setViewHolderBot(View view) {
            this.viewHolderBot = new ViewHolderBot(view);
        }

        private void setViewHolderBotImagen(View view) {
            this.viewHolderBotImagen = new ViewHolderBotImagen(view);
        }
    }

    public class ViewHolderHumano{
        TextView tvMensaje;
        TextView tvFechaHora;


        private ViewHolderHumano(@NonNull View view) {
            tvMensaje = (TextView)view.findViewById(R.id.tvMensaje);
            tvFechaHora = (TextView)view.findViewById(R.id.tvFechaHora);

        }
        public void setViews(Mensaje mensaje) {
            tvMensaje.setText(String.valueOf(mensaje.getMensaje()));
            tvFechaHora.setText(mensaje.getFecha());
        }
    }

    public class ViewHolderBot{
        TextView tvMensaje;
        TextView tvFechaHora;


        private ViewHolderBot(@NonNull View view) {
            tvMensaje = (TextView)view.findViewById(R.id.tvMensaje);
            tvFechaHora = (TextView)view.findViewById(R.id.tvFechaHora);

        }
        public void setViews(Mensaje mensaje) {
            tvMensaje.setText(String.valueOf(mensaje.getMensaje()));
            tvFechaHora.setText(mensaje.getFecha());
        }
    }

    public class ViewHolderBotImagen{
        TextView tvMensaje;
        TextView tvFechaHora;


        private ViewHolderBotImagen(@NonNull View view) {
            tvMensaje = (TextView)view.findViewById(R.id.tvMensaje);
            tvFechaHora = (TextView)view.findViewById(R.id.tvFechaHora);

        }
        public void setViews(Mensaje mensaje) {
            tvMensaje.setText(String.valueOf(mensaje.getMensaje()));
            tvFechaHora.setText(mensaje.getFecha());
        }
    }
}