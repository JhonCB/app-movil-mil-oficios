package com.studyway.miloficios.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;

import com.studyway.miloficios.R;

import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

/**
 * Creado por Jhon Coronel 15/10/2019
 * Contacto: ghoozh@gmail.com
 */

public class BaseFragment extends Fragment{
        public final static String TAG = "BaseFragment";
        final String nomFuente = "apple_san_francisco_light";

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
            ViewPump.init(ViewPump.builder().addInterceptor(new CalligraphyInterceptor(
                    new CalligraphyConfig.Builder()
                            .setDefaultFontPath("font/" + nomFuente + ".ttf")
                            .setFontAttrId(R.attr.fontPath)
                            .build())).build());
        }

    //TODO : aplicar el tipo de fuente a todo el activity
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(ViewPumpContextWrapper.wrap(context));
    }
}

