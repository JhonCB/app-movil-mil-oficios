package com.studyway.miloficios.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.speech.tts.Voice;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.studyway.miloficios.R;
import com.studyway.miloficios.activities.ActivityDetalleServicio;
import com.studyway.miloficios.adapters.AdapterRvChatBot;
import com.studyway.miloficios.adapters.AdapterRvServicio;
import com.studyway.miloficios.asyntask.ListaServicioPorSocioTask;
import com.studyway.miloficios.asyntask.ListaServicioTask;
import com.studyway.miloficios.asyntask.ListaServicioXCategoriaTask;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.dao.PerfilDAO;
import com.studyway.miloficios.dialogs.CategoriaDialog;
import com.studyway.miloficios.dialogs.RegistrarServicioDialog;
import com.studyway.miloficios.entities.Categoria;
import com.studyway.miloficios.entities.Mensaje;
import com.studyway.miloficios.entities.Servicio;
import com.studyway.miloficios.utils.Utils;
import com.studyway.waveswiperefreshlayout.WaveSwipeRefreshLayout;

import java.util.ArrayList;
import java.util.Locale;

import ai.api.AIServiceException;
import ai.api.DefaultAIListener;
import ai.api.android.AIConfiguration;
import ai.api.android.AIService;
import ai.api.model.AIError;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;
import ai.api.model.Result;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Creado por Jhon Coronel 15/10/2019
 * Contacto: ghoozh@gmail.com
 */
public class FragmentHomeSocio extends BaseFragment {

    @BindView(R.id.wsrlHome) WaveSwipeRefreshLayout wsrlHome;
    @BindView(R.id.rvServicioSocio) RecyclerView rvServicioSocio;

    private AdapterRvServicio adapterRvServicio;
    private static FragmentHomeSocio fragmentHome;

    public FragmentHomeSocio() {
        // Required empty public constructor
    }

    public static FragmentHomeSocio newInstance() {
        if (fragmentHome == null){
            fragmentHome =  new FragmentHomeSocio();
        }
        return fragmentHome;
    }

    public static void destroyInstance()
    {
        fragmentHome = null;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_socio, container,false);
        ButterKnife.bind(this,  view);
        initSwipe();
        consultarServicios();
        return view;
    }

    private void initSwipe(){
        wsrlHome.setColorSchemeColors(getResources().getColor(R.color.colorPrimaryDark));
        wsrlHome.setWaveColor(getResources().getColor(R.color.colorPrimary));
        wsrlHome.setShadowRadius(5);
        wsrlHome.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
            @Override public void onRefresh() {
                // Do work to refresh the list here.
                    consultarServicios();
            }
        });
    }

    private void consultarServicios(){
        if(wsrlHome != null){
            wsrlHome.setRefreshing(true);
        }
        try {
            new ListaServicioPorSocioTask(getContext(),Integer.parseInt(new PerfilDAO(getContext()).getUltimaSesion().getIdPerfil()), new ListaServicioPorSocioTask.IListaServicioSocio() {
                @Override
                public void onResult(int codResultado, String desResultado, ArrayList<Servicio> alServicio) {
                    if(wsrlHome != null){
                        wsrlHome.setRefreshing(false);
                    }
                    if(codResultado == Constantes.RESULT_OK){
                        adapterRvServicio = new AdapterRvServicio(getContext(), alServicio, new AdapterRvServicio.IAdapterServicio() {
                            @Override
                            public void onClick(Servicio servicio) {
                                Intent intent = new Intent(getActivity(), ActivityDetalleServicio.class);
                                intent.putExtra("iServicio",servicio);
                                startActivity(intent);
                            }

                            @Override
                            public void onScrolledUp(int lastItemPosition) {

                            }

                            @Override
                            public void onScrolledDown(int lastItemPosition) {

                            }

                            @Override
                            public void onScrolledTwoItem(boolean isTwoItem) {

                            }

                        });
                        rvServicioSocio.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,false));
                        rvServicioSocio.setAdapter(adapterRvServicio);
                    }else{
                        new Utils().toast(desResultado,getContext());
                    }
                }
            }).execute();
        }catch (Exception e){
            if(wsrlHome != null){
                wsrlHome.setRefreshing(false);
            }
        }


    }

    @OnClick({R.id.rlContenedor,R.id.btCrearOferta})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.rlContenedor:
                Utils.toHideSoftKeyboard(getActivity());
                break;
            case R.id.btCrearOferta:
                new RegistrarServicioDialog(getContext(), new RegistrarServicioDialog.IRegistro() {
                    @Override
                    public void onCorrecto(String descripcion) {
                        new Utils().toast(descripcion,getContext());
                        consultarServicios();
                    }

                    @Override
                    public void onError(String descripcion) {
                        new Utils().toast(descripcion,getContext());
                    }
                }).show();
                break;
        }
    }



    @Override
    public void onResume() {
        super.onResume();
       // isBuscarOpen = false;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
