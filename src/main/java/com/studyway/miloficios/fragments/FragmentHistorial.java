package com.studyway.miloficios.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.studyway.miloficios.R;
import com.studyway.miloficios.adapters.AdapterRvSolicitudServicio;
import com.studyway.miloficios.asyntask.ListaServicioPorSocioTask;
import com.studyway.miloficios.asyntask.ListaSolicitudPorSocioTask;
import com.studyway.miloficios.asyntask.ListaSolicitudPorUsuarioTask;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.dao.PerfilDAO;
import com.studyway.miloficios.entities.Perfil;
import com.studyway.miloficios.spf.SPF;
import com.studyway.miloficios.utils.Utils;
import com.studyway.waveswiperefreshlayout.WaveSwipeRefreshLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Creado por Jhon Coronel 15/10/2019
 * Contacto: ghoozh@gmail.com
 */
public class FragmentHistorial extends BaseFragment {
    @BindView(R.id.rvListaSolicitud) RecyclerView rvListaSolicitud;
    @BindView(R.id.wsrlHome) WaveSwipeRefreshLayout wsrlHome;

    private static FragmentHistorial fragmentHome;
    private AdapterRvSolicitudServicio AdapterRvSolicitudServicio;

    public FragmentHistorial() {
        // Required empty public constructor
    }

    public static FragmentHistorial newInstance() {
        if (fragmentHome == null){
            fragmentHome =  new FragmentHistorial();
        }
        return fragmentHome;
    }

    public static void destroyInstance()
    {
        fragmentHome = null;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_historial, container,false);
        ButterKnife.bind(this,  view);
        initSwipe();
        consultarSolicitudes();
        return view;
    }

    private void initSwipe(){
        wsrlHome.setColorSchemeColors(getResources().getColor(R.color.colorPrimaryDark));
        wsrlHome.setWaveColor(getResources().getColor(R.color.colorPrimary));
        wsrlHome.setShadowRadius(5);
        wsrlHome.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
            @Override public void onRefresh() {
                    consultarSolicitudes();
            }
        });
    }

    private void consultarSolicitudes(){
        if(wsrlHome != null){
            wsrlHome.setRefreshing(true);
        }
        if(new PerfilDAO(getContext()).getUltimaSesion().getTipoUsuario() == Perfil.SOCIO){
            new ListaSolicitudPorSocioTask(getContext(), SPF.getInstance(getContext()).getKeyCodigoTemporal(), (codResultado, desResultado, alSolicitud) -> {
                if(wsrlHome != null){
                    wsrlHome.setRefreshing(false);
                }
                if(codResultado == Constantes.RESULT_OK){
                    AdapterRvSolicitudServicio = new AdapterRvSolicitudServicio(getContext(), alSolicitud, solicitud -> {
//                    Intent intent = new Intent(getActivity(), ActivityDetalleServicio.class);
//                    intent.putExtra("iServicio",alSolicitud);
//                    startActivity(intent);
                    });
                    rvListaSolicitud.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,false));
                    rvListaSolicitud.setAdapter(AdapterRvSolicitudServicio);
//                new Utils().toast(desResultado,getContext());
                }else{
                    new Utils().toast(desResultado,getContext());
                }
            }).execute();
        }else{
            new ListaSolicitudPorUsuarioTask(getContext(), SPF.getInstance(getContext()).getKeyCodigoTemporal(), (codResultado, desResultado, alSolicitud) -> {
                if(wsrlHome != null){
                    wsrlHome.setRefreshing(false);
                }
                if(codResultado == Constantes.RESULT_OK){
                    AdapterRvSolicitudServicio = new AdapterRvSolicitudServicio(getContext(), alSolicitud, solicitud -> {
//                    Intent intent = new Intent(getActivity(), ActivityDetalleServicio.class);
//                    intent.putExtra("iServicio",alSolicitud);
//                    startActivity(intent);
                    });
                    rvListaSolicitud.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,false));
                    rvListaSolicitud.setAdapter(AdapterRvSolicitudServicio);
//                new Utils().toast(desResultado,getContext());
                }else{
                    new Utils().toast(desResultado,getContext());
                }
            }).execute();
        }
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
