package com.studyway.miloficios.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.studyway.miloficios.R;

import butterknife.ButterKnife;

/**
 * Creado por Jhon Coronel 15/10/2019
 * Contacto: ghoozh@gmail.com
 */
public class FragmentPuntos extends BaseFragment {
    private static FragmentPuntos fragmentHome;
    public FragmentPuntos() {
        // Required empty public constructor
    }

    public static FragmentPuntos newInstance() {
        if (fragmentHome == null){
            fragmentHome =  new FragmentPuntos();
        }
        return fragmentHome;
    }

    public static void destroyInstance()
    {
        fragmentHome = null;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_puntos, container,false);
        ButterKnife.bind(this,  view);
        return view;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
