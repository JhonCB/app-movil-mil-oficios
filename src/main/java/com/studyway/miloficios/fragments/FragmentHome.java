package com.studyway.miloficios.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.speech.tts.Voice;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.studyway.miloficios.R;
import com.studyway.miloficios.activities.ActivityDetalleServicio;
import com.studyway.miloficios.adapters.AdapterRvChatBot;
import com.studyway.miloficios.adapters.AdapterRvServicio;
import com.studyway.miloficios.asyntask.ListaServicioTask;
import com.studyway.miloficios.asyntask.ListaServicioXCategoriaTask;
import com.studyway.miloficios.constantes.Constantes;
import com.studyway.miloficios.dao.PerfilDAO;
import com.studyway.miloficios.dialogs.CategoriaDialog;
import com.studyway.miloficios.entities.Categoria;
import com.studyway.miloficios.entities.Mensaje;
import com.studyway.miloficios.entities.Servicio;
import com.studyway.miloficios.utils.Utils;
import com.studyway.waveswiperefreshlayout.WaveSwipeRefreshLayout;

import java.util.ArrayList;
import java.util.Locale;

import ai.api.AIServiceException;
import ai.api.DefaultAIListener;
import ai.api.android.AIConfiguration;
import ai.api.android.AIService;
import ai.api.model.AIError;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;
import ai.api.model.Result;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Creado por Jhon Coronel 15/10/2019
 * Contacto: ghoozh@gmail.com
 */
public class FragmentHome extends BaseFragment {
    @BindView(R.id.rlContenedor) RelativeLayout rlContenedor;

    @BindView(R.id.wsrlHome) WaveSwipeRefreshLayout wsrlHome;

    @BindView(R.id.svHome) ScrollView svHome;

    @BindView(R.id.rlBuscar) RelativeLayout rlBuscar;
    @BindView(R.id.tvTituloBuscar) TextView tvTituloBuscar;

    @BindView(R.id.ivAtras) ImageView ivAtras;
    @BindView(R.id.ivFiltro) ImageView ivFiltro;
    @BindView(R.id.etBuscar) EditText etBuscar;
    @BindView(R.id.cvBuscar) CardView cvBuscar;

    @BindView(R.id.llServicioTop) LinearLayout llServicioTop;
    @BindView(R.id.llMicrofono) LinearLayout llMicrofono;
    @BindView(R.id.cvServicioTodo) CardView cvServicioTodo;
    @BindView(R.id.rvListaTodoServicio) RecyclerView rvListaTodoServicio;
    @BindView(R.id.llServicioBot) LinearLayout llServicioBot;

    @BindView(R.id.llSubir) LinearLayout llSubir;

    @BindView(R.id.rlChatBot) RelativeLayout rlChatBot;
    @BindView(R.id.ivAtrasChatBot) ImageView ivAtrasChatBot;
    @BindView(R.id.ivSonido) ImageView ivSonido;
    @BindView(R.id.rvListaChatBot) RecyclerView rvListaChatBot;
    @BindView(R.id.etChat) EditText etChat;
    @BindView(R.id.btEnviarChat) Button btEnviarChat;

    private boolean isBuscarOpen = false;
    private AdapterRvServicio adapterRvServicio;
    private static FragmentHome fragmentHome;
    private AdapterRvChatBot adapterRvChatBot;
    //mensaje inicial
    private String mensajeInicial = "Hola estimado %s, en que lo puedo ayudar?";
    private String nombreUsuario = "usuario";
    //chat bot
    private AIService aiService;
    private TextToSpeech textToSpeech;

    //estaticos
    private static final int RECORD_AUDIO = 101;
    public static boolean IS_CHATBOT_ACTIVO = false;

    public FragmentHome() {
        // Required empty public constructor
    }

    public static FragmentHome newInstance() {
        if (fragmentHome == null){
            fragmentHome =  new FragmentHome();
        }
        return fragmentHome;
    }

    public static void destroyInstance()
    {
        fragmentHome = null;
    }

    public  void ChatBotInvisible(){
        if(rlChatBot != null){
            rlChatBot.setVisibility(View.INVISIBLE);
            IS_CHATBOT_ACTIVO = false;
        }

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container,false);
        ButterKnife.bind(this,  view);
        initSwipe();
        getUsuario();
        closeViewBuscar();
        initServiceChatBot();
        initChatBot();

        return view;
    }

    private void getUsuario(){
        try {
            nombreUsuario = new PerfilDAO(getContext()).getUltimaSesion().getNombreCompleto();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initSwipe(){
        wsrlHome.setColorSchemeColors(getResources().getColor(R.color.colorPrimaryDark));
        wsrlHome.setWaveColor(getResources().getColor(R.color.colorPrimary));
        wsrlHome.setShadowRadius(5);
        wsrlHome.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
            @Override public void onRefresh() {
                // Do work to refresh the list here.
                if(isBuscarOpen){
                    consultarServicios();
                }else{
                    new Handler().postDelayed(()->{
                        new Utils().toast("isBuscarOpen : "+isBuscarOpen,getContext());
                        wsrlHome.setRefreshing(false);
                    },1000);
                }
            }
        });
    }

    private void initServiceChatBot(){
        final AIConfiguration config = new AIConfiguration("dbebb8e1e6cf47daa17d74a667f75c80",
                AIConfiguration.SupportedLanguages.Spanish,
                AIConfiguration.RecognitionEngine.System);

        aiService = AIService.getService(getContext(),config);
        aiService.setListener(new DefaultAIListener() {
            @Override
            public void onResult(AIResponse response) {
                Result result = response.getResult();
                String salida = result.getFulfillment().getSpeech();

                Mensaje mensajeUsu = new Mensaje();
                mensajeUsu.setTipoMensaje(Mensaje.HUMANO);
                mensajeUsu.setMensaje(result.getResolvedQuery());
                mensajeAutomatico(mensajeUsu);

                Mensaje mensajeBot = new Mensaje();
                mensajeBot.setTipoMensaje(Mensaje.BOT);
                mensajeBot.setMensaje(result.getFulfillment().getSpeech());


                if(result.getFulfillment().getSpeech().contains("Actualmente contamos con %s Servicios")){
                    new ListaServicioTask(getContext(), new ListaServicioTask.IListaServicio() {
                        @Override
                        public void onResult(int codResultado, String desResultado, ArrayList<Servicio> alServicio) {
                            if(codResultado == Constantes.RESULT_OK){
                                String a = String.format(Locale.getDefault(), salida, String.valueOf(alServicio.size()));
                                mensajeBot.setMensaje(a);
                                mensajeAutomatico(mensajeBot);
                                if(isVolumenOn){
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { //validación
                                        textToSpeech.speak(a,TextToSpeech.QUEUE_FLUSH,null,null);
                                    }else{
                                        new Utils().toast("Solo se reproduce en android mayor a 5",getContext());
                                    }
                                }
                            }else{

                            }
                        }
                    }).execute();
                } else{
                    mensajeAutomatico(mensajeBot);
                    if(isVolumenOn){
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { //validación
                            textToSpeech.speak(salida,TextToSpeech.QUEUE_FLUSH,null,null);
                        }else{
                            new Utils().toast("Solo se reproduce en android mayor a 5",getContext());
                        }
                    }
                }
            }

            @Override
            public void onError(AIError error) {
                new Utils().toast("No lo entendí, vuelva a hablar",getContext());
            }

            @Override
            public void onListeningCanceled() {

            }
        });

        textToSpeech = new TextToSpeech(getContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
//                getVoces();
            }
        });
    }

    private Voice getVoces(){
        Voice tmpVoice1 = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ArrayList<Voice> alVoice = new ArrayList<>(textToSpeech.getVoices());
            Log.i(TAG, "alVoice : "+alVoice.size());
        }
        return tmpVoice1;
    }

    private void initChatBot(){
        ArrayList<Mensaje> alMensaje = new ArrayList<>();

        Mensaje mensaje = new Mensaje();
        mensaje.setTipoMensaje(Mensaje.BOT_IMAGEN);
        String a = String.format(Locale.getDefault(), mensajeInicial, nombreUsuario);
        mensaje.setMensaje(a);

        alMensaje.add(mensaje);
        adapterRvChatBot = new AdapterRvChatBot(getContext(),alMensaje);
        rvListaChatBot.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,true));
        rvListaChatBot.setAdapter(adapterRvChatBot);
    }

    private boolean isVolumenOn = true;
    private boolean isChatBotInit = false;
    @SuppressLint("StaticFieldLeak")
    @OnClick({R.id.llAbrirChatBot,R.id.ivAtrasChatBot,R.id.btEnviarChat,R.id.llMicrofono,R.id.llSonido})
    public void onClickChat(View view){
        switch (view.getId()){
            case R.id.llAbrirChatBot:
//                TransitionUtils.animarVistaHorizontal(rlChatBot,false,200);
                rlChatBot.setVisibility(View.VISIBLE);
                IS_CHATBOT_ACTIVO = true;
                if(!isChatBotInit){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        String a = String.format(Locale.getDefault(), mensajeInicial, nombreUsuario);
                        textToSpeech.speak(a,TextToSpeech.QUEUE_FLUSH,null,null);
                        isChatBotInit = true;
                    }
                }
                break;
            case R.id.ivAtrasChatBot:
//                TransitionUtils.animarVistaHorizontal(rlChatBot,true,200);
                rlChatBot.setVisibility(View.INVISIBLE);
                IS_CHATBOT_ACTIVO = false;
                break;
            case R.id.btEnviarChat:
//                chatGenerado(etChat.getText().toString());
                if(!etChat.getText().toString().isEmpty()){

                    Mensaje mensajeUsu = new Mensaje();
                    mensajeUsu.setTipoMensaje(Mensaje.HUMANO);
                    mensajeUsu.setMensaje(etChat.getText().toString());
                    mensajeAutomatico(mensajeUsu);

                    AIRequest request = new AIRequest();
                    request.setQuery(etChat.getText().toString());
                    etChat.setText("");

                        new AsyncTask<AIRequest, Void, AIResponse>() {
                            @Override
                            protected AIResponse doInBackground(AIRequest... requests) {
                                final AIRequest request = requests[0];
                                try {
                                    return aiService.textRequest(request);
                                } catch (AIServiceException e) {
                                }
                                return null;
                            }
                            @Override
                            protected void onPostExecute(AIResponse response) {
                                if (response != null) {
                                    // process aiResponse here
                                    Result result = response.getResult();
                                    String salida = result.getFulfillment().getSpeech();

                                    Mensaje mensajeBot = new Mensaje();
                                    mensajeBot.setTipoMensaje(Mensaje.BOT);
                                    mensajeBot.setMensaje(result.getFulfillment().getSpeech());

                                    if(result.getFulfillment().getSpeech().contains("Actualmente contamos con %s Servicios")){
                                        new ListaServicioTask(getContext(), new ListaServicioTask.IListaServicio() {
                                            @Override
                                            public void onResult(int codResultado, String desResultado, ArrayList<Servicio> alServicio) {
                                                if(codResultado == Constantes.RESULT_OK){
                                                    String a = String.format(Locale.getDefault(), salida, String.valueOf(alServicio.size()));
                                                    mensajeBot.setMensaje(a);
                                                    mensajeAutomatico(mensajeBot);
                                                    if(isVolumenOn){
                                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { //validación
                                                            textToSpeech.speak(a,TextToSpeech.QUEUE_FLUSH,null,null);
                                                        }else{
                                                            new Utils().toast("Solo se reproduce en android mayor a 5",getContext());
                                                        }
                                                    }
                                                }else{

                                                }
                                            }
                                        }).execute();
                                    } else{
                                        mensajeAutomatico(mensajeBot);
                                        if(isVolumenOn){
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { //validación
                                                textToSpeech.speak(salida,TextToSpeech.QUEUE_FLUSH,null,null);
                                            }else{
                                                new Utils().toast("Solo se reproduce en android mayor a 5",getContext());
                                            }
                                        }
                                    }
                                }
                            }
                        }.execute(request);
                }else{
                    new Utils().toast("Ingrese su consulta",getContext());
                }
                break;
            case R.id.llMicrofono:
                if(accederMicrofono()){
                    aiService.startListening();
                }
                break;
            case R.id.llSonido:
                if(isVolumenOn){
                    ivSonido.setImageDrawable(getResources().getDrawable(R.drawable.ic_volume_off));
                    isVolumenOn = false;
                    if(textToSpeech != null)
                        textToSpeech.stop();

                }else{
                    ivSonido.setImageDrawable(getResources().getDrawable(R.drawable.ic_volume_up));
                    isVolumenOn = true;
                }
                break;

        }
    }



    private void chatGenerado(String s){
        Mensaje mensaje = new Mensaje();
        mensaje.setTipoMensaje(Mensaje.BOT);

        Mensaje mensajeusu = new Mensaje();
        mensajeusu.setMensaje(s);
        mensajeusu.setTipoMensaje(Mensaje.HUMANO);
        mensajeAutomatico(mensajeusu);
        etChat.setText("");
        if(s.contains("hola") ){
            mensaje.setMensaje("Hola, usuario que desea.");
            mensajeAutomatico(mensaje);
        }else if(s.contains("chau")){
            mensaje.setMensaje("Nos vemos!");
            mensajeAutomatico(mensaje);
        }else if(s.contains("servicio")){
            mensaje.setMensaje("Tenemos multiples servicios en 3 provincias de lima!, Lima, Huaraz, Huarmey");
            mensajeAutomatico(mensaje);
        }else if(s.contains("categoria")){
            mensaje.setMensaje("Tenemos las siguientes categorias, Carpintero, Gasfitero, Mudanzas, Seguridad, Arquitecto.");
            mensajeAutomatico(mensaje);
        }else if(s.contains("carpintero")){
            mensaje.setMensaje("Tenemos 10 usuarios que publicaron recientemente");
            mensajeAutomatico(mensaje);
        }else{
            mensaje.setMensaje("No le entendimos, vuelva a preguntar");
            mensajeAutomatico(mensaje);
        }
    }

    private void mensajeAutomatico(Mensaje mensaje){
        if(adapterRvChatBot != null){
            adapterRvChatBot.addMensaje(mensaje);
            adapterRvChatBot.notifyDataSetChanged();
        }
    }

    private void openViewBuscar(){
        tvTituloBuscar.setVisibility(View.INVISIBLE);
        rlBuscar.setVisibility(View.VISIBLE);
        etBuscar.setEnabled(true);
        Utils.toShowSoftKeyboard(getActivity(),etBuscar);
        llServicioTop.setVisibility(View.GONE);
        ivAtras.setVisibility(View.VISIBLE);
        ivFiltro.setVisibility(View.VISIBLE);
        cvServicioTodo.setVisibility(View.VISIBLE);
        llServicioBot.setVisibility(View.GONE);
        etBuscar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(adapterRvServicio != null){
                    adapterRvServicio.getFilter().filter(charSequence.toString());
                }else{
                    new Utils().toast("Está vacio",getContext());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void closeViewBuscar(){
        tvTituloBuscar.setVisibility(View.VISIBLE);
        rlBuscar.setVisibility(View.INVISIBLE);
        etBuscar.setEnabled(false);
        llServicioTop.setVisibility(View.VISIBLE);
        ivAtras.setVisibility(View.INVISIBLE);
        ivFiltro.setVisibility(View.INVISIBLE);
        cvServicioTodo.setVisibility(View.GONE);
        llServicioBot.setVisibility(View.VISIBLE);
    }

    private void consultarServicios(){
        if(wsrlHome != null){
            wsrlHome.setRefreshing(true);
        }
        new ListaServicioTask(getContext(), (codResultado, desResultado, alServicio) -> {
            if(wsrlHome != null){
                wsrlHome.setRefreshing(false);
            }
            if(codResultado == Constantes.RESULT_OK){
                adapterRvServicio = new AdapterRvServicio(getContext(), alServicio, new AdapterRvServicio.IAdapterServicio() {
                    @Override
                    public void onClick(Servicio servicio) {
                        Intent intent = new Intent(getActivity(), ActivityDetalleServicio.class);
                        intent.putExtra("iServicio",servicio);
                        startActivity(intent);
                    }

                    @Override
                    public void onScrolledUp(int lastItemPosition) {

                    }

                    @Override
                    public void onScrolledDown(int lastItemPosition) {

                    }

                    @Override
                    public void onScrolledTwoItem(boolean isTwoItem) {
                        if(isTwoItem){
                            llSubir.setVisibility(View.VISIBLE);
//                            wsrlHome.setEnabled(false);
                            svHome.fullScroll(ScrollView.FOCUS_DOWN);
                        }else{
//                            wsrlHome.setEnabled(true);
                            llSubir.setVisibility(View.INVISIBLE);
                        }
                    }

                });
                rvListaTodoServicio.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,false));
                rvListaTodoServicio.setAdapter(adapterRvServicio);
            }else{
                new Utils().toast(desResultado,getContext());
            }
        }).execute();

    }

    @OnClick({R.id.rlContenedor,R.id.ivAtras,R.id.ivFiltro,R.id.cvBuscar,R.id.llSubir})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.rlContenedor:
                Utils.toHideSoftKeyboard(getActivity());
                break;
            case R.id.ivAtras:
                isBuscarOpen = false;
                closeViewBuscar();
                etBuscar.clearFocus();
                etBuscar.setText("");
                Utils.toHideSoftKeyboard(getActivity());
                break;
            case R.id.ivFiltro:
                filtroCategoria();
                break;
            case R.id.cvBuscar:
                if(!isBuscarOpen){
                    isBuscarOpen = true;
                    openViewBuscar();
                    consultarServicios();
                }
                break;
            case R.id.llSubir:
                svHome.fullScroll(ScrollView.FOCUS_UP);
                rvListaTodoServicio.scrollToPosition(RecyclerView.SCROLLBAR_POSITION_DEFAULT);
                llSubir.setVisibility(View.GONE);
                break;
        }
    }

    private void filtroCategoria(){
        new CategoriaDialog(getContext(), new CategoriaDialog.ICategoriaDialog() {
            @Override
            public void onClick(Categoria categoria) {
                if(wsrlHome != null){
                    wsrlHome.setRefreshing(true);
                }
                new ListaServicioXCategoriaTask(getContext(), categoria.getCodCategoria(), new ListaServicioXCategoriaTask.IListaServicio() {
                    @Override
                    public void onResult(int codResultado, String desResultado, ArrayList<Servicio> alServicio) {
                        if(wsrlHome != null){
                            wsrlHome.setRefreshing(false);
                        }
                        if(codResultado == Constantes.RESULT_OK){
                            adapterRvServicio = new AdapterRvServicio(getContext(), alServicio, new AdapterRvServicio.IAdapterServicio() {
                                @Override
                                public void onClick(Servicio servicio) {
                                    Intent intent = new Intent(getActivity(), ActivityDetalleServicio.class);
                                    intent.putExtra("iServicio",servicio);
                                    startActivity(intent);
                                }

                                @Override
                                public void onScrolledUp(int lastItemPosition) {

                                }

                                @Override
                                public void onScrolledDown(int lastItemPosition) {

                                }

                                @Override
                                public void onScrolledTwoItem(boolean isTwoItem) {
                                    if(isTwoItem){
                                        llSubir.setVisibility(View.VISIBLE);
//                                        wsrlHome.setEnabled(false);
                                         svHome.fullScroll(ScrollView.FOCUS_DOWN);
                                    }else{
//                                        wsrlHome.setEnabled(true);
                                        llSubir.setVisibility(View.INVISIBLE);
                                    }
                                }
                            });
                            rvListaTodoServicio.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,false));
                            rvListaTodoServicio.setAdapter(adapterRvServicio);
                        }else{
                            new Utils().toast(desResultado,getContext());
                        }
                    }
                }).execute();
            }
        }).show();
    }

    private boolean accederMicrofono(){
        //si la API 23 a mas
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            //Habilitar permisos para la version de API 23 a mas

            int verificarPermisoReadContacts = ContextCompat
                    .checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO);
            //Verificamos si el permiso no existe
            if(verificarPermisoReadContacts != PackageManager.PERMISSION_GRANTED){
                //verifico si el usuario a rechazado el permiso anteriormente
                if(shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)){
                    //Si a rechazado el permiso anteriormente muestro un mensaje
                    mostrarExplicacion();
                }else{
                    //De lo contrario carga la ventana para autorizar el permiso
                    requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, RECORD_AUDIO);
                }
            }else{
                //Si el permiso ya fue concedido abrimos en intent de RECORD_AUDIO
               return true;
            }

        }else{//Si la API es menor a 23 - abrimos en intent de RECORD_AUDIO

           return true;
        }
        return false;
    }

    private void mostrarExplicacion() {
        new AlertDialog.Builder(getActivity())
                .setTitle("Autorización")
                .setMessage("Necesito permiso para usar el microfono de tu dispositivo.")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, RECORD_AUDIO);
                        }

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Mensaje acción cancelada
                        mensajeAccionCancelada();
                    }
                })
                .show();
    }

    public void mensajeAccionCancelada(){
        Toast.makeText(getApplicationContext(),
                "Haz rechazado la petición, por favor considere en aceptarla.",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case RECORD_AUDIO:
                //Si el permiso a sido concedido abrimos la agenda de contactos
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    llMicrofono.callOnClick();
                } else {
                    mensajeAccionCancelada();
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
       // isBuscarOpen = false;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
