package com.studyway.miloficios.entities;

/**
 * Created by kenny on 16/04/2017.
 */
public class Host extends AbstractEntity {

    byte[] data = null;

    private String host = "";
    private int puerto = 0;
//    private String keyEncrypted = "";
//    private int idDispositivc = 0;
//    private int codEmpresa = 0;

    public void setHost(String host) {
        this.host = host;
    }

    public void setPuerto(int puerto) {
        this.puerto = puerto;
    }

    /**
     * @param host String
     * @param puerto int
     */
    public Host(String host, int puerto) {
        this.host = host;
        this.puerto = puerto;
    }

    /**
     * value encrypted
     * @param data byte[]
     */
    public Host(byte[] data) {
        this.data = data;
    }

    public Host() {
    }

    public String getHost() {
        return host;
    }

    public int getPuerto() {
        return puerto;
    }

//    public String getKeyEncrypted() {
//        return keyEncrypted;
//    }

    public byte[] getData() {
        return data;
    }
}
