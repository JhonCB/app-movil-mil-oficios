package com.studyway.miloficios.entities;

import com.studyway.miloficios.enums.IDUTILS;
import com.studyway.miloficios.utils.Utils;

public class Mensaje {
    public static final int HUMANO = 1;
    public static final int BOT = 2;
    public static final int BOT_IMAGEN = 3;

    private String fecha = new Utils().getFechaHora(IDUTILS.DDHHMMSS);
    private String mensaje;
    private int tipoMensaje; // 1 - humano // 2 - bot

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public int getTipoMensaje() {
        return tipoMensaje;
    }

    public void setTipoMensaje(int tipoMensaje) {
        this.tipoMensaje = tipoMensaje;
    }
}
