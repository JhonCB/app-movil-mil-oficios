package com.studyway.miloficios.entities;
/**
 * creado por Jhon Coronel
 * Correo: Ghoozh@gmail.com
 */
public class Localizacion {
    private int codLocalizacion = 0;
    private String nomLocalizacion = "";
    private String latitud = "";
    private String longitud = "";

    public int getCodLocalizacion() {
        return codLocalizacion;
    }

    public void setCodLocalizacion(int codLocalizacion) {
        this.codLocalizacion = codLocalizacion;
    }

    public String getNomLocalizacion() {
        return nomLocalizacion;
    }

    public void setNomLocalizacion(String nomLocalizacion) {
        this.nomLocalizacion = nomLocalizacion;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    //to display object as a string in spinner
    @Override
    public String toString() {
        return nomLocalizacion;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Localizacion){
            Localizacion c = (Localizacion )obj;
            if(c.getNomLocalizacion().equals(nomLocalizacion) && c.getCodLocalizacion()==codLocalizacion ) return true;
        }

        return false;
    }

}

