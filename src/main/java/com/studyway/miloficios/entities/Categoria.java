package com.studyway.miloficios.entities;
/**
 * creado por Jhon Coronel
 * Correo: Ghoozh@gmail.com
 */
public class Categoria {
    private int codCategoria = 0;
    private String nomCategoria = "";

    public int getCodCategoria() {
        return codCategoria;
    }

    public void setCodCategoria(int codCategoria) {
        this.codCategoria = codCategoria;
    }

    public String getNomCategoria() {
        return nomCategoria;
    }

    public void setNomCategoria(String nomCategoria) {
        this.nomCategoria = nomCategoria;
    }

    @Override
    public String toString() {
        return nomCategoria;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Categoria){
            Categoria c = (Categoria)obj;
            if(c.getNomCategoria().equals(nomCategoria) && c.getCodCategoria()==codCategoria ) return true;
        }

        return false;
    }

}

