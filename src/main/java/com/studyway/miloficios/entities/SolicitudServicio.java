package com.studyway.miloficios.entities;

import java.io.Serializable;

/**
 * creado por Jhon Coronel
 * Correo: Ghoozh@gmail.com
 */
public class SolicitudServicio extends AbstractEntity implements Serializable {
    public int codServicio = 0;
    public String costo = "";
    public String cotizacion = "";
    public int estado = 0;
    public String fecha = "";
    public String categoria = "";
    public String urlFoto = "";
    public String nomOfertante = "";
    public String desServicio = "";
    public String ubicacion = "";

    public SolicitudServicio() {

    }

    public SolicitudServicio(int codServicio, String costo, String cotizacion, int estado, String fecha, String categoria, String urlFoto, String nomOfertante, String desServicio, String ubicacion) {
        this.codServicio = codServicio;
        this.costo = costo;
        this.cotizacion = cotizacion;
        this.estado = estado;
        this.fecha = fecha;
        this.categoria = categoria;
        this.urlFoto = urlFoto;
        this.nomOfertante = nomOfertante;
        this.desServicio = desServicio;
        this.ubicacion = ubicacion;
    }

    public int getCodServicio() {
        return codServicio;
    }

    public void setCodServicio(int codServicio) {
        this.codServicio = codServicio;
    }

    public String getCosto() {
        return costo;
    }

    public void setCosto(String costo) {
        this.costo = costo;
    }

    public String getCotizacion() {
        return cotizacion;
    }

    public void setCotizacion(String cotizacion) {
        this.cotizacion = cotizacion;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getNomOfertante() {
        return nomOfertante;
    }

    public void setNomOfertante(String nomOfertante) {
        this.nomOfertante = nomOfertante;
    }

    public String getDesServicio() {
        return desServicio;
    }

    public void setDesServicio(String desServicio) {
        this.desServicio = desServicio;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
}
