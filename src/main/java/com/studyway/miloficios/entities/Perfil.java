package com.studyway.miloficios.entities;
/**
 * creado por Jhon Coronel
 * Correo: Ghoozh@gmail.com
 */
public class Perfil  extends AbstractEntity{
    //SQL BD
    public static int ADMINISTRADOR = 1;
    public static int SOCIO = 2;
    public static int CLIENTE = 3;

    public final static int GOOGLE = 10;
    public final static int FACEBOOK = 9;
    public final static int STUDYWAY = 8;

    private String nombre = "";
    private String apellidos = "";
    private String nombreCompleto = "";
    private String telefono = "";
    private int tipoUsuario = 0; //Prestador de servicio (0) - Consumidor (1)
    private String email = "";
    private String idPerfil = "";
    private int tipoPerfil = 0; //REQUEST.INVITADO.getInt(); // default
    private String urlFotoPerfil = "";
    private int flagEnviado = 0;
    private int sesionActiva = 0;

    //no están en SQLITE
    private String contrasenia = "";
    private String dni = "";
    private String direccion = "";
    private int codLocalizacion = 1;
    private String fechaNacimiento = "";
    private String nomUsuario = "";


    public Perfil(){

    }

    public Perfil(String nombre, String apellidos, String nombreCompleto, String telefono, String email, String idPerfil, int tipoPerfil, String urlFotoPerfil) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.nombreCompleto = nombreCompleto;
        this.telefono = telefono;
        this.email = email;
        this.idPerfil = idPerfil;
        this.tipoPerfil = tipoPerfil;
        this.urlFotoPerfil = urlFotoPerfil;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(int tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(String idPerfil) {
        this.idPerfil = idPerfil;
    }

    public int getTipoPerfil() {
        return tipoPerfil;
    }

    public void setTipoPerfil(int tipoPerfil) {
        this.tipoPerfil = tipoPerfil;
    }

    public String getUrlFotoPerfil() {
        return urlFotoPerfil;
    }

    public void setUrlFotoPerfil(String urlFotoPerfil) {
        this.urlFotoPerfil = urlFotoPerfil;
    }

    public int getFlagEnviado() {
        return flagEnviado;
    }

    public void setFlagEnviado(int flagEnviado) {
        this.flagEnviado = flagEnviado;
    }

    public int getSesionActiva() {
        return sesionActiva;
    }

    public void setSesionActiva(int sesionActiva) {
        this.sesionActiva = sesionActiva;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getCodLocalizacion() {
        return codLocalizacion;
    }

    public void setCodLocalizacion(int codLocalizacion) {
        this.codLocalizacion = codLocalizacion;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getNomUsuario() {
        return nomUsuario;
    }

    public void setNomUsuario(String nomUsuario) {
        this.nomUsuario = nomUsuario;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }
}
