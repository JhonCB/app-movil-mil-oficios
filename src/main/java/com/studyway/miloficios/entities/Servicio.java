package com.studyway.miloficios.entities;

import java.io.Serializable;

/**
 * creado por Jhon Coronel
 * Correo: Ghoozh@gmail.com
 */
public class Servicio extends AbstractEntity implements Serializable {
    public int codServicio = 0;
    public String nombreUsuPub = "";
    public int calificacion = 0;
    public String fechaInicio = "";
    public String fechaFin = "";
    public String estado = ""; //hací lo definieron en la BD e.e
    public String descripcionServicio = "";
    public String descripcion = "";
    public String ubicacion = "";
    public String urlFoto = "";
    public String latitud = "";
    public String longitud = "";

    public Servicio() {
    }

    public Servicio(int codServicio, String nombreUsuPub, int calificacion, String fechaInicio, String fechaFin, String estado, String descripcionServicio, String descripcion, String ubicacion, String urlFoto, String latitud, String longitud) {
        this.codServicio = codServicio;
        this.nombreUsuPub = nombreUsuPub;
        this.calificacion = calificacion;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.estado = estado;
        this.descripcionServicio = descripcionServicio;
        this.descripcion = descripcion;
        this.ubicacion = ubicacion;
        this.urlFoto = urlFoto;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public int getCodServicio() {
        return codServicio;
    }

    public void setCodServicio(int codServicio) {
        this.codServicio = codServicio;
    }

    public String getNombreUsuPub() {
        return nombreUsuPub;
    }

    public void setNombreUsuPub(String nombreUsuPub) {
        this.nombreUsuPub = nombreUsuPub;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDescripcionServicio() {
        return descripcionServicio;
    }

    public void setDescripcionServicio(String descripcionServicio) {
        this.descripcionServicio = descripcionServicio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }
}
